# How to add / improve an exercise?

The `_docs` directory contains the different exercises. Create in this directory a new directory containing the Markdown documents and the images of your exercise.

The images can be placed in the subdirectory of `_docs` created for the exercise or put in the ʻassets / images` directory if the images are reusable throughout the site.

> Note: the https://gitlab.com/stm32python/assets repository contains assets common to all sites (fr, en; it ...)

The document `_data / toc.yml` contains the description of the contents (on the left) of the site. This document should be completed with the entries to your tutorial.

Before pushing to the central site, check the proper functioning of the generation from your local environment with the following commands then by opening the page `http: //127.0.0.1: 4000 / it /`

````bash
git clone git@gitlab.com: stm32python / it.git
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
````

Once the push has started, check the status of the CI / CD pipeline https://gitlab.com/stm32python/it/pipelines

# Remarks
* The `_config.yml` document contains the site parameters used for the generation.
* The `pages` directory contains the templates of the generated HTML pages.
* The `_includes` directory contains the templates of the HTML fragments used for the generation of HTML pages.
* HTML pages are generated in the `_site` directory
* The ʻassets` directory contains all the _assets_ (`.css`,` .js`, `.svg`,` .png` ...) referenced in the generated HTML pages.