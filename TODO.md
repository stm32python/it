# TODOLIST

## Général
* Choix de la licence pour les codes sources (probablement BSD-3)
* Choix de la licence pour les documents (probablement une des CC https://creativecommons.org/licenses/?lang=fr-FR)
* lister les contributeurs dans `CONTRIBUTORS.md`
* ajouter des references Wikipedia dans le glossaire
* ajouter une FAQ (Domande frequenti)

## Tutoriel
* Guide d'installation Windows 10
* Tutoriel pour [Fritzing](https://fritzing.org/home/)
* Tutoriel pour [Jupyter](https://jupyter.org/)
* Tutoriel pour Matlab
* Tutoriel pour les cartes fille
    * MEMS
    * [X-NUCLEO-GNSS1A1 : GNSS expansion board based on Teseo-LIV3F module for STM32 Nucleo](https://www.st.com/en/ecosystems/x-nucleo-gnss1a1.html)
    * [DC Motor Driver](https://air.imag.fr/index.php/File:MonsterMotoShield%2BSTNucleoF401.jpg)
    * Stepper Driver
    * TM1638 SPI https://skyduino.wordpress.com/2012/07/31/arduino-carte-tm1638-7-segments-leds-boutons/
* Tutoriel pour composants indivituels
    * Keypad 4x3 https://air.imag.fr/index.php/Keypad
    * [Nunchuck](https://air.imag.fr/index.php/Nunchuck) 
    * Super Nintendo NES Gamepad (en cours de réalisation par Alex)
    * Old fashion VU meter (à brancher sur une sortie PWN de la carte)
    * Capteur de distance infrarouge
    * Lecteur NFC SPI (ISO 14443, MiFare)
        * [ST25R readers](https://www.st.com/en/nfc/st25-nfc-rfid-readers.html)
        * [PN532 Module NFC Arduino reader](https://learn.adafruit.com/adafruit-pn532-rfid-nfc/arduino-library)
    * Capteur présence IR (Motion detector)
* Tutoriel Mini-Robot avec application de télécommande
    * accéléromêtre (choc) et magnetomêtre (correction de trajectoire) du [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * H-Bridge pour 2 moteurs DC https://air.imag.fr/index.php/L298N_Stepper_Motor_Driver_Controller_Board_for_Arduino
    * [Châssis Robotique 2WD](https://www.robotshop.com/eu/fr/chassis-robotique-2wd-pour-debutant.html)
    * Capteur de distance infrarouge (type Sharp GP2Y0A41SK0F)
    * Capteur de distance time-of-flight (VL53L0X, VL53L1X ...)
* Tutoriel Station météo + Qualité de l'air
    * [X-NUCLEO-IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * [Weather Meter Kit](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)
    * Grove Arduino shield
    * [RJ11 6-Pin Connector](https://www.sparkfun.com/products/132) x2
* Tutoriel G-Code pour le pilotage de plusieurs moteurs pas-à-pas (_steppers_ _motors_) NEMA.
* Ajout de la carte STM32WB55 à l'[IDE MBlock](https://ide.mblock.cc/?python#/devices) ([voir](https://www.mblock.cc/doc/en/developer-documentation/add-device-1.html))
* Tutoriel d'horloge 4 x 7 segments ou LCD avec une RTC et une synchronisation DCF77 https://forum.arduino.cc/index.php?topic=356124.0 
* Tutoriel station Météo (avec afficheur LCD) avec le kit Meteo https://www.sparkfun.com/products/15901
* Tutoriel Station Qualité de l'Air : voir https://github.com/airqualitystation et https://airqualitystation.github.io
* Tutoriel Smart Doorbell : voir https://blog.st.com/stm32-roadshow/
* Tutoriel Contrôle d'accès (Badgeuse NFC, Capteur présence infra-rouge, Buzzer, RTC ...)
* Tutoriel Nunchuk + Pan-Tilt (2 servo-moteurs) + VU Metres (analogiques et LED)
* Tutoriel Sonometre Carte Nucleo 2 micros + VU Metres (analogiques et LED)

## Firmware
* STM32WL55 (tutoriel avec TheThingsNetwork)
* [STM32 Nucleo B-L072Z-LRWAN1](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) (tutoriel avec TheThingsNetwork)

## Pages
* Générer les favicons à partir du logo
* Ajout d'un menu pour le changement de langue
* Ajout d'une grille pour la page d'accueil (Masonry https://masonry.desandro.com/)
* Ajout les tags aux pages et faire une page de tags 
* Ajout de SEO tags https://github.com/jekyll/jekyll-seo-tag
* [x] Ajouter le logo STM32Python
* Ajouter des permalinks dans les index des tutoriels
* Ajouter un site à propos de la vie privée (site.privacy dans _config.yml)
* Ajout du feed.xml ?

## Conventions d'écriture
* nom de fichiers et images en minuscule
* rédaction/correction des tutoriels à la seconde personne du pluriel "vous".