# Comment ajouter/editer un exercice ?

## La structure du dépôt Gitlab

* Le répertoire `_docs` contient les différents exercices.
* Le document `_data/toc.yml` contient la description du sommaire (à gauche) du site.
* Le document `_config.yml` contient les paramêtres du site utilisés pour la génération.
* Le répertoire `pages` contient les patrons des pages HTML générées.
* Le répertoire `_includes` contient les patrons des fragments HTML utilisés pour la génération des pages HTML.
* Les pages HTML sont générées dans le répertoire `_site`
* Le répertoire `assets` contient tous les _assets_ (`.css`, `.js`, `.svg`, `.png` ...) référencés dans les pages HTML générées. Il contient les images qui sont réutilisables un peu partout dans le site.

> A noter: le dépôt https://gitlab.com/stm32python/assets contient les assets communs à tous les sites (fr, en ...)

## Corrections, ajouts et améliorations d'exercices

Créez dans le répertoire `_docs` un nouveau répertoire contenant les documents Markdown et les images de votre exercice.

Les images peuvent être placées dans le sous-répertoire de `_docs` créé pour l'exercice ou bien mis dans le répertoire `assets/images` si les images sont réutilisables un peu partout dans le site.

Complétez le document `_data/toc.yml` pour ajouter votre exercice au sommaire (à gauche) du site. 

## Edition depuis votre poste de travail

L'édition des documents peut se faire directement depuis le site https://gitlab.com/stm32python/it . Cependant, chaque modification déclenche le pipeline CI de regénération des pages. Cette opération est lente et consomme de l'énergie.

Vous pouvez éditer depuis votre poste de travail plusieurs pages du site et vérifier le rendu depuis votre poste de travail.

Pour cela, vous devez installer le générateur Jekyll de la facon suivante:
```bash
git clone git@gitlab.com:stm32python/fr.git
cd fr
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Ouvrez la page `http://127.0.0.1:4000/fr/`


Récuperez les dernières modifications avec de commencer des éditions:
```bash
cd fr
git pull
```

Editez les pages; le générateur Jekyll sera relancé à chaque sauvegarde. Fraichissez la page web dans votre navigateur pour constater les modifications effectuées.

Une fois que les ajouts et les modifications sont satisfaisantes, poussez les modifications vers le serveur central.
```bash
git add *
git commit -m "amélioration de ceci cela"
git push
```

Une fois le `push` effectué, vérifiez l'état du pipeline CI/CD https://gitlab.com/stm32python/fr/pipelines

Visualisez les pages du site en ligne https://stm32python.gitlab.io/fr/

