---
layout: page
title: About
permalink: /about/
---
# About

## Contesto

Uno dei temi trattati da questo corso è l'Internet of Things (IoT) ossia l'estensione di Internet a cose e luoghi nel mondo fisico.
L'obiettivo è portare i giovani ad un primo livello di comprensione dell'Internet of Things. La sfida è promuovere l'orientamento degli studi verso le materie STEM (Scienza, Tecnologia, Ingegneria, Matematica).

## Obiettivo

L'obiettivo di STM32Python è fornire agli insegnanti delle scuole superiori e ai loro studenti, materiali didattici open source utili per partire con  l'Internet delle cose per l'insegnamento di materie tecnologiche. Questi supporti sono basati sulla piattaforma STMicroelectronics Nucleo con microcontrollore STM32, e consentono di produrre codice assembly, C, C++ e microPython per microcontrollori STM32.

I materiali prodotti possono essere utilizzati anche da altri corsi, in particolare quelli realitivi ad NSI (Numerics and Computer Sciences), ad IS (Engineering Sciences), o nella specializzazione tecnologica STI2D (Sciences and Technologies of the "Industria e sviluppo sostenibile).


## Partner
I partner di STM32Python sono:
* I Rettorati delle Università di [Grenoble](http://www.ac-grenoble.fr) e di [Aix-Marseille](http://www.ac-aix-marseille.fr),
* [ST Microelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr),
* [Perlatecnica](http://www.perlatecnica.it/),
* [ISIS Ferraris-Buccini Marcianise](https://www.isismarcianise.edu.it/ferraris-buccini/).

## Come contribuire

Insegnante, Studente, Studente liceale, Ingegnere, Hobbista, che hai completato questi tutorial: non esitare a contattarci per contribuire al progetto!

stm32python-contact@imag.fr

## Contributeurs
* Erwan LE SAINT
* Michael ESCODA
* Richard PHAN
* Romaric NOLLOT
* Guy CHATEIGNIER
* Didier DONSEZ
* Baptiste JOLAINE
* Aurélien REYNAUD
* Pedro LOPES
* Gaël LEMIERE
* Robin FARGES
* Florian VIOLET
* Leïla MICHELARD
* Manon CHAIX
* Gloria NGUENA
* Jeremy SLAMA
* Alex FOUILLEUL
* Nicolas GIROUD
* Angelo ALBERICO
* Raffaele CASCELLA
* Jonathan DE LUCA
* Salvatore TESSITORE 
* Luca DELL'OSTIA
* Anna Iodice
* Mauro D'Angelo 

