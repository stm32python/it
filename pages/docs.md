---
layout: page
title: Documentation
permalink: /docs/
---

# Documentazione

Benvenuti nella pagina di documentazione di STM32 Python! Qui è possibile accedere direttamente alle diverse parti del sito.

<div class="section-index">
    <hr class="panel-line">
    {% for post in site.docs  %}        
    <div class="entry">
    <h5><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h5>
    <p>{{ post.description }}</p>
    </div>{% endfor %}
</div>
