---
layout: page
title: STM32Python
permalink: /
---
# Benvenuti nel sito Web STM32MicroPython

<p align="center">
<img src="assets/logos/stm32upython.svg" width="400px" alt="logo"/>
</p>


## Contesto

Uno dei temi trattati da questo corso è l’Internet of Things (IoT) ossia l’estensione di Internet a cose e luoghi nel mondo fisico. L’obiettivo è portare i giovani ad un primo livello di comprensione dell’Internet of Things. La sfida è promuovere l’orientamento degli studi verso le materie STEM (Scienza, Tecnologia, Ingegneria, Matematica).

## Obiettivo

L’obiettivo di STM32Python è fornire agli insegnanti delle scuole superiori e ai loro studenti, materiali didattici open source utili per partire con l’Internet delle cose per l’insegnamento di materie tecnologiche. Questi supporti sono basati sulla piattaforma STMicroelectronics Nucleo con microcontrollore STM32, e consentono di produrre codice assembly, C, C++ e microPython per microcontrollori STM32.

I materiali prodotti possono essere utilizzati anche da altri corsi, in particolare quelli realitivi ad NSI (Numerics and Computer Sciences), ad IS (Engineering Sciences), o nella specializzazione tecnologica STI2D (Sciences and Technologies of the “Industria e sviluppo sostenibile).

## Partner
I partner di STM32Python sono:
* I Rettorati delle accademie di [Grenoble](http://www.ac-grenoble.fr) e di [Aix-Marseille](http://www.ac-aix-marseille.fr),
* [STMicroelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr),
* [Perlatecnica](http://www.perlatecnica.it/),
* [ISIS Ferraris-Buccini Marcianise](https://www.isismarcianise.edu.it/ferraris-buccini/).
