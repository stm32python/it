---
title: versione di µPython e STM32WB
description: versione di µPython e STM32WB

---

## Versioni

| Descrizione | Autore| Data | Revisione | Firmware |
| ----------- | ------ | ---- | -- | --------------------- |
| Brouillon   | RNT 	| 20/12/2019 | Draft | | 	
| Prima consegna pre-officina| RNT | 14/01/2019 | 1 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |
| Aggiunta di Bluetooth Low Energy | RNT | 19/02/2020 | 2 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |
| Aggiunta di scheda di espansione IKS01A3 | RNT | 30/03/2020 | 3 | [firmware_micropython](../../assets/Firmware/firmware_micropython.dfu) |

