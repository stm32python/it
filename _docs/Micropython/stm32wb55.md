---
title: Il kit di sviluppo STM32WB55
description: Il kit di sviluppo STM32WB55

---
# Il kit di sviluppo STM32WB55

## Panoramica della scheda Nucleo STM32WB55

La scheda STM32-NUCLEO è progettata con i componenti necessari per avviare il microcontrollore.

I connettori di estensione (ARDUINO, MORPHO) consentono all'utente di collegare componenti elettronici esterni all'STM32.

La scheda elettronica dispone inoltre di un connettore per batteria CR2032, 3 pulsanti (SW1, SW2, SW3), 3 LED (rosso, verde e blu) e 2 connettori micro-USB.

_Ecco lo schema a blocchi del kit NUCLEO-WB55:_
 
![image](images/architecture.png)

## Descrizione della scheda Nucleo STM32BW55

_Vista dall'alto e dal basso del kit di sviluppo:_

![image](images/board.png)


### PCB Antenna

Questa è l'antenna bluetooth del microcontrollore STM32WB55. È una cosiddetta antenna PCB perché è integrata direttamente nel circuito della scheda. La sua forma complessa è studiata in modo da ottimizzare la ricezione e l'emissione di onde radio per la frequenza bluetooth di 2.4 GHz.


### Connettori Arduino / Morpho 

I kit di estensione possono essere aggiunti semplicemente con questi connettori standardizzati.

Ad esempio, puoi collegare il kit microfono Mems (X-NUCLEO-CCA02M2) alla scheda NUCLEO-WB55:

![image](images/boards.png)


### USB User

Questa è la porta USB che verrà utilizzata per:

*   Comunica con l'interprete MicroPython.
*   Programma il sistema.
*   Potenzia il kit di sviluppo


### Socket CR2032

Una volta programmato il sistema, sarà possibile fornire al kit una batteria CR2032, in modo da rendere il sistema portatile.


#### STLINK

Questo è lo strumento che ti permette di programmare il microcontrollore, per questo usa il "drag and drop" per rilasciare il firmware sul dispositivo USB : **WB55-NODE**. 

### User LEDs

Questi LED sono accessibili allo sviluppatore e possono essere attivati ​​utilizzando MicroPython.

Guarda l'esempio cambia lo stato di un'uscita (GPIO) (attiva un LED)

