---
title: Registrazione di un file audio su una scheda micro-SD
description:Registrazione di un file audio su una scheda micro-SD
---

### Registrazione di un file audio su una scheda micro-SD

In questo capitolo vedremo due nuove funzionalità, la registrazione di un file audio in formato wav e il salvataggio del file sulla scheda SD.

![SDCard](images/microphone_1.png)

_La dimensione della memoria Flash della scheda nucleo STM32WB55 è limitata, la funzione audio dipende dalla funzionalità di registrazione sulla scheda SD._

Per completare questo tutorial dovrai avere:

1. L'ultimo firmware micropython nella revisione 4 e dopo aver aggiornato la scheda Nucleo.
2. Un microfono dalla scheda di espansione STEVAL-MIC003V1, 1V1 o 2V1.
![Micro STEVAL-MIC003V1](images/microphone_2.png)
3.Supporto per micro SD e scheda SD. (https://www.adafruit.com/product/254)
![Support SDCard](images/microphone_3.png)

Ecco come cablare i diversi componenti con la scheda Nucleo :

![Montage Fritzing](images/microphone_4.png)

Una volta completato il cablaggio, è ora possibile registrare un file audio sulla scheda µSD.

Per questo ecco i comandi python da usare:

``` python
import pyb

audio = pyb.Sai() #Inizializzazione del dispositivo audio SAI

# Il metodo di registrazione accetta 2 parametri
# 1 : Il nome del file.
# 2 : Il tempo di registrazione in numero di secondi

audio.record('test_audio.wav', 5)
```

_Si consiglia di ricollegare la scheda NUCLEO per risincronizzare i file presenti con WIndows._

Il file audio "test_audio.wav" dovrebbe quindi apparire nell'elenco dei file memorizzati sulla scheda SD.

Sarai in grado di scaricare il file utilizzando Windows Explorer.

