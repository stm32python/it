---
title: Download
description: Collegamenti ed esercizi per il download del firmware

---
# Download di STM32microPython

**Utility software**<br>
[Notepad++](https://drive.google.com/drive/folders/1cvxYGfjHUo5_WrzCS3nMCThETnBiXGI0?usp=sharing)<br>
[TeraTerm](https://drive.google.com/drive/folders/1ZHIoUInsrr_ekHqgWNX5gP6gFnjybX6M?usp=sharing)<br>
[Thonny](https://drive.google.com/drive/folders/1iXiL6h0Fg4It9dGXlwMlYXABr6kiJjk5?usp=sharing)<br>
[PuTTY](https://drive.google.com/drive/folders/1QaN4n6_zORZ7P8XOjuydCYZOuSsGiICq?usp=sharing)<br>
[PyScript](https://drive.google.com/drive/folders/1EI5MaYYN4TEkXPOmChPhXUfrTyALq4LH?usp=sharing)<br>

**Firmware**<br>
[Firmware_1.13](https://drive.google.com/drive/folders/1hrllG6rvtkksMqYClUx0DB360AFfaxR5?usp=sharing)

**TP ed esempi**<br>
- [GPIO](https://drive.google.com/drive/folders/1DiLyczHdsHKvI42AuZqsXHzCuAQto4HH?usp=sharing) (Usa GPIO attraverso esempi di lettura / scrittura)
    - [BLINK](https://drive.google.com/drive/folders/1OcVAapqjPVz6O-TzgY9D9Nk7M7wX5m2I?usp=sharing) (Lampeggia un LED con una pausa di 100 ms)<br>
    - [Bottoni](https://drive.google.com/drive/folders/1jlvg8n15AOjq-tlPlK1CzQq3AiOeA4ck?usp=sharing) (Recupera il valore dei 3 pulsanti della scheda NUCLEO-WB55 tramite l'UART) ***Esercizi di base*** <br>
    - [Chenillard](https://drive.google.com/drive/folders/1jlvg8n15AOjq-tlPlK1CzQq3AiOeA4ck?usp=sharing) (Fai un chase con i 3 LED della scheda NUCLEO-WB55) ***Esercizi di base*** <br>
- [ADC](https://drive.google.com/drive/folders/1XAsawgi9TZguRd0xsCB6ym-X1msduJkA?usp=sharing) (Lettura di un valore analogico)<br>
    - [Potentiomètre](https://drive.google.com/drive/folders/1z81SefKdJfMtB4gO5wsGtSwt0Ft_snV6?usp=sharing) (Lettura di un valore analogico da un potenziometro) ***Esercizi di base*** <br>    
    - [Potentiomètre GROVE](https://drive.google.com/drive/folders/1SBU0z-jxGM6Tuw23r6XrN4WAArMSoHSn?usp=sharing) (Lettura di un valore analogico del potenziometro GROVE) ***Esercizi con il potenziometro GROVE*** <br>    


- [LCD](https://drive.google.com/drive/folders/1pZDNmAUu1uJ1fwTnsKhNiW2FjeUjvQjE?usp=sharing) (Visualizzazione del testo su uno schermo LCD)<br>
    - [LCD oled I2C](https://drive.google.com/drive/folders/1duCMNQUrZK77HbquBYPk-xxrFBY1KsW0?usp=sharing) (Visualizza il testo su uno schermo LCD OLED dotato di driver SSD1306) ***Esercizi di base*** <br>
    - [LCD RGB GROVE](https://drive.google.com/drive/folders/1DXiMB_KreojxsrNGuav6uEgeV5DCPgN0?usp=sharing) (Visualizza il testo su uno schermo LCD GROVE RGB)<br><br>
- [Afficheur 7 segments](https://drive.google.com/drive/folders/1ofAKSS4xH9wYaqVOVbevqYDppu7-QzVT?usp=sharing) (Usa il display a 7 segmenti del boschetto) ***Exercice afficheur 7 segments GROVE*** <br>
- [Capteurs](https://drive.google.com/drive/folders/1_vTThqJDM9yjbqyhf7piKjsi2Ba9a0Re?usp=sharing) (Utilizza i sensori della scheda IKS ma anche quelli presenti nel kit boschetto)<br>
    - [HTS221 temperature & humidité (IKS)](https://drive.google.com/drive/folders/1K_Vf-lfnhXh7SyUDHPlAYDzQ3GXq7y8f?usp=sharing) (Visualizza la temperatura e l'umidità su UART ogni 1000 ms)
    - [LIS2DW12 accéléromètre (IKS)](https://drive.google.com/drive/folders/1_QamFKFrW9G1_y81lvvp9izCnwu6OVG-?usp=sharing) (Rileva le accelerazioni sugli assi X, Y e Z.) ***Esercizi avanzati*** <br> 

    - [LIS2MDL magnétomètre](https://drive.google.com/drive/folders/1mrBEAMuY4I_0ug1n1Nd2f6v9n64OSSrs?usp=sharing) (Visualizza i valori della bussola sull'UART)<br>
    - [LPS22HH temperature & baromètre](https://drive.google.com/drive/folders/1AA_TNeY3H192iNwOcHG3D6VCz48HpowZ?usp=sharing) (Visualizza la temperatura e la pressione atmosferica sull'UART)<br>
    - [LSM6DSO capteur inertiel (IKS)](https://drive.google.com/drive/folders/1AA_TNeY3H192iNwOcHG3D6VCz48HpowZ?usp=sharing) (Visualizza i valori del sensore inerziale in mG sull'UART)<br>
    - [STTS751 temperature (IKS)](https://drive.google.com/drive/folders/1CnYA3ma8nce4BXFTvUzgwsUgIVHPReZM?usp=sharing) (Visualizza la temperatura sull'UART ogni 1000 ms)<br>
    - [DS18X20 temperature (onewire)](https://drive.google.com/drive/folders/1PcPXOUFS-uhDNxUJoNcKMargf8jrnJpC?usp=sharing) (Visualizza la temperatura sull'UART)<br><br>

- [BLE](https://drive.google.com/drive/folders/1fCdS52mXV_JdiAr9_qPNXRR4T7YEm5BE?usp=sharing) (Utilizzare il Bluetooth Low Energy della scheda NUCLEO-WB55 e inviare valori T ° ... casuali all'applicazione del sensore ST BLE) ***Esercizi di base***<br>

- [DEL Infrarouge (GROVE)](https://drive.google.com/drive/folders/1brZTXDqQQnRLv1i4Y-xkD1F-fRYB-cUC?usp=sharing) (Utilizzando il LED IR) ***Esercizio LED a infrarossi GROVE***<br>
- [Luminosité (GROVE)](https://drive.google.com/drive/folders/1itcd2G1O9j0yV1GqZISoculRMxS_oA71?usp=sharing) (Utilizzo del sensore di luce) ***GROVE esercizio di luminosità***<br>
- [Buzzer (GROVE)](https://drive.google.com/drive/folders/1Djq0uu6mjPGXdY3nkpXBhYjpPsijHzwy?usp=sharing) (Utilizzando il buzzer) ***Exercice buzzer GROVE*** <br>
- [Boite a outils](https://drive.google.com/drive/folders/1Ai3PxDEt_lg25iMSsxLRzbKdU-fQH9zE?usp=sharing) (Trova pezzi di codice che possono aiutarti, timer, interruzioni...)<br>

- [Applications](https://drive.google.com/drive/folders/1q3iUhJ6baRqnyXtEBbkm1Hl4xi8gG31Z?usp=sharing) (Trova applicazioni come BLE casting...)<br>
    - [BLE casting](https://drive.google.com/drive/folders/1FPvQwvTw085IOhLFaMeLIHgwr5JS6unF?usp=sharing) (Visualizza i valori dei sensori della scheda IKS sull'applicazione del tuo smartphone e sul tuo display)<br>
    - [Alarme (GROVE)](https://drive.google.com/drive/folders/17-AiX-s7BcV0JSwIUTA1IoGFgAVjD05N?usp=sharing) (Rilevazione del movimento con la cattura del kit boschetto) ***Exercice alarme GROVE*** <br>
