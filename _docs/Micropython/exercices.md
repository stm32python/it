---
title: Esercizi di base con MicroPython
description: Esercizi di base con MicroPython

---
# Esercizi di base con MicroPython
**Il codice sorgente di ogni esercizio è disponibile in [area download](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)**<br><br>
## Recupera lo stato di un pulsante (GPIO) (Leggere lo stato di un pulsante)

Ci sono 3 pulsanti SW1, SW2, SW3 disponibili per lo sviluppatore : 

![image](images/boutons.png)

Vedremo in questa sottosezione come inizializzare un Pin in modalità "Input" e visualizzare un messaggio quando si preme uno dei 3 pulsanti utilizzando **pyb.Pin**. 

Useremo il cosiddetto metodo “pull” per chiedere al sistema MicroPython lo stato del Pin (1 o 0). 

Per ragioni elettroniche, lo stato del Pin a riposo ovvero a pulsante rilasciato, è equivalente a 1 mentre lo stato alla pressione di un pulsante è 0.

*   Apri l'editor di script e modifica il file ***main.py***: 

```python
import pyb # Libreria MicroPython che consente l'accesso alle periferiche (GPIO, LED, etc)
import time # Libreria che consente di interrompere il sistema
print("I GPIO con MicroPython sono facili")
# Inizializzazione dei pin di ingresso (SW1, SW2, SW3)
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
# Inizializzazione delle variabili
Vecchio_Valore_sw1 = 0
Vecchio_Valore_sw2 = 0
Vecchio_Valore_sw3 = 0
while 1: # Creazione di un ciclo infinito
	# il sistema si ferma per 300 ms
	time.sleep_ms(300)
	#Recupero dello stato dei pulsanti 1,2,3
	Valore_sw1 = sw1.Valore()
	Valore_sw2 = sw2.Valore()
	Valore_sw3 = sw3.Valore()
	#Lo stato attuale è diverso dallo stato precedente?
	if Valore_sw1 != Vecchio_Valore_sw1:
    	    if Valore_sw1 == 0:
        	        print("Il pulsante 1 (SW1) viene premuto")
    	    else:
        	        print("Il pulsante 1 (SW1) viene rilasciato")
    	    Vecchio_Valore_sw1 = Valore_sw1
	if Valore_sw2 != Vecchio_Valore_sw2:
    	    if Valore_sw2 == 0:
        	        print("Il pulsante 2 (SW2) viene premuto")
    	    else:
        	        print("Il pulsante 2 (SW2) viene rilasciato")
    	    Vecchio_Valore_sw2 = Valore_sw2
	if Valore_sw3 != Vecchio_Valore_sw3:
            if Valore_sw3 == 0:
        	       print("Il pulsante 3 (SW3) viene premuto")
    	    else:
        	       print("Il pulsante 3 (SW3) viene rilasciato")
    	    Vecchio_Valore_sw3 = Valore_sw3
```


*   
Salva lo script * main.py * (CTRL + S), quindi riavvia la scheda (CTRL + D).
Quando si preme uno dei 3 pulsanti (SW1, SW2, SW3), i messaggi nella console indicano lo stato dei pulsanti.

## Modifica lo stato di un output (GPIO) (Luce a LED)

L'obiettivo ora è accendere i LED sul kit di sviluppo:

![image](images/leds.png)


Ecco l'organizzazione dei LED per colore e numero:



1. LED 1 : blu
2. LED 2 : verde
3. LED 3 : rosso

In MicroPython, il modulo ** pyb.LED ** ti consente di gestire i LED in modo molto semplice.

In questa parte si vuole eseguire un “chaser”, questo esercizio consiste nell'accendere e spegnere i LED uno dopo l'altro, ciclicamente.

*  Utilizzando il metodo precedente e il seguente script, eseguire il chaser con MicroPython:

```python
import pyb
import time
print("I LED con MicroPython sono facili")
# Inizializzazione dei LEDs (LED_1, LED_2, LED_3)
led_blu = pyb.LED(1)
led_verde = pyb.LED(2)
led_rosso = pyb.LED(3)
# Inizializzazione del contatore LED
contatore_led = 0
while 1: # Creazione di un ciclo infinito
    if contatore_led == 0:
        led_blu.on()
        led_rosso.off()
        led_verde.off()
    elif contatore_led == 1:
        led_blu.off()
        led_verde.on()
        led_rosso.off()
    else:
        led_blu.off()
        led_verde.off()
        led_rosso.on()
    # Vogliamo accendere il LED successivo alla successiva iterazione del ciclo
    contatore_led = contatore_led + 1
    if contatore_led > 2:
        contatore_led = 0
    time.sleep_ms(500) # il sistema si ferma per 500 ms
```

## Lettura di un valore analogico (ADC)

Vorremmo ora convertire il valore analogico (0-3,3 V) di un segnale in un valore digitale (0-4095). 

È possibile collegare ad A0-5 un ingresso che restituisca una tensione compresa tra 0 e + 3.3V (eventualmente variabile nel tempo).Questo connettore è collegato direttamente all'ADC del microcontrollore.
 Il segnale può quindi essere convertito in un segnale digitale:
![image](images/adc.png)


Per questa dimostrazione utilizzeremo un potenziometro commerciale (10 KOhm) che colleghiamo ad A0, in questo modo:

![image](images/potentiometre.png)

Qualsiasi riferimento del potenziometro da 10 KOhm funzionerà per la dimostrazione. Ecco un riferimento:

[PTV09A-4020F-B103](https://www.mouser.fr/ProductDetail/Bourns/PTV09A-4020F-B103?qs=sGAEpiMZZMtC25l1F4XBU1xwXnrUt%2FuoeIXuGADl09o%3D)  : ![image](images/potentiometre2.png)

Con l'aiuto del seguente script, utilizza la funzione * pyb.ADC * per interagire con l'ADC STM32:

```python
import pyb
import time
print("ADC con MicroPython è facile")
# Inizializzazione dell'ADC sul Pin A0
adc_A0 = pyb.ADC(pyb.Pin('A0'))
while 1:
    valore_numerico = adc_A0.read()
    # È ora necessario convertire il valore numerico rispetto alla tensione di riferimento (3,3V) e
    # il numero di bit del convertitore (12 bits - 4096 valori)
    valore_analogico = (valore_numerico * 3.3) / 4095
    print("Il valore della tensione è :", valore_analogico, "V")
    # il sistema si ferma per 500 ms
    time.sleep_ms(500)
```

Puoi avviare il terminale Putty e osservare il valore in Volt che evolve, ogni 500ms, quando giri il potenziometro.

## Visualizzazione su uno schermo LCD OLED (I2C)

È molto facile usare un display OLED con MicroPython per visualizzare i messaggi. Vedremo in questo esempio come collegare lo schermo LCD in I2C, quindi come controllare lo schermo per inviare messaggi con MicroPython.

Per l'esempio, utilizzeremo lo schermo OLED monocromatico, 192 x 32 pixel, di Adafruit, tuttavia tutti gli schermi che integrano il driver SSD1306 sono compatibili.

Ecco come collegare il display OLED: 
* D15=SCL
* D14=SDA

![image](images/oled.png)



Avremo bisogno del file [ssd1306.py](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) disponibile direttamente in [TP LCD](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) 

Al termine del download, sarà necessario trasferire il file nella directory del dispositivo PYBLASH.

*   Ora modifica il file script ***main.py***:

```python
from machine import Pin, I2C
import ssd1306
from time import sleep

#Inizializzazione del dispositivo I2C
i2c = I2C(scl=Pin('SCL'), sda=Pin('SDA'), freq=100000)

#Impostazione delle caratteristiche dello schermo
larghezza_schermo_oled = 128
lunghezza_schermo_oled = 32
oled = ssd1306.SSD1306_I2C(larghezza_schermo_oled, lunghezza_schermo_oled, i2c)

#Invio di testo da visualizzare sullo schermo OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Troppo facile !!!', 0, 20)
oled.show()
```
## Uso di BLE (Bluetooth Low Energy)
In questa parte vedremo come comunicare tramite Bluetooth Low Power con l'applicazione STBLESensor e la scheda di sviluppo WB55.

### Installazione del sensore ST BLE sul tuo smartphone

Installa l'applicazione ST BLE Sensor sul tuo smartphone su [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) o [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)

Consultare [la descrizione completa dei vari servizi offerti dall'applicazione STBLESensor](https://github.com/STMicroelectronics/STBlueMS_Android)<br>

### Comunicazione BLE in MicroPython
Per comunicare in Bluetooth Low Energy con micropython, sarà necessario includere 2 nuovi file nella directory del disco usb * PYBFLASH *:
1. ***ble_advertising.py*** (File della guida per la creazione del messaggio di avviso)
2. ***ble_sensor.py*** (Classe che consente la gestione della connessione BLE)
<br>Dovrai scaricare gli script necessari per questo esempio [TP BLE](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)

Grazie al file * ble_sensor.py *, potremo creare un oggetto BLE con 1 servizio e 2 caratteristiche.

_Questo file dovrà essere modificato per cambiare il profilo BLE, se necessario._

Dopo l'avvio dello script, il kit di sviluppo WB55 inizia a emettere frame BLE, chiamati * advertising *. Questi messaggi vengono utilizzati per identificare l'oggetto Bluetooth e per indicare che il dispositivo è pronto per essere connesso.

Il nome del dispositivo è: *** WB55-MPY ***. Verificheremo con l'applicazione per smartphone se la scheda WB55 è in trasmissione bluetooth.

### uso
Avvia l'applicazione *** STBLESensor *** sul tuo smartphone:

![image](images/app1.png)

* Quindi tocca l'icona della lente di ingrandimento per visualizzare i dispositivi BLE circostanti:

![image](images/app2.png)

In questo esempio, il profilo BLE che abbiamo scelto ci permette di simulare un termometro e di accendere o spegnere un LED. Il valore del termometro viene generato casualmente ogni secondo.

* Connettiti alla scheda di sviluppo premendo "WB55-MPY":

![image](images/app3.png)

Il LED blu sulla scheda WB55 dovrebbe accendersi quando è collegata all'applicazione.
Possiamo osservare, in questa schermata, la variazione casuale di temperatura tra 0 e 100,0 ° C.
È possibile visualizzare la temperatura in modalità grafica.

* A tale scopo, premere il pulsante menu ![image](images/app-menu.png):

![image](images/app4.png)

* Ora premi ![image](images/app-plot.png):

![image](images/app5.png)

* Per visualizzare il grafico, premere ![image](images/app-play.png):

![image](images/app6.png)

Puoi usare il pulsante![image](images/app-options.png) per modificare le opzioni del grafico, come la dimensione dell'asse X o abilitare il cambio automatico della scala Y.

Ora studieremo l'invio di informazioni dallo SmartPhone alla piattaforma WB55.
Per questo usiamo l'applicazione per accendere o spegnere il LED rosso del kit di sviluppo.

* A tale scopo, premere il pulsante menu ![image](images/app-menu.png):

![image](images/app7.png)

* Ora scegli l'opzione ![image](images/app-switch.png):

![image](images/app8.png)

In questa schermata puoi controllare il LED rosso del kit di sviluppo.