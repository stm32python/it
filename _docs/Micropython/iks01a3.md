---
title: Esercizi avanzati in MicroPython con la scheda di espansione IKS01A3
description: Esercizi avanzati in MicroPython con la scheda di espansione IKS01A3

---
# Esercizi MicroPython avanzati con la scheda di espansione IKS01A3

Avrai bisogno della scheda di espansione IKS01A3 per questi esercizi.<br>
**Il codice sorgente di ogni esercizio / sensore è disponibile in[area download](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)**<br>
## Avviare

La scheda di espansione IKS01A3 è una scheda dimostrativa di diversi sensori MEMS di ST Microelectronics. La sua versione A3 contiene i seguenti sensori:

* [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) : Accelerometro 3D + giroscopio 3D
* [LIS2MDL](https://www.st.com/content/st_com/en/products/mems-and-sensors/e-compasses/lis2mdl.html) : Magnetometro 3D
* [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) : Accelerometro 3D
* [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) : Barometro (260-1260 hPa)
* [HTS221](https://www.st.com/content/st_com/en/products/mems-and-sensors/humidity-sensors/hts221.html) : Sensore di umidità relativa
* [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) : Termometro (–40 °C to +125 °C) 

Questi sensori sono collegati al bus * I2C * della scheda NUCLEO-WB55.

La scheda di espansione * IKS01A3 * dispone anche di uno slot in formato DIL a 24 pin per aggiungere ulteriori sensori I2C (ad esempio, il giroscopio [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a3.png)

## Avviare

Collegare la scheda * IKS01A3 *, fare attenzione a rispettare le marcature sui connettori: CN9 -> CN9, CN5 -> CN5, ecc.

Una volta collegata correttamente la scheda di espansione, ecco l'elenco dei sensori con i quali è possibile comunicare tramite I2C.

## Utilizzo dell'accelerometro LIS2DW12

Per questa dimostrazione, abbiamo scelto di utilizzare l'accelerometro a 3 assi (LIS2DW12).

La dimostrazione consiste nell'accendere un LED lungo l'asse su cui viene rilevata l'accelerazione (-1g) dovuta alla gravità.

Ecco la configurazione degli assi di accelerazione / colori dei LED:

*   Asse x : LED verde
*   Asse y : LED blu
*   Asse z : LED rosso

![image](images/iks01a3-3dir.png)


Come per ogni utilizzo di una libreria esterna, copia il file ***LIS2DW12.py*** (disponibile nell'esempio [LIS2DW12](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)) nella directory del disco USB : ***PYBFLASH***. 


Ora modifica lo script ***main.py***: 

``` python
from machine import I2C
import LIS2DW12
import pyb
import time

i2c = I2C(1) # I2C n ° 1 della scheda NUCLEO-W55 è utilizzato per comunicare con il sensore LISDW12
accelerometro = LIS2DW12.LIS2DW12(i2c)

led_blu = pyb.LED(1)
led_verde = pyb.LED(2)
led_rosso = pyb.LED(3)

while(1):
    time.sleep_ms(500)
    if abs(accelerometro.x()) > 700 : # Se il valore assoluto dell'accelerazione sull'asse X è maggiore di 700 mG, allora
        led_verde.on()
    else:
        led_verde.off() 
    if abs(accelerometro.y()) > 700 : # Se il valore assoluto dell'accelerazione sull'asse Y è maggiore di 700 mG allora
        led_blu.on()
    else:
        led_blu.off() 
    if abs(accelerometro.z()) > 700 : # Se il valore assoluto dell'accelerazione sull'asse Z è maggiore di 700 mG allora
        led_rosso.on()
    else:
        led_rosso.off() 
```

## Altri sensori

**È possibile utilizzare gli altri sensori sulla scheda di espansione grazie alle librerie e agli script di esempio disponibili in [area download](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)**

Nota, i seguenti 3 sensori vengono utilizzati per costruire le basi di una stazione meteorologica collegata.

*  STTS751 : Termometro (–40 °C to +125 °C) 
*  HTS221 : Sensore di umidità relativa
*  LPS22HH : Barometro (260-1260 hPa)


## Elementi di base dei sensori:  

In questa sezione sono elencati per ogni sensore: 
* Importazioni di librerie e dichiarazione del nome del sensore che deve essere all'inizio del programma
* Funzioni che consentono l'accesso ai valori di misura


### LSM6DSO - accelerometro e giroscopio 3D

Dichiarazioni iniziali:  
``` python  
from machine import I2C
import LSM6DSO

i2c = I2C(1)	# L'I2C n ° 1 della scheda NUCLEO-W55 serve per comunicare con il sensore
intertial_sensor = LSM6DSO.LSM6DSO(i2c)	# il nome del sensore
```

Mesures :  
``` python  
intertial_sensor.ax()	# risultato in mg (g intensità di gravità)
intertial_sensor.ay()
intertial_sensor.az()

intertial_sensor.gx()	# risultato in rad / s
intertial_sensor.gy()
intertial_sensor.gz()
```  


### LIS2MDL - Magnetometro 3D

Dichiarazioni iniziali : 
``` python  
from machine import I2C
import LIS2MDL

i2c = I2C(1)	# L'I2C n ° 1 della scheda NUCLEO-W55 serve per comunicare con il sensore
bussola = LIS2MDL.LIS2MDL(i2c)		# il nome del sensore
``` 

Mesures :  
``` python
bussola.x()	# risultato in µT
bussola.y()
bussola.z()
```  


### LPS22HH - Barometro (260-1260 hPa) e sensore di temperatura 

Dichiarazioni iniziali : 
``` python  
from machine import I2C
import LIS2MDL

i2c = I2C(1)	# L'I2C n ° 1 della scheda NUCLEO-W55 serve per comunicare con il sensore
bussola = LIS2MDL.LIS2MDL(i2c)		# il nome del sensore
``` 

Le misure:  
``` python
sensor.temperature()	# risultato in ° C
sensor.pressione()		# risultato in hPa
```  


### HTS221 - Sensore di temperatura e umidità relativa 

Dichiarazioni iniziali : 
``` python  
from machine import I2C
import HTS221

i2c = I2C(1)	# L'I2C n ° 1 della scheda NUCLEO-W55 serve per comunicare con il sensore
sensor = HTS221.HTS221(i2c)		# il nome del sensore
```  

Le misure:  
``` python  
sensor.temperatura()	# risultato in ° C
sensor.umidità()		# risultato in%
```  


### STTS751 - Termometro (–40 °C à +125 °C)  

Dichiarazioni iniziali :  
``` python  
from machine import I2C
import STTS751

i2c = I2C(1)	# L'I2C n ° 1 della scheda NUCLEO-W55 serve per comunicare con il sensore
sensor = STTS751.STTS751(i2c)	# il nome del sensore
```
Le misure:  
``` python  
sensor.temperatura()	# risultato in ° C
```  