---
title: Comunicazione in BLE con la scheda da uno smartphone 
description: Comunicazione in BLE con le schede da uno smartphone

---
# Comunicazione in BLE con le schede da uno smartphone






In questa parte viene illustrato come comunicare in Bluetooth Low Power con l'applicazione STBLESensor e la scheda di sviluppo WB55.


## Installazione del sensore ST BLE sul tuo smartphone


Installa l'applicazione ST BLE Sensor sul tuo smartphone su [Google Play](https://play.google.com/store/apps/details?id=com.st.bluems) o [IOS Store](https://apps.apple.com/it/app/st-ble-sensor/id993670214)

![image](images/stmblesensorapp.png) ![Android](images/stmblesensorapp-qr-android.png)   ![iOS](images/stmblesensorapp-qr-ios.png)


Di seguito la descrizione completa dei diversi servizi offerti dall'applicazione STBLESensor :

[https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)


## Comunicazione BLE in MicroPython

Per comunicare in Bluetooth Low Energy con micropython, sarà necessario includere 2 nuovi file nella directory del disco usb “PYBFLASH” :
1. [ble_advertising.py](../../assets/Script/BLE/ble_advertising.py) (File della guida per la creazione del messaggio di avviso)
2. [ble_sensor.py](../../assets/Script/BLE/ble_sensor.py)  (Classe utilizzata per gestire la connessione BLE)
*  Dovrai scaricare gli script necessari per questo esempio [ici](../../assets/Script/BLE/Script_BLE.zip)

Grazie al file ble_sensor.py, potremo creare un oggetto BLE con 1 servizio e 2 caratteristiche.



_Questo file dovrà essere modificato per cambiare il profilo BLE, se necessario._

Una volta avviato lo script, il kit di sviluppo WB55 inizia a emettere frame BLE, chiamati "advertising". Questi messaggi identificano l'oggetto Bluetooth e indicano che il dispositivo è pronto per essere connesso.

Il nome del dispositivo è: "WB55-MPY" Verificheremo con l'applicazione per smartphone se la scheda WB55 è in trasmissione bluetooth.


## uso

Avvia l'applicazione STBLESensor sul tuo smartphone :

![image](images/app1.png)


Quindi premere l'icona della lente di ingrandimento per visualizzare i dispositivi BLE circostanti :

![image](images/app2.png)



In questo esempio, il profilo BLE che abbiamo scelto ci permette di simulare un termometro e di accendere o spegnere un LED. Il valore del termometro viene generato casualmente ogni secondo.



Connettiti alla scheda di sviluppo premendo "WB55-MPY":

![image](images/app3.png)

_Il LED blu sulla scheda WB55 dovrebbe accendersi quando è collegata all'applicazione._

Possiamo osservare in questa schermata, l'evoluzione casuale della temperatura tra 0 e 100,0 ° C.

È possibile visualizzare la temperatura in modalità grafica.

A tale scopo, premere il pulsante menu ![image](images/app-menu.png) :

![image](images/app4.png)

Ora premi![image](images/app-plot.png) :

![image](images/app5.png)

Per visualizzare il grafico, premere ![image](images/app-play.png) :

![image](images/app6.png)

Puoi usare il pulsante ![image](images/app-options.png) per modificare le opzioni del grafico, come la dimensione dell'asse X o abilitare il cambio automatico della scala Y.


Studieremo ora l'invio di informazioni dallo SmartPhone alla piattaforma WB55.
Per questo usiamo l'applicazione per accendere o spegnere un LED rosso nel kit di sviluppo.

A tale scopo premere il pulsante menu ![image](images/app-menu.png) :

![image](images/app7.png)

Ora scegli l'opzione ![image](images/app-switch.png) :

![image](images/app8.png)

In questa schermata puoi controllare il LED rosso del kit di sviluppo.