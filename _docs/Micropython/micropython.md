---
title: µPython
description: µPython

---
# MicroPython (µPython)

<img align="center" src="images/upython.png" alt="drawing" width="100"/>

MicroPython è un'implementazione leggera ed efficiente del linguaggio di programmazione Python 3 che include un piccolo sottoinsieme della libreria Python standard ottimizzata per funzionare su microcontrollori e in ambienti vincolati.

MicroPython è ricco di funzionalità avanzate come guest da riga di comando, numeri interi di precisione arbitraria, chiusure, comprensione di elenchi, generatori, gestione delle eccezioni e altro ancora. Tuttavia, è abbastanza compatto da adattarsi e funzionare in soli 256 KB di spazio codice e 16 KB di RAM.

* [Sito web](http://micropython.org/)
* [Biblioteche](http://docs.micropython.org/en/latest/library/index.html)
