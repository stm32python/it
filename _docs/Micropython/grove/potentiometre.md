---
title: Tutorial Potenziometro
---

# Tutorial per l'utilizzo del potenziometro in MicroPython con una scheda STM32WB55

- **Prerequisiti :**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:

Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Notepad++).

Per questo sensore deve essere collegato al **pin A1**.

- **Il potenziometro:**

![Image](images/potentiometre.png)

Il potenziometro, *rotary angle sensor* in inglese, permette di modulare manualmente una tensione di ingresso compresa tra 0V e + 3.3V (per la nostra scheda). Dopo la conversione di questa tensione (dall'ADC del pin analogico a cui è collegato il potenziometro) il microcontrollore riceve un valore intero compreso tra 0 e 4095. Questo valore può quindi essere utilizzato per controllare altri attuatori, ad esempio per variare il volume di un buzzer


Codice MicroPython :
```python
import pyb
from pyb import Pin, ADC
import time

adc = ADC(Pin('A1'))
while(True):
	print(adc.read()) # read value, 0-4095
	time.sleep(1)      # sleep for 500 milliseconds

````
