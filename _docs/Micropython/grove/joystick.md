---
title: Tutorial Joystick
---

# Tutorial per l'utilizzo del sensore joystick in MicroPython con una scheda STM32WB55

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png) 
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:


Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Notepad++).

Per questo sensore deve essere collegato al **pin A0**.

- **Il joystick-pollice:**


![Image](images/joystick.png)


Questo joystick è simile a quelli che si possono trovare su un controller PS2. Ciascuno degli assi è collegato a un potenziometro che fornirà una tensione corrispondente. Inoltre, il joystick ha un pulsante. Il codice permette di controllare tutti gli stati che il joystick può assumere. Possiamo controllare il nostro input nell'interprete Python.


Code MicroPython :
```python
import pyb
import time
from pyb import Pin

vertical = ADC(PIN('A0'))            #en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
horizontal= ADC(PIN('A1'))           #sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3..

while True :
    time_sleep_ms(500)
    x=vertical.read()
    y=horizontal.read()

    if (x<=780 && x>=750) :
        print("Haut")
    if (x<=280 && x>=240) :
        print("Bas")
    if (y<=780 && y>=750) :
        print("Gauche")
    if (y<=280 && y>=240) :
        print("Droite")
    if (x>=1000) :                #en appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
        print("Appuyé")
```

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
