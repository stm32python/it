---
title: Esercizio con il sensore di movimento PIR *(sensore di movimento PIR)* in MicroPython
description: Esercizio con il sensore di movimento PIR *(sensore di movimento PIR)* in MicroPython
---

# Esercizio con il sensore di movimento PIR in MicroPython

- **Prerequisiti :**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)

Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:

![Image](images/pybflash.png)

Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Putty).

Per questo sensore deve essere collegato al **pin D4**.

- **Sensore di movimento (PIR):**

Il sensore che utilizzeremo è in grado di rilevare il movimento. Rileva la radiazione infrarossa nel suo campo visivo e ne deduce una presenza o un movimento. Questo sensore può essere utilizzato in molte applicazioni, in particolare in un sistema di allarme (Cf Tuto Alarme) .

![Image](images/grove-pir_motion.jpg)
Per il codice, quando il sensore rileva un movimento, viene attivato un interrupt. Il messaggio "Rilevato movimento" verrà visualizzato nell'interprete Python e il LED della scheda si accenderà per un periodo di tempo. Quindi torniamo allo stato iniziale.

Codice MicroPython :
```python
from machine import Pin
from time import sleep

motion = False

def handle_interrupt(pin):
  global motion
  motion = True
  global interrupt_pin
  interrupt_pin = pin

led = Pin('LED', Pin.OUT)
pir = Pin('D4', Pin.IN)

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
  if motion:
    print('Motion detected!')
    #print('Motion detected! Interrupt caused by:', interrupt_pin)
    led.value(1)
    sleep(5)
    led.value(0)
    print('Motion stopped!')
    motion = False
```

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
