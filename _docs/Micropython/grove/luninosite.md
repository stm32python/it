---
title: Tutorial Luminosità
---

# Tutorial per l'utilizzo del sensore di luminosità in MicroPython con una scheda STM32WB55

- **Prerequisiti :**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.
![Image](images/shield.png)
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:


Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Notepad++).

Per questo sensore deve essere collegato al **pin A1**.

- **Sensore di luminosità**

![Image](images/capteur-de-lumiere.png)

Questo sensore utilizza una fotoresistenza per rilevare l'intensità della luce dell'ambiente circostante. Il valore di questa fotoresistenza diminuisce quando è illuminata.
Codice MicroPython :
```python
import pyb
from pyb import Pin, ADC
import time

adc = ADC(Pin('A1'))
while(True):
	print(adc.read()) # read value, 0-4095
	time.sleep(1)      # sleep for 500 milliseconds

```
Un valore verrà visualizzato nell'interprete Python ogni 0,5 secondi..

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
