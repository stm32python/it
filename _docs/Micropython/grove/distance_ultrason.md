---
title: Esercizio con il sensore di distanza a ultrasuoni Grove in MicroPython
description: Esercizio con il sensore di distanza a ultrasuoni Grove in MicroPython
---

# Esercizio con il sensore di distanza a ultrasuoni Grove in MicroPython

<div align="center">
<img alt="Grove - GPS" src="images/grove-ultrason.jpg" width="400px">
</div>

In arrivo

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
