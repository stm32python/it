---
title: Tutoriel Buzzer
---

# Tutorial per l'utilizzo del sensore Buzzer in MicroPython con una scheda STM32WB55¶

**Stesso prerequisito del sensore di inclinazione..**

Il sensore va collegato al pin D3..

**Il Buzzer :**

Il buzzer vibra e produce un suono quando la tensione gli viene trasmessa. È possibile modificare la frequenza del suono.


![Image](images/buzzer.png)
Il codice consiste nel far leggere al cicalino la tabella delle *frequenze* contenente una serie di note. 

Code MicroPython :
```python
from pyb import Pin, Timer

frequency = [262, 294, 330, 349, 370, 392, 440, 494]

BUZZER = Pin('D3') # D3 has TIM1, CH3
while 1 :
    for i in range (0,7) :
        tim1 = Timer(1, freq=frequency[i])
        ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
        ch3.pulse_width_percent(5)

```

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
