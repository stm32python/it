---
title: Tutorial Sensore Touch
---

# Tutorial per l'utilizzo del sensore tattile in MicroPython con una scheda STM32WB55¶

- **Prerequisiti :**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Pour écrire et exécuter un script MicroPython, rendez vous dans le fichier **main.py** se trouvant dans le lecteur **PYBFLASH** de votre carte :


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:


- **Sensore tattile(Touch sensor):**

Questo sensore ha solo 3 pin che devono essere collegati a GND e VCC (alimentazione, cavo rosso e nero) e D4 (segnale, cavo giallo).

![Image](images/capteur_tactile.png)

Questo sensore si comporta come un interruttore; sarà in stato ON se c'è contatto e in stato OFF se non c'è contatto.

Ecco il codice in MicroPython:
```python

import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")
```
