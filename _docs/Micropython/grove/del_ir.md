---
title: Tutoriel LED Infrarouge
---

# Tutorial per l'utilizzo di un LED a infrarossi in MicroPython con una scheda STM32WB55¶

Questo tutorial funziona per qualsiasi tipo di LED, ma qui utilizziamo l'esempio di un LED a infrarossi.

- **Prerequisiti :**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. Collegare il LED allo schermo sul pin D4. I pin Dx permettono l'elaborazione digitale del segnale (0 o 1) mentre i pin Ax gestiscono i segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:


Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Notepad++).
- **Lampeggio del LED IR :**


Il codice proposto è molto semplice e consiste nel far lampeggiare il nostro LED in un loop dopo un'inizializzazione del pin D4.

*Nota*: se il LED utilizzato è un LED a infrarossi, è possibile verificare che stia lampeggiando bene con la fotocamera del telefono!

Code MicroPython :
```python
import pyb
from pyb import Pin
import time

led = Pin('D4', Pin.OUT_PP)

while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds
	led.off()
	print("LED OFF")
	time.sleep(1)           # sleep for 1 second
	led.on()
	print("LED ON")
```

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
