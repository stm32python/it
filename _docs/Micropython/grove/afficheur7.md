---
titolo: Tutorial 7 display a 7 segmenti
descrizione: Tutorial basato su un display a 7 segmenti e un driver TM1637
---
# Tutorial display a 7 segmenti

<div align="center">
<img alt="Grove - 4-Digit Display" src="images/grove-4_digit_display.jpg" width="400px">
</div>

Per utilizzare il display sono necessarie le librerie  **pyb** e **tm1637**.
pyb  è già presente, è necessario installare tm1637. Spostare il file**tm1637.py** nella cartella dove si trova **main.py**.
La biblioteca è installata! Ora è possibile utilizzare queste due biblioteche con :
```
from pyb import Pin
import tm1637
```
Ora impostate il vostro accesso al display su A0 con la seguente istruzione:

```
tm = tm1637.TM1637(clk=Pin('A0'), dio=Pin('A1'))
```

Ora è possibile modificare la visualizzazione con :
```
tm.write([<case1>, <case2>, <case3>, <case4>])
```
È possibile controllare il display a 7 segmenti in modo indipendente con una parola binaria della forma 0bABCDEFG. Basta impostare un 1 o uno 0 a seconda che si voglia attivare o disattivare il segmento.
Ad esempio, per visualizzare 0123, scrivere :  
```
tm.write([0b0111111, 0b0000110, 0b1011011, 0b1001111])
```

![Etichetta display a 7 segmenti ](images/300px-7_segment_display_labeled.jpg)



> Credito immagine : [Wikipedia](https://fr.wikipedia.org/wiki/Fichier:7_segment_display_labeled.svg), [Seedstudio](http://wiki.seeedstudio.com/Grove-4-Digit_Display/)
