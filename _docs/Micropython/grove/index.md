---
title: Esercizi MicroPython con sensori Grove
description: Esercizi MicroPython con sensori Grove

---
# Esercizi MicroPython con sensori Grove

Avrai bisogno della scheda di espansione Grove per completare questi esercizi.

![Image](images/shield.png)
![Image](images/shield_carte.png)

## Inizio

La scheda di espansione Grove è una scheda in formato Arduino che consente di collegare sensori e attuatori digitali, analogici, UART e I2C utilizzando la connessione [Grove](http://wiki.seeedstudio.com/Grove_System/).

**I codici sorgente degli esercizi Groves sono tutti disponibili [nell'area download](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)**

## Esercizi con i sensori Grove

Qui troverai alcuni esercizi per i [principali sensori in formato Grove](http://wiki.seeedstudio.com/Grove_System/).

 - [LED a infrarossi](del_ir)
 - [Potenziometro](potentiometre)
 - [Luminosità](luninosite)
 - [Display a 7 segmenti](afficheur7)
 - [Allarme](alarme)
 - [Buzzer](buzzer)
 - [Movimento](mouvement)
 - [Joystick](joystick)
 - [Rumore](bruit)
 - [Shock (Inclinazione)](choc)
 - [Sensore tattile](toucher)
 - [Display LCD a 16 caratteri x 2 righe](lcd_16x2)
 - [Distanza per ultrasuoni](distance_ultrason) (in arrivo)
 - [Module UART GPS SIM28](gps) (in arrivo)
 - [Nintendo Nunchuk](nunchuk)
 - [Sonda di temperatura impermeabile DS1820](ds1820)

> Credito immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System)
