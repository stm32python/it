---
title: Esercizio con l'adattatore Nintendo Nunchuk Grove in MicroPython
description: Esercizio con l'adattatore Nintendo Nunchuk Grove in MicroPython
---

# Esercitati con l'adattatore Nintendo Nunchuk Grove in MicroPython¶


<h2>Descrizione/h2>
Il Nunchuk è un controller aggiuntivo che si collega al WiiMote (un controller progettato per essere utilizzato per la console Nintendo Wii). 
 Il Nunchuk è composto dalle seguenti caratteristiche: :
 - Joystick a 2 assi (x, y)
 - Accelerometro a 3 assi ( (x, y, z)
 - Pulsanti (C et Z)

Comunica in collegamento [I2C](https://stm32python.gitlab.io/fr/docs/Micropython/glossaire) Una modalità di comunicazione che richiede solo 4 fili (5V, GND, SDA, SCL).


<h2>Montaggio</h2>

Per utilizzare il controller utilizziamo un connettore UEXT to Grove come questo:
<div align="center">
<img alt="Grove - Nintendo Nunchuk Adapter" src="images/grove-nunchuk.jpg" width="200px">
</div>

Poi si deve collegare il regolatore ad un connettore I2C sulla scheda Grove.


<h2>Programma</h2>
Per semplificare il codice utilizziamo una libreria esterna. La libreria è disponibile seguendo questo [link](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/wiichuck.py). Le fichier *main* est quant à lui disponible via ce [lien](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/main.py).

**Passaggio 1 :** per far funzionare il programma, dobbiamo prima importare 3 librerie. Per fare ciò, dobbiamo importarli all'inizio del nostro codice in questo modo:
```
from machine import I2C, Pin
from time import sleep_ms
from wiichuck import WiiChuck
```

**Passaggio 2 :** quindi devono essere inizializzati aggiungendo queste righe di codici. La prima riga specifica quale canale I2C stiamo usando (nel nostro caso stiamo usando il canale 1). La seconda riga di codice viene utilizzata per definire una variabile*wii* che recupererà le informazioni restituite dalla libreria.
```
i2c = I2C(1)
wii = WiiChuck(i2c)
```

**Passaggio3 :**  Infine visualizziamo le informazioni restituite dal controller sotto forma di tabella. Questa tabella verrà visualizzata nel nostro terminale e verrà eseguita in un ciclo infinito (quindi ricordati di fare ctrl + c se vuoi fermare la visualizzazione).
```
while True:
	direction = ''
	if wii.joy_up:
		direction = 'Haut'
	elif wii.joy_down:
		direction = 'Bas'
	elif wii.joy_right:
		direction = 'Droite'
	elif wii.joy_left:
		direction = 'Gauche'
	else:
		direction = '-----'
	if(wii.c):
		Cbouton = 'C'
	else:
		Cbouton = '-'
	if(wii.z):
		Zbouton = 'Z'
	else:
		Zbouton = '-'

	print("Joystick: (%3d, %3d) %6s \t| Accelerometre XYZ: (%3d, %3d, %3d) \t| Boutons: %s %s" %(wii.joy_x, wii.joy_y, direction, wii.accel_x, wii.accel_y, wii.accel_z, Cbouton, Zbouton))

	wii.update()
	sleep_ms(100)
```


<h2>Risultato</h2>

Ecco fatto! Possiamo ora osservare il risultato con i dati estratti dal Nunchuk in questa forma :


<div align="center">
<img alt="Affichage des données du Nunchuk" src="images/nunchuk-python.png" width="800px">
</div>


Fai un movimento con il Nunchuk e dovresti vedere i valori dell'accelerometro variare a seconda dell'asse su cui l'hai spostato. Puoi anche muovere il joystick e vedere dove si trova. Inoltre hai un feedback sullo stato dei pulsanti.


<h2>Per andare oltre</h2>

In questo esercizio abbiamo visto la visualizzazione dei dati del Nunchuk. Per andare oltre possiamo, ad esempio, recuperare questi dati ed elaborarli per attivare i servo motori secondo la direzione del joystick.

(prossimamente)


> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
