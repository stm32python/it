---
title: Esercizio con il modulo GPS SIM28 Grove in MicroPython
description: Esercizio con il modulo GPS SIM28 Grove in MicroPython
---

# Esercitazione con il modulo GPS SIM28 Grove in MicroPython

<div align="center">
<img alt="Grove - GPS" src="images/grove-gps.jpg" width="400px">
</div>

In arrivo

> Credito immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System)
