---
title: Tutoriel Allarme
---

# Tutorial di programmazione degli allarmi MicroPython con una scheda STM32WB55

Questo tutorial è un esempio di esercizio da fare. Utilizzeremo in parallelo il **buzzer**  il sensore di movimento  **PIR motion sensor**.
L'obiettivo è semplice: se viene rilevato un movimento, il buzzer produce un suono..

- **Prerequisiti :**

Fare riferimento ai tutorial del buzzer e del sensore di movimento per maggiori dettagli.
Collegare il buzzer a **D3** il sensore a **D2**

- **Programmazione di un allarme:**

Se viene rilevato un movimento, si attiva l'interruzione che provoca l'attivazione del led e del buzzer.


Codice MicroPython :
```python
from machine import Pin
from time import sleep
from pyb import Pin, Timer

motion = False

frequency = 440

BUZZER = Pin('D3') # D4 has TIM3, CH2
tim1 = Timer(1, freq=frequency)
ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)

def handle_interrupt(pin):
  global motion
  motion = True
  global interrupt_pin
  interrupt_pin = pin

led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)
pir = Pin('D2', Pin.IN)

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
  if motion:
    print('Motion detected!')
    #print('Motion detected! Interrupt caused by:', interrupt_pin)
    led_rouge.on()
    led_bleu.off()
    ch3.pulse_width_percent(5)
    sleep(1)
    led_bleu.on()
    led_rouge.off()
    ch3.pulse_width_percent(0)
    print('Motion stopped!')
    motion = False
```

> Credito immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
