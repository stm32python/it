---
title: Esercizio con il display LCD 16x2 Grove in MicroPython
description: Esercizio con il display LCD 16x2 Grove in MicroPython
---
# Esercizio con il display LCD Grove 16x2 in MicroPython¶

<div align="center">
<img alt="Grove - LCD 16x2 Display" src="images/grove-lcd_16x2_display.jpg" width="600px">
</div>

**Attrezzature**
- NUCLEO-WB55
- Scheda Grove Base Shield
- LCD RGB Grove
- Scheda IKS01A3

**Obiettivo dell'esercizio**
- Misurare la temperatura (in gradi Celsius) dell'aria ambiente ogni secondo
- Visualizzare la temperatura sull'LCD Grove RGB e sul terminale USB USER
- Regolare il colore di sfondo del display LCD e accendere i LED in base alla temperatura

**Messa in opera**
- Collegare la Grove Base Shield per la scheda Arduino al NUCLEO-WB55
- Collegare il display a **I2C1**
- Collegare la scheda IKS01A3
- Copiare i file (librerie) i2c_lcd_screen.py, i2c_lcd_backlight.py, i2c_lcd e STTS751 sulla scheda NUCLEO
- Copiare il seguente codice nel file main.py (codice sorgente disponibile nell'area download)

**Codice sorgente**
```python
from machine import I2C
import STTS751
import pyb
import time
import i2c_lcd

i2c = I2C(1) # On utilise l'I2C n°1 de la carte NUCLEO-W55 pour communiquer avec le capteur
sensor = STTS751.STTS751(i2c)

#Instance de la classe Display
lcd = i2c_lcd.Display(i2c)
lcd.home()

led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while(1):
   
    temp = round(sensor.temperature(),1)
 
    print("Temperature : " + str(temp) + "°C (", end='')
  
    lcd.move(0,0)
    lcd.write('Temperature (C)')
    lcd.move(0,1)
    lcd.write(str(temp))
    
    if temp > 25 :
        led_rouge.on()
        print("chaud)")
        lcd.color(255,0,0) #Back light du LCD : rouge
    elif temp > 18 and temp <= 25 :
        led_vert.on()
        print("confortable)")
        lcd.color(0,255,0) #Back light du LCD : verte
    else:
        led_bleu.on()
        print("froid)")
        lcd.color(0,0,255) #Back light du LCD : bleue

    led_rouge.off()
    led_vert.off()
    led_bleu.off()
    
    time.sleep_ms(1000)
```

> Credito immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System)
