---
title: Tutoriel Sound-Sensor
---

# Tutorial per l'utilizzo del sensore di suono in MicroPython con una scheda STM32WB55¶
- **Prerequisiti:**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. Collegare questo sensore allo schermo sul pin D4. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore **PYBFLASH** della tua scheda:


Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprire questo file con un editor di script (es: Notepad++).


Per questo sensore deve essere collegato al **pin A1**.

- **Sensore del suono:**

![Image](images/capteur_sonore.png)

Questo sensore può essere utilizzato come rilevatore di rumore, infatti la sua uscita è proporzionale al livello sonoro circostante.

Code MicroPython :
```python
import pyb
import time
from pyb import Pin

adc=ADC(PIN('A1'))

while True :
    time_sleep_ms(500)
    print(adc.read())

```

Une valeur sera affichiée dans l'interpréteur Python toutes les 0.5s.

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
