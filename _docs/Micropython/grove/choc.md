---
title: Tutoriel Tilt Sensor
---

# Tutorial per l'utilizzo del sensore di inclinazione in MicroPython con una scheda STM32WB55

- **Prerequisiti:**

Il kit del sensore fornisce una schermatura (immagine sotto) che si adatta alla scheda e semplifica il collegamento dei sensori. Basta collegarlo alla scheda. I pin Dx consentono l'elaborazione digitale del segnale (0 o 1) oppure i pin Ax gestiscono segnali analogici.

![Image](images/shield.png)
![Image](images/shield_carte.png)


Per scrivere ed eseguire uno script MicroPython, vai al file **main.py** che si trova nel lettore**PYBFLASH** della tua scheda:

Il file main.py verrà eseguito di default all'avvio di MicroPython. Aprite questo file con il vostro editor di script (es: Notepad++).

Per questo sensore deve essere collegato al **pin D4**.

- **Sensore di inclinazione :**

![Image](images/tiltsensorim.png)

•	Il sensore di inclinazione è un sensore che cambia stato quando l'inclinazione rispetto alla verticale supera un dato valore limite (la sfera nel sensore segue il suo movimento, rotola e fa contatto). Questo sensore può quindi essere in due stati (denominato ON e OFF nel codice)..

Codice MicroPython :
```python
import pyb
import time
from pyb import Pin

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)


while True :
	time.sleep_ms(500)      # sleep for 500 milliseconds

	print(p_in.value()) # get value, 0 or 1

	if(p_in.value() == 1):
		print("ON")
	else:
		print("OFF")
```

> Credito immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
