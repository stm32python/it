---
title: Domande frequenti
description: Domande frequenti per risolvere i tuoi problemi

---
# Domande frequenti

<html>
    <ol type="1">
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Cosa fare quando una scheda non viene più rilevata improvvisamente da Windows?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Verificare il cablaggio.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Prova un'altra porta USB.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Cambia cavo.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Eseguire nuovamente il flashing della scheda, vedere "Guida di avvio di Linux o Windows 10».</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Cosa fare quando Windows dichiara i file come "file corrotti"? Inoltre, non è più possibile inserire un file sulla scheda, riscriverne uno e persino modificarlo.</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Ciò accade in rari casi quando la carta non è stata utilizzata "correttamente". Ecco alcuni suggerimenti:
                    <ol type="i">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Usa Thonny o Geany (scaricabile dalla rete).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Verificare che venga rilevata la porta COM corretta: <i>Tool</i> -> <i>Option</i> -> <i>Interpreter</i> poi vai avanti <i>Micro Python</i> e <i>Port COM xx</i>.</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Apri il file dall'editor di testo. <i>File</i> -> <i>Open</i> quindi seleziona « <i>This Computer</i> »quindi utilizzare il browser per trovare i file sulla scheda (PYBFLASH : ).</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;Utilizzare Ctrl + D per il riavvio graduale, utilizzare solo il ripristino hardware (pulsante di ripristino sulla scheda) in caso di funzionamento non software.</li>
                    </ol>
                </li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Devi refleshare la scheda.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Cosa fare quando i sensori sulla scheda di espansione IKS01-A3 ritornano solo « 0 » ?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Verifica le connessioni.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Evitare di spingere troppo i connettori.</li>
            </ol>
        </li><br><br>
        <li><b>&nbsp;&nbsp;&nbsp;&nbsp;Cosa fare quando non succede nulla con lo scudo Grove?</b>
            <ol type="a">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Verificare i collegamenti.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;Assicurarsi che l'interruttore in basso a sinistra dello schermo (sotto il connettore A0) sia su 3,3 V.</li>
            </ol>
        </li><br><br>
    </ol>
</html>