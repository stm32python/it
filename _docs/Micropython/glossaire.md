---
title: Glossaire
description: Glossaire

---
# Glossaire

**GPIO** : General Purpose Input Output (IT :Porta di ingresso).

**LED** : Luminescent Electronic Diode_ (IT: Diodo ad emissione luminosa).

**ADC** : Analog to Digital Converter (IT : Convertitore da analogico a digitale).

**DAC** : Digital to Analog Converter (IT : Convertisseur Numérique vers Analogique).

**TIM** : Timer (IT : Orologio).

**UART** : Universal Asynchronous Receiver Transmitter (IT : Ricevitore Trasmettitore asincrono universale).

**I2C** : Inter-Integrated Circuit (IT : _ bus di comunicazione_ intercircuito integrato).

**SPI** : Serial Peripheral Interface (IT: Interfaccia seriale per periferiche).

**LCD** : Liquid Crystal Display(IT: schermo a cristalli liquidi).

**OLED** : Organic Luminescent Electronic Diode (IT: Diodo elettro-luminescente organico) .

**µC** : Microcontrollore.

**ALU** : Arithmetic and Logic Unit (IT : Unità di funzionamento aritmetica e logica)

**BLE** : Bluetooth Low Energy (IT : Bluetooth a basso consumo energetico).

**Cortex M4** : Tipo di core di un microcontrollore sviluppato dalla società ARM.

**RAM** : Tipo di memoria volatile in cui vengono inserite le variabili. 

**FLASH** :Tipo di memoria non volatile in cui viene inserito il codice compilato.

**ST-LINK V2** :Programmatore ST per trasferire il codice nella memoria FLASH e RAM di un microcontrollore.

**NUCLEO** : Nome commerciale dato a schede di sviluppo prodotte da STMicroelectronics.

**Morpho** : Tipo di connettore ARDUINO.

**Arduino** : Ecosistema educativo di schede elettroniche interconnettibili.

**Firmware** : Nome assegnato al file di output di un programma compilato.

**Bootloader** : Sistema di avvio che consente l'aggiornamento del firmware.

**DFU** : Device Firmware Update(IT: Mise à jour firmware du système).
