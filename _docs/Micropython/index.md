---
title: µPython e STM32WB
description: µPython e STM32WB

---
# MicroPython e STM32WB

<p>
<img align="center" src="images/upython.png" alt="upython" width="100"/>
<img align="center" src="images/st2.png" alt="stm" width="250"/>
</p>

Troverai in questa parte tutti i tutorial di MicroPython.

Prima di tutto, assicurati di avere un'installazione funzionale.
Il protocollo da seguire è dettagliato nelle 2 guide di avvio.

## Sommario

* [Cos'è MicroPython?](micropython)
* [Guida rapida di Windows 10](install_win10)
* [Guida rapida di Linux](install_linux)
* [Il kit di sviluppo STM32WB55](stm32wb55)
* [Esempi di base di utilizzo di MicroPython](exercices)
* [Esempi di utilizzo di MicroPython con la scheda di espansione IKS01A3](iks01a3)
* [Esempi di utilizzo di MicroPython con sensori Grove](grove)
* [Utilizzo di BLE con uno smartphone](ble)
* [Creazione di un'applicazione per smartphone con MIT App Inventor](AppInventor)
* [Cattura e registra un file audio su una scheda micro-SD](microphone)
* [Programmazione di un mini-robot e controllo dei suoi motori DC da uno smartphone](prossimamente)
* [Domande frequenti](faq)
* [Glossaire](glossaire)
* [Link utili](ressources)
