---
title: Risorse
description: Risorse

---
# Risorse

## Documentazione

**Sito ufficiale di MicroPython :** 

[https://micropython.org/](https://micropython.org/)

**Documentazione generale di MicroPython :** 

[http://docs.micropython.org/en/latest/](http://docs.micropython.org/en/latest/)

**Esempio di utilizzo della libreria pyb (molto utile) :**

_Attenzione il pinout è diverso dalla scheda pyB V1.1: _

[http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control](http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control)

** Codice sorgente del progetto MicroPython: **

[https://github.com/micropython/micropython](https://github.com/micropython/micropython)

**Sito ufficiale di Python 3 :**

[https://www.python.org/](https://www.python.org/)

**Documentazione tecnica NUCLEO-WB55:** 

[https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

**Documentazione dell'applicazione BLESTSensor Android :**

[https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)


## Pinout dal kit NUCLEO-WB55


Ecco la corrispondenza dei connettori con il numero di pin del microcontrollore:

![Tableau](images/pins.png)


## Descrizione pin STM32

![Tableau](images/tableau1.png)

![Tableau](images/tableau2.png)
