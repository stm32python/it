---
title: Guida rapida di Windows 10
description: Guida rapida di Windows 10

---
# Guida rapida di Windows 10

> Questi passaggi devono essere eseguiti dall'insegnante con accesso amministratore sotto Windows 10, per ciascuna scheda NUCLEO WB55 utilizzata nel lavoro pratico.

In questa guida introduttiva, faremo:
- Programmare la scheda con il firmware
- Utilizzare un terminale di comunicazione UART (TeraTerm o PUTTY)
- Programma il nostro primo script Python su Nucleo

## Configurazione elettronica

**Avrai bisogno di un cavo da USB a micro USB.**

Questa configurazione si riduce a:

- Spostare o assicurarsi che il ponticello sia impostato su ***USB_STL***
- Collega un cavo micro USB alla porta ***ST_LINK*** in ***sotto LED4 e LED5***

Ecco come deve essere configurato il kit :

![image](images/Nucleo_WB55_Config_STL.png)

Queste modifiche consentono di configurare STM32WB55 in modalità di programmazione STL_Link..<br>
Potremo aggiornare il firmware (file binario) tramite USB_ST_Link.

## Programmazione della scheda

Scarica e archivia il file [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin)

Collega la scheda NUCLEO-WB55 (USB_STLINK) con il cavo USB al tuo computer.

### Installazione del firmware

Apri lettore ***NOD_WB55***

![image](images/Nod_wb55.PNG)

Trascina e rilascia il file[Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) (fichier .bin).<br>
Il firmware è ora integrato (non è visibile nel lettore NOD_WB55)

Posiziona il ponticello ***USB_MCU*** così come il cavo micro USB sul connettore ***USB_USER*** sotto il ***pulsante RESET***

![image](images/Nucleo_WB55_Config_MCU.png)

***Riavvia la scheda Nucleo***

La tua scheda Nucleo è ora in grado di interpretare un file .py

Diamo un'occhiata ai file generati dal sistema MicroPython con Windows Explorer:

Apri il dispositivo PYBFLASH con Windows Explorer.

![Image](images/dfu_util_3.png)

Vedremo più avanti come modificare gli script Python disponibili nel filesystem PYBFLASH.

**Note importanti :** 
Lo script `boot.py`, può essere modificato da utenti avanzati, permette di inizializzare MicroPython e in particolare di scegliere quale script verrà eseguito dopo l'avvio di MicroPython, di default lo script `main.py`.<br>

## Installazione dell'ambiente di programmazione

Sono disponibili tre soluzioni:
- Usa la versione portatile o installa (sono richiesti diritti di amministratore) [Notepad++](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)con evidenziazione della sintassi Python e un terminale seriale ([PuTTY](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement), [TeraTerm](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement)...)
- Installa (diritti di amministratore richiesti) [Thonny](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) e il suo terminale integrato
- Usa la versione portatile di [PyScripter](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) e un terminale seriale ([PuTTY](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement), [TeraTerm](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargementn)...).


## Configurazione del terminale di comunicazione
Ora che MicroPython è presente nel kit NUCLEO-WB55, vorremmo testare la comunicazione con Python. Il test consiste nell'invio di un comando Python e nella verifica che l'esecuzione avvenga.

La comunicazione avviene tramite USB tramite una porta seriale, quindi abbiamo bisogno di un software che possa inviare comandi Python come testo alla scheda e ricevere il risultato dell'esecuzione.

Apri TeraTerm dopo averlo installato (se non hai i diritti di amministratore sul tuo PC, Puttytel è disponibile in versione portatile nell'area download)

Selezionare l'opzione ***Serial*** e il ***port COM*** corrispondente alla tua scheda (**COMx: USB Serial Device**) :<br>
![Image](images/TeraTerm.PNG)<br>
Se la COM corrispondente alla scheda non è disponibile, verificare che il kit sia elencato sulla porta COM corretta, vedere **Verificare l'assegnazione della porta COM**.

Configurare i seguenti campi nel menu ***Setup*** / ***Serial Port***<br>
![Image](images/TeraTerm_Setup.png)


Viene visualizzata una nuova finestra :<br>
![Image](images/TeraTermDisplay_1.png)


Se necessario, premere * CTRL + D * per creare un file **Reset software**.

Ora puoi eseguire i comandi Python.<br>
```python
print("Hello World")
```

![Image](images/TeraTermDisplay_2.png)

Abbiamo ora completato la programmazione del firmware MicroPython.<br>
E possiamo eseguire funzioni Python tramite la porta COM

Il nostro kit di sviluppo NUCLEO-WB55 è ora pronto per l'uso con Python.


Tenere aperta la finestra di TeraTerm, sarà utile per visualizzare l'esecuzione degli script.


## Scrivi ed esegui uno script MicroPython

Vedremo, in questa parte, come modificare con Notepad ++ ed eseguire uno script Python sul nostro NUCLEO-WB55.

Avvia Notepad ++ dopo averlo installato (puoi anche usare la versione portatile)

Questo strumento ti consentirà di modificare i tuoi script Python.

Apri il file ***main.py*** presente sul Nucleo (rappresentato dal lettore PYBFLASH)<br>

Il nostro primo script consisterà nel visualizzare il messaggio 10 volte `MicroPython è fantastico` con il numero del messaggio e tutto questo sul nostro Nucleo.

Scrivi il seguente algoritmo nell'editor di script:
![Image](images/Notepad_test1.png)

Assicurati di salvare il file(*CTRL+S*) ***main.py*** sulla scheda (lettore PYBFLASH)<br>
Esegui un reset software della scheda premendo * CTRL + D * nel tuo terminale seriale (TeraTerm per esempio), lo script viene interpretato direttamente!<br>

![Image](images/TeraTerm_test1.png)

Lo script è stato eseguito con successo! Possiamo vedere il nostro messaggio visualizzato 10 volte nel terminale.<br>
Hai appena scritto il tuo primo script Python incorporato in una scheda NUCLEO-WB55!<br> 

**Note importanti:**
qui c'è un [elenco di librerie che possono essere utilizzate in fase di sviluppo in MicroPython](http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries)<br>

## Appendice: controllare l'assegnazione della porta COM

È stato necessario creare una porta `COM` su Windows.

Scrivi `gestione dispositivi` nella barra di ricerca di Windows, quindi fai clic su `Aprire` :

![image](images/port_COM.png)

Si apre una nuova finestra :

![image](images/gestion_peripheriques.png)

Prendere nota del numero di porta ***COM***. Nell'esempio sopra, il kit NUCLEO-WB55 è collegato alla porta ***COM3***. Con questo numero **COMx** dovrai accedere al tuo terminale seriale (TeraTerm...).