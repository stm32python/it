


---
title: Guida rapida di Windows 10
description: Guida rapida di Windows 10

---
# Guida rapida di Windows 10

> Questi passaggi devono essere eseguiti dall'insegnante con accesso amministratore sotto Windows 10, per ogni scheda NUCLEO WB55 utilizzata nel lavoro pratico. 

In questa guida introduttiva, faremo:
- Installa il driver USB sulla scheda Nucleo (tramite ZADIG)
- Programmare la scheda con la DFU (tramite DFU-UTIL)
- Installare l'ambiente di programmazione sul tuo PC
- Utilizzare un terminale di comunicazione UART (PUTTY)
- Programma il nostro primo script Python su Nucleo

## Configurazione elettronica

**Avrai bisogno di un cavo da USB a micro USB.**

Questa configurazione si riduce a :

- Spostare il jumper su USB_MCU
- Utilizzare il jumper presente su SWDIO e posizionarlo sui Pin 5 e 7 del connettore ARDUINO CN7
- Collegare un cavo micro USB_USER alla porta sotto SW4

Ecco come deve essere configurato il kit :

![image](images/Nucleo_WB55_Config_Programmation.svg)


Queste modifiche consentono di configurare STM32WB55 in modalità bootloader. Potremo aggiornare il firmware tramite USB.

## Programmazione della scheda

Scarica l'ultima versione sul desktop di Windows [Firmware](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) ed estrai la cartella **TP_Python** sulla scrivania

Questo file deve essere presente su ogni postazione per gli studenti.

Ecco l'elenco dei file nella cartella :

![Image](images/dossier_tp_python.png)

Collega la scheda NUCLEO-WB55 (USB_USER) con il cavo USB al tuo computer.

Il primo passo è installare i driver USB gratuiti per comunicare con la scheda STM32WB55. Per questo useremo il software Zadig.

### Installazione del driver USB gratuito (Zadig)

Fare doppio clic sul software ***Zadig***.

![Image](images/autorisation_zadig.png)

Consenti all'app di apportare modifiche facendo clic sul pulsante ***si***.

Ecco l'interfaccia :

![Image](images/zadig_1.png)

Vai a ***Options*** quindi fare clic su * *Elenca tutti i dispositivi*.

![Image](images/zadig_2.png)

Selezionare ***USB DFU in FS mode*** utilizzando l'elenco a discesa.<br>
Se non compare nell'elenco a discesa, cambia il cavo USB (alcuni cavi non funzionano).

![Image](images/zadig_3.png)

Selezionare ***libusbK (v3.0.7.0)*** usando le frecce:

![Image](images/zadig_4.png)

Quindi avviare l'installazione del driver facendo clic su ***Replace Driver***.

![Image](images/zadig_5.png)

Stiamo ora utilizzando la comunicazione USB con la scheda NUCLEO-WB55 in Windows.

Esci dal software Zadig.


### Programmazione del firmware MicroPython della scheda (dfu-util)

È ora necessario programmare la scheda con il firmware MicroPython. Useremo il software per questo **dfu-util**. Questo passaggio potrebbe essere potenzialmente eseguito dallo studente, nel caso in cui il firmware MicroPython debba essere aggiornato.

Usa il file ***flash_dfu-util.bat*** facendo doppio clic su di esso.<br>

Quindi, consenti a Windows di eseguire il programma.

Si apre una finestra di comando di Windows (potrebbe essere troppo veloce per vederla), attendere che la programmazione finisca:

![Image](images/dfu_util_1.png)

La programmazione è terminata, la finestra si chiude.

Ora è necessario spostare il jumper dal connettore CN7, PIN 5-7 a SWDIO:

![Image](images/Nucleo_WB55_Config_Programmation_2.png)

Premi il bottone ***Reset(SW4)*** sopra la presa ***USB_USER*** sul kit di sviluppo.

Viene visualizzato un messaggio di Windows:

![Image](images/dfu_util_2.png)

Ignorare questo messaggio per quanto riguarda i problemi del lettore.

Se vedi questo messaggio, l'operazione è andata a buon fine.

È ora possibile comunicare con il software MicroPython tramite il collegamento USB.

Diamo un'occhiata ai file generati dal sistema MicroPython con Windows Explorer:

Apri con Windows Explorer, il dispositivo PYBFLASH.

![Image](images/dfu_util_3.png)

Vedremo più avanti come modificare gli script Python disponibili nel filesystem PYBFLASH.

Prima di tutto proveremo a comunicare con l'interprete dei comandi Python direttamente sul kit di sviluppo NUCLEO-WB55.



## Installazione dell'ambiente di programmazione

Sono disponibili tre soluzioni:
- Installa (diritti di amministratore richiesti) [Thonny](https://thonny.org/) e il suo terminale integrato
- Installa (diritti di amministratore richiesti) [Notepad++](https://notepad-plus-plus.org/downloads/) con il plugin [Evidenziazione della sintassi di Python](http://npppythonscript.sourceforge.net/download.shtml) e un terminale seriale (PuTTY, [TeraTerm](https://ttssh2.osdn.jp/index.html.en)...)
- Usa la versione portatile di PyScripter (fornita nella confezione) e un terminale seriale (PuTTY, [TeraTerm](https://ttssh2.osdn.jp/index.html.en)...). Dans ce dernier cas, suivez le tutoriel suivant.

Queste modifiche dovrebbero essere padroneggiate dagli studenti in modo che possano comunicare con l'interprete e modificare gli script MicroPython sul kit NUCLEO-WB55.

## Configurazione del terminale di comunicazione

Ora che MicroPython è presente sul kit NUCLEO-WB55, vorremmo testare la comunicazione con Python. Il test consiste nell'invio di un comando python e nella verifica che l'esecuzione avvenga.

La comunicazione avviene tramite USB tramite una porta seriale, quindi abbiamo bisogno di un software che possa inviare comandi python come testo alla scheda e ricevere il risultato dell'esecuzione.

Per fare ciò, avvia l'utente Puttytel, disponibile nella directory TP_Python:

![Image](images/putty_1.png)

Selezionare l'opzione `Serial` :

![Image](images/putty_2.png)

Configura i campi `Serial Line` in `COM3` e `Speed` a `115200`. Quindi fare clic su `Open`.

Se viene visualizzato il seguente messaggio:

![Image](images/putty_3.png)


Verificare che il kit sia elencato sulla porta COM corretta, vedere **Vérifier l'attribution du port COM**.

Altrimenti, viene visualizzata una nuova finestra:

![Image](images/putty_4.png)


Premi CTRL + C per visualizzare l'interprete dei comandi Python:

![Image](images/putty_5.png)

Ora puoi eseguire i comandi Python.

```python
print("Hello World")
```

![Image](images/putty_6.png)

Abbiamo ora completato la programmazione del firmware MicroPython.

Il nostro kit di sviluppo NUCLEO-WB55 è ora pronto per l'uso con Python.


Tieni aperta la finestra di Putty, ti sarà utile vedere l'esecuzione degli script.

**NOTE :** L'autocompletamento (premere TAB) è disponibile durante la comunicazione seriale.


## Configurazione dell'editor di script Python

Vedremo, in questa parte, come impostare un ambiente di sviluppo MicroPython.


### Installazione dell'ambiente


Esegui il programma di installazione `Portable Python-3.8.0 x64.exe`, il programma di installazione ti chiede dove estrarre l'ambiente Python.
Usa il pulsante `...` per selezionare la cartella `TP_Python` sul desktop.

![Image](images/pyscript_1.png)

Quindi attendi che i file vengano estratti.

Ora abbiamo un ambiente di sviluppo Python con il quale lo sviluppatore può scrivere script MicroPython.

L'ambiente è disponibile nella cartella `TP_Python\Portable Python-3.8.0 x64` :

![Image](images/pyscript_2.png)

Avvia lo strumento `PyScript-Launcher` : 

![Image](images/pyscript_3.png)

Questo strumento ti consentirà di eseguire il debug e di eseguire i tuoi script Python.

**Note importanti :**

> La Biblioteca `pyb`,ovvero, il raggruppamento di funzioni micropython per accedere alle periferiche del microcontrollore non è disponibile durante il debug degli script su Windows.

> Fai anche attenzione a usare solo le librerie Python-3 portate in micropython. Ad esempio la libreria `numpy` non è ancora ufficialmente implementato in MicroPython, quindi sarà impossibile eseguire un file `import numpy` in uno script MicroPython da scaricare su NUCLEO-WB55.

qui è [elenco di librerie che possono essere utilizzate in fase di sviluppo in MicroPython](http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries)<br>

L'editor di script python (PyScript) è quindi utile per il test degli algoritmi o il completamento automatico dei metodi python.

### Scrivi ed esegui uno script MicroPython

Torna all'editor di script PyScript-Launcher, quindi espandi il lettore PYBFLASH:

![Image](images/pyscript_4.png)

Qui vediamo 2 file interessanti : `boot.py` e `main.py`.

Lo script * boot.py *, che deve essere modificato da utenti avanzati, viene utilizzato per inizializzare MicroPython. Permette in particolare di scegliere quale script verrà eseguito dopo l'avvio di MicroPython, di default lo script `main.py`.

Lascia la configurazione iniziale per ora.

Saremo interessati alla sceneggiatura `main.py`.


Fare doppio clic su di esso:

![Image](images/pyscript_5.png)

Il nostro primo script consisterà nel visualizzare il messaggio 10 volte `MicroPython est génial` con il numero del messaggio e tutto questo non sul nostro Nucleo ma *****sous Windows*****.


Scrivi il seguente algoritmo nell'editor di script:

![Image](images/pyscript_6.png)


Esegui e controlla il risultato premendo![Image](images/pyscript_play.png)

![Image](images/pyscript_7.png)

Lo script **funziona bene sotto Windows**.

Ora vogliamo **eseguirlo sulla scheda NUCLEO-WB55**.

Per fare ciò, salva il file script `main.py` (`CTRL+S`), quindi tornare al terminale Putty. 

Lo script verrà eseguito al riavvio di MicroPython.

Per eseguire un riavvio premere `CTRL+C` (per visualizzare l'interprete `>>>`) quindi CTRL + D (per forzare il riavvio) nel terminale Putty: 

![Image](images/pyscript_8.png)

Il nostro script è stato eseguito con successo! Possiamo vedere il nostro messaggio visualizzato 10 volte nel terminale.

Siamo ora pronti per eseguire comandi MicroPython che ci consentono di accedere alle periferiche del microcontrollore STM32WB55.

## Appendice: controllare l'assegnazione della porta COM


È stato necessario creare una porta `COM` su Windows.

Ecrivez `gestione dispositivi` nella barra di ricerca di Windows, quindi fare clic su `Aprire` :

![image](images/port_COM.png)

Si apre una nuova finestra:

![image](images/gestion_peripheriques.png)

Prendere nota del numero di porta `COM`. Nell'esempio sopra, il kit NUCLEO è collegato alla porta `COM3`. Questo è questo numero `COMx` che sarà necessario tornare a Putty.