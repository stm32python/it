---
title: Guida rapida di Linux
description: Guida rapida di Linux

---
# Tutorial per l'utilizzo di MicroPython da una macchina virtuale Linux

**Strumenti necessari per installare MicroPython :**  
È necessario utilizzare un computer Windows con una macchina virtuale Linux installata o direttamente un computer Linux.

**Per ottenere una macchina virtuale :**  
Troverai una macchina virtuale "Polytech" seguendo [questo link](https://polytech-prog.gricad-pages.univ-grenoble-alpes.fr/polytech-microc/).
Vai alla categoria "Risorse per la programmazione dell'STM32 su un personal computer" »,
quindi facendo clic su "Con VirtualBox ».


## Installazione di MicroPython per STM32 su un computer Linux

Dal desktop Linux, apri un terminale facendo
Fare clic con il tasto destro e poi "Apri un terminale qui"
Quindi inserisci i seguenti comandi uno per uno
per installare il software prerequisito.

```bash
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
```

Immettere la password quando richiesto.
Premere il tasto "o" per accettare l'installazione quando richiesto.
Una volta installato il prerequisito software, è necessario ripristinare il progetto MicroPython da
lo strumento git scrivendo in un terminale (aperto da una cartella in cui verrà collocata l'utility per Micropython) i seguenti comandi:

```bash
git clone https://github.com/micropython/micropython
cd ~/micropython
git submodule update --init
cd mpy-cross
make
cd ../ports/stm32
```

È ora possibile eseguire il seguente comando:

```bash
make BOARD={your-board-model}
```

È necessario sostituire * {your-board-model} * con il nome della scheda STM32 utilizzata.
Ad esempio, se stai usando un * NUCLEO F446RE *, sarà necessario scrivere il comando :  

```bash
make BOARD=NUCLEO_F446RE
```

È disponibile un elenco di schede STMicroelectronics compatibili con MicroPython [qua](https://github.com/micropython/micropython/tree/master/ports/stm32/boards).

*È possibile che il comando make non funzioni come desiderato, in questo caso è necessario aggiungere

```bash
PYTHON=python2 per ottenere il seguente comando :
make BOARD={your-board-model} PYTHON=python2
```

* Hai completato con successo questo passaggio se il terminale visualizza:  

![make-board.png](images/make-board.png)

Gli ordini precedenti hanno generato un file
nominato `build-{your– board–model}` disponibili in
`/micropython/ports/stm32` (la cartella micropython è dove hai aperto il terminale)  

Apri Esplora file per recuperare questa cartella. Contiene un file con estensione `.hex`, è il motore MicroPython che installeremo nell'STM32.

Per questo useremo il programma scaricabile STM32CubeProgrammer [ici](https://my.st.com/content/my_st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stm32cubeprog.license=1575477973406.product=STM32CubeProg.version=2.2.1.html#get-software). Dovrai creare un account myST.  

- 1 - Installazione in ambiente Linux :

Apri un terminale e inserisci i seguenti comandi :

```bash
sudo apt-get install libusb-1.0.0-dev    
sudo apt install openjdk-8-jre-headless
sudo apt purge openjfx  
sudo apt install openjfx=8u161-b12-1ubuntu2 libopenjfx-jni=8u161-b12-1ubuntu2 libopenjfx-java=8u161-b12-1ubuntu2  
sudo apt-mark hold openjfx libopenjfx-jni libopenjfx-java
sudo apt-get update
```  
Allora entra :
```bash  
sudo update-alternatives --config java
```  
Se il terminale visualizza `Niente da configurare`, continua, altrimenti ti verranno mostrate diverse righe con dei numeri all'inizio. Immettere il numero della riga contenente **java-8-openjdk**, e vai avanti.  

Estrai l'archivio scaricato, quindi esegui**SetupSTM32CubeProgrammer-x.y.z.linux** (x,y,z variano a seconda della versione)  
Lasciati guidare dall'installatore. 

Quindi inserisci il comando in un terminale :  
```bash
sudo cp ~/STMicroelectronics/STM32Cube/STM32CubeProgrammer/Drivers/rules/*.* /etc/udev/rules.d
```  
- 2 - Installazione in ambiente Windows :  

Copia il file `.hex` su chiavetta USB per poi passare a un ambiente Windows.  
Estrai l'archivio scaricato, quindi esegui **SetupSTM32CubeProgrammer-x.y.z.exe** (x,y,z variano a seconda della versione)  
Lasciati guidare dall'installatore.  

`STM32CubeProgrammer` è ora installato.  

Quindi apri il file`.hex` (1) in STM32CubeProgrammer e collegare la scheda Nucleo al computer.  
Sul software, fare clic su `connect` (2) poi `download` (3) e infine `verify` (3).  
Se si verifica un errore durante la connessione, provare a ricollegare il cavo o cambiare la porta USB.   

![cube-programmer.png](images/cube-programmer.png)  

Il codice lampeggia sulla carta se viene osservato un messaggio di successo della verifica.  
Ora hai installato con successo MicroPython su una scheda STM32.


## Primi codici in MicroPython 

Apri un terminale seriale come gtkterm o PuTTY.  
Scegli la porta seriale corrispondente alla tua scheda STM32 (`periferica responsabile > porte(COM et LPT) > STMicroelectronics`, la porta è tra parentesi su Windows e `/dev/ttyACM0` su linux), con una velocità di 115200. 
Si dovrebbe quindi osservare dopo aver premuto il pulsante nero RESET un messaggio simile:

![reset-button.png](images/reset-button.png)

È quindi possibile utilizzare il terminale come interprete Python,
questo è l'interprete interattivo MicroPython denominato REPL.
Ora inserisci le seguenti righe per testare il linguaggio Python:

```python
print(« Hello World »)
for i in range (10) :
```

Noterai che le iscrizioni `>>>` sono diventate `...` dopo il secondo comando, questo è normale perché la nozione di rientro è molto importante nel linguaggio Python a differenza di altri linguaggi come il C per esempio.
Quindi inserire più tardi `print(i)` quindi premere il tasto Invio tre volte per osservare:

![print-i.png](images/print-i.png)  

Abbiamo quindi creato una variabile denominata `i` e l'abbiamo incrementato fino al valore `9`.  
inserire `help()` avere alcuni comandi specifici per il linguaggio MicroPython.


## Utilizzo del software Geany per leggere un programma MicroPython

Ora sappiamo come programmare in MicroPython su un STM32 da un terminale seriale. Questo può poi diventare restrittivo nel caso di un programma di diverse decine di righe perché è quindi necessario scrivere riga dopo riga, tenendo conto dell'indentazione del linguaggio Python.
Successivamente "automatizzeremo" il tool di sviluppo Geany in modo da "compilare" un programma scritto in MicroPython.

Per prima cosa è necessario installare il software Geany:  
* In un ambiente Linux, inserisci questo comando su un terminale :

```bash
sudo apt-get install geany
```

* In un ambiente Windows, scarica il software[qua](https://www.geany.org/download/releases/).  

Quindi prendi il file `pyboard.py` fornito con il prodotto.  
Questo file contiene un programma scritto in linguaggio Python che permette di inviare programmi MicroPython alla scheda STM32 da rete fissa.  
Crea una directory sul tuo computer e rilascia il file pyboard.py al suo interno.
È quindi necessario configurare il software Geany.
ApUna volta aperto il software, vai al menu`Costruire > Definisci gli ordini di costruzione` per aggiungere un nuovo pulsante con nome `MicroPython` nella sezione `Comandi di esecuzione`.  
Nel campo successivo, scrivi:

```bash
python pyboard.py --device '/dev/ttyACM0' "%f"** per linux
```

```
python pyboard.py --device COMx "%f"
```
per Windows, sostituisci x con il numero che trovi sotto `gestione delle periferiche > porta(COM e LPT) > STMicroelectronics`, il numero è tra parentesi.  
Nota: la porta può cambiare tra ciascuna scheda e la porta USB.

Devi prenderlo :  

![geany-compilateur.png](images/geany-compilateur.png)  

Ora puoi creare un file con il nome che preferisci con un'estensione `.py` (creare un documento di testo e aggiungerlo manualmente `.py`) e posizionalo nella directory che hai appena creato.  
Aprilo con Geany quindi esegui questo programma con `Costruire> MicroPython`.  

Questo tutorial è finito, puoi scrivere uno script ed eseguirlo da Geany !

## Lavoro pratico con MicroPython

Per prima cosa, vedremo come accendere un LED.
Per fare ciò, scrivi il codice sottostante direttamente su un emulatore seriale o sull'IDE Geany: 

```python
from pyb import LED
led = LED(1)
led.on()
```

Questo codice accende il LED utente del microcontrollore, questo LED è solitamente
di colore verde.

* È molto importante avviare un programma MicroPython per STM32 con l'importazione
dalla libreria pyb o uno di questi componenti (LED, Pin, ExtInt, Timer .Etc).
Aggiungi all'inizio (prima riga) del tuo programma:

```python
import pyb
```

Introduction aux entrées/sorties :  
```python
from pyb import Pin

p_out = Pin('PA5', Pin.OUT_PP) # LED verde corrispondente a LED(1)
p_out.high() # p_out.low() per spegnere il LED

p_in = Pin('PC13', Pin.IN, Pin.PULL_UP)
p_in.value() # assume il valore 0 o 1
```

Anche il LED utente dovrebbe accendersi qui. Funzione `p_in.value()` deve tornare e visualizzare un bit che assume il valore 0 o 1 a seconda dello stato di `PC13` (di solito il pulsante utente).

Abbiamo imparato come accendere un pin di output e leggere il valore di un pin di input, andiamo a
guardare ora come generare un PWM.
Quando viene utilizzata la modulazione di larghezza di impulso (PWM)
vogliamo generare un segnale pseudo-analogico da una sorgente digitale.
I segnali PWM hanno le seguenti caratteristiche:
* Una frequenza, che designa la velocità alla quale il segnale ha completato un ciclo completo
(10Hz → 10 cicli al secondo).
* Un ciclo di lavoro che corrisponde alla durata allo stato alto nel periodo del segnale

![duty-cycle-examples.png](images/duty-cycle-examples.png)  

Prima di generare un PWM, è imperativo sapere se il pin dell'STM32 che vogliamo
utilizzare è compatibile con questa modulazione e anche il numero del timer (così come il suo canale) utilizzato per la modulazione.
Ad esempio, il pin PA5 solitamente è collegato al LED utente sulla maggior parte delle schede Nucleo
può essere collegato elettricamente al canale 1 del Timer 2 su alcuni STM32F4.

In questo caso, il codice per far lampeggiare questo LED con un PWM di frequenza 1Hz e con a
Il ciclo di lavoro del 50% sarà:

```python
from pyb import Pin, Timer
LED = Pin('PA5') # PA5 --> TIM2, CH1
tim = Timer(2, freq=1)
ch = tim.channel(1, Timer.PWM, pin=LED)
ch.pulse_width_percent(50) #DutyCycle de 50%
```

Il duty cycle è 0,5 con una frequenza PWM di 1Hz, quindi il LED è acceso per
500 millisecondi e spento anche per 500 millisecondi.

È disponibile un elenco di frammenti di codice MicroPython [qua](http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control)  

**Nota :**  
Questi codici non sono ancora tutti compatibili con i microcontrollori STM32,
sono destinati alla scheda pyboard (una scheda esperimento che può essere programmata nativamente in formato
MicroPython). 


## Riferimenti 
* [Flashing Micropython to STM32 Nucleo/](http://awhtechnologies.com/flashing-micropython-to-stm32-nucleo/)
* [MicroPython : Iniziare](http://micropython.fr/installation/de_quoi_avez_vous_besoin)  
* [Interagisci con micropython da uno script Python](http://micropython.fr/technique/python_to_micropython)
* [UM2237 Manuale utente Descrizione software STM32CubeProgrammer](https://www.st.com/content/ccc/resource/technical/document/user_manual/group0/76/3e/bd/0d/cf/4d/45/25/DM00403500/files/DM00403500.pdf/jcr:content/translations/en.DM00403500.pdf)
* [Illustration PWM](https://en.wikipedia.org/wiki/File:Duty_Cycle_Examples.png)  
