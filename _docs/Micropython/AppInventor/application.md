---
title: Tutorial di AppInventor
---
# Creazione di un'applicazione con AppInventor

<img src="images/Mit_app_inventor.png" width="200">

La scheda IKS01A3 oltre alla Nucleo-WB55 del kit educativo STM32Python può fornire molte informazioni, come la temperatura.

La scheda Nucleo-WB55 dispone di connettività BLE (Bluetooth Low Energy) e può quindi essere interrogata o controllata da remoto tramite smartphone. L'obiettivo di questo tutorial è creare una nostra applicazione per smartphone che recuperi e poi visualizzi le misure dei sensori (ad esempio quelli di uno scudo * IKS01A3 *) collegati ai pin di una scheda *Nucleo-WB55*.
Per fare questo useremo lo strumento ***MIT App Inventor*** che permette di semplificare la creazione delle applicazioni, in particolare grazie ad un'interfaccia grafica simile a quella di Scratch.

Affinché questo tutorial funzioni, è necessario scaricare 3 file (forniti da STMicroElectronics) disponibili nel [TP BLE](https://stm32python.gitlab.io/fr/docs/Micropython/Telechargement) e copiarli sulla scheda Nucleo-WB55 : 
* **ble_advertising.py**
* **ble_sensor.py**
* **main.py** (dobbiamo sostituire il main.py della scheda con questo main.py lì)

## 1. Inizializzazione del progetto

Per prima cosa, creeremo un nuovo progetto dall'applicazione AppInventor e analizzeremo l'interfaccia di sviluppo.

Potrai accedere al file **AppInventor** in [clicca qui](https://appinventor.mit.edu/).

Per iniziare un nuovo progetto, basta fare clic su *Create Apps!*.

<div align="center">
<img src="images/fenetre_1.png" width="700px">
</div>

Da quel momento puoi cambiare la lingua (in alto a destra) e passare alla lingua francese: il nostro tutorial è realizzato dal sito in francese.
Una volta creato il tuo account, puoi fare clic su * Avvia nuovo progetto ... *. Chiameremo il nostro progetto "MyApply".

L'interfaccia di sviluppo di AppInventor è suddivisa in 2 parti: la parte del design e la parte del codice.

<div align="center">
<img src="images/fenetre_design.png" width="400px">
<img src="images/fenetre_code.png" width="400px">
</div>


Puoi passare da una finestra all'altra facendo clic sui pulsanti * Designer * e * Blocchi * situati in alto a destra dello schermo

## 2. Design della schermata iniziale

Dans un premier temps nous allons travailler sur le design de notre application. Nous allons devoir déterminer quels sont éléments (boutons, label, listes ...) nécessaires.

Sul lato sinistro, hai diverse schede contenenti tutte le funzionalità che puoi portare nella tua app.

Per aggiungere un elemento al nostro schermo, trascinalo sul disegno dello schermo del telefono (la finestra * Interfaccia *). Qui puoi vedere il menu principale della nostra applicazione:

<div align="center">
 <img src="images/fenetre_home.png" width="500px">
</div>

La creazione degli articoli avviene sotto forma di un albero. È importante identificare le diverse aree e le diverse caratteristiche che conterranno.

Qui, possiamo vedere che gli elementi (la lente di ingrandimento e il testo) sono incorniciati in una zona. Queste aree sono anche chiamate * layout *. Definiscono un "framework" in cui sono raggruppate diverse funzionalità.

* Quindi apri la scheda _Disposizione_ sul lato sinistro.

Dobbiamo ora scegliere un tipo di disposizione per i nostri articoli in queste aree: sceglieremo arbitrariamente una disposizione verticale.

* Seleziona _Vertical Arrangement_ e trascinalo sullo schermo dello smartphone.

<div align="center"><img src="images/arrangement_vertical.png" width="700px"></div>

Per poter centrare la nostra disposizione verticale, abbiamo fatto clic su * Screen1 * (nella parte * Components *), quindi abbiamo impostato la disposizione orizzontale e verticale su * Center *.


Possiamo vedere sullo schermo 2 oggetti diversi:
* testi: queste sono _etichette_, si trovano nella scheda _User Interface_
* un'immagine: questa immagine è in realtà un pulsante (_pulsante_) che può essere trovato anche nella scheda _User Interface_.

Questi oggetti possono essere migliorati cambiando il loro colore, o la loro dimensione, ad esempio, nel menu a destra (* Proprietà *). Questo è il motivo per cui, ad esempio, il pulsante centrale è rappresentato con un'immagine di lente di ingrandimento o che alcuni testi sono in grassetto o più grandi. Puoi personalizzarli come desideri.

Quindi trascina sullo schermo 3 _etichette_ e un _pulsante_ all'interno del tuo _Disposizione verticale_. È possibile modificare il testo di questi componenti nella parte _Testo_ delle _Proprietà_.

La nostra applicazione verrà utilizzata per entrare in contatto con la scheda tramite BLE. Una volta attivate la geolocalizzazione e il Bluetooth del tuo telefono, analizzeremo il nostro ambiente e selezioneremo dall'elenco dei dispositivi disponibili quello a cui vogliamo connetterci. Quindi abbiamo bisogno di un * List Selector *. Questo può essere trovato nella scheda * Interfaccia utente *.

**Attenzione :** per ottenere il design desiderato, è necessario rendere invisibile questo elenco deselezionando il campo * Visibile * nelle sue * Proprietà *.

Tra * Interfaccia * e * Proprietà * troverai una parte * Componenti * che mostra l'albero dei widget che hai aggiunto alla tua applicazione. Ecco come dovrebbe apparire il tuo albero:

<div align="center">
<img src="images/component.png" width="300px">
</div>

Puoi fare clic su * Rinomina * per rinominare i tuoi componenti come nell'immagine sopra.

Un'ultima parte molto importante è la capacità della nostra applicazione di connettersi tramite bluetooth. Nel nostro caso si tratta di una connessione BLE che non è disponibile nelle funzionalità di base di AppInventor.

Sotto la parte * Tavolozza * troverai una scheda * Estensione *. Devi importare il file <a href="../../../assets/MITAppInventor/connexion_BLE.aix"
 download>connexion_BLE</a>

Una volta importato, devi trascinarlo nell'_Interfaccia_. Lo vedrai apparire sotto lo smartphone nella categoria _Componenti non visibili_.

Allo stesso modo, per poter gestire vari avvisi come il rilevamento del dispositivo, avrai bisogno di un _Notificatore_ nella categoria _Interfaccia utente_.

Ora dovresti ottenere il risultato mostrato all'inizio di questa parte.

## 3. Configurazione della connessione

Per realizzare la nostra applicazione dovremo associare un comportamento a questi componenti, in base alle azioni dell'utente.

Quindi passa ora alla parte * Blocchi *

<div align="center">
<img align="center" src="images/fenetre_code.png" width="600px">
</div>

La parte * Blocchi * contiene tutto il "codice" di cui avrai bisogno.

Qui trovi tutte le basi della programmazione, con operatori booleani (vero, falso), sintassi dei loop, ecc. Ogni categoria è simbolizzata da un colore diverso come puoi vedere nel pannello a sinistra dello schermo. Possiamo anche modificare o richiamare le istruzioni sui nostri diversi componenti (creati nella parte * Design *) cliccando sul componente da modificare e utilizzando i diversi metodi proposti.

### Avvio dell'applicazione

Il primo passo è inizializzare la nostra schermata di benvenuto, ovvero definire i comportamenti che si verificano all'apertura dell'applicazione.
Per questo, c'è nel componente _Screen1_ la struttura `quando Screen1 si inizializza`. Trascina questa struttura sullo schermo.

Per aiutarci a visualizzare lo stato della nostra connessione Bluetooth, scegliamo di cambiare lo sfondo dell'etichetta "Stato_connessione" in base allo stato della connessione:

* <span style="color: #FF0000">Rouge</span> : Pas de connexion
* <span style="color: #FFC800">Orange</span> : Connexion en cours
* <span style="color: #00FF00">Vert</span> : Connexion réussie

Per cambiare la struttura di un'etichetta devi usare la struttura `mettere ... a`.

Cliccando sul componente `Stato_connessione` possiamo quindi selezionare la struttura` mettere Stato_connessione Colore di sfondo a` e scegliere il colore appropriato nella parte _Colori_ (qui rosso). Trascina questo componente nel blocco `quando Screen1 si inizializza`.

Allo stesso modo, in assenza di connessione, l'etichetta `Etat_connexion` deve contenere il testo" Nessuna connessione ".

Quindi possiamo anche usare`impostazione Stato_Connessione_Testo su` aggiungendovi un campo di scrittura in _Testo_.

Dovresti ottenere questo:

<div align="center">
<img align="center" src="images/cnx_1.png" width="600px">
</div>

### Inizializzazione del pulsante di connessione

Ora gestiremo cosa succede quando premiamo il pulsante * Connessione * (`quando Clicchi Connessione`).

Quindi seleziona quel componente lì e aggiungilo alla schermata del codice.

Questo dovrebbe abilitare la ricerca del dispositivo (`chiamata BluetoothLE1 IniziaScansione`).

Incorpora questo codice lì in quello precedente.

Per aiutare l'utente a conoscere lo stato dell'applicazione, useremo * Notificatore * per specificare l'azione corrente.

Infine, aggiungi il blocco `chiamata Notificatore1 Avviso su schermo` + _Testo_:" BLE ricerca in corso .. " `

<div align="center">
<img align="center" src="images/cnx_2.png" width="600px">
</div>

### Ricerca dispositivo

Ora dobbiamo definire cosa fare quando i dispositivi sono stati trovati.

Trascina il blocco `quando BluetoothLE1 DispositivoTrovato` nella finestra.

La ricerca delle periferiche deve essere prima interrotta.

Annidare il blocco `chiamata BluetoothLE1 StopScansione` all'interno del precedente.

Nel campo * DispositivoTrovato * del nostro BLE c'è l'elenco dei dispositivi disponibili. Aggiorneremo quindi la nostra * Lista_BLE * con gli elementi trovati dal nostro BLE che sono caratterizzati dal loro nome (quindi da una stringa di caratteri):

Aggiungi il blocco `mettere Lista_BLE Elementi della catena a BluetoothLE1 ListaDispositivi`.

Anche la nostra * etichetta * `Stato_connessione` è cambiata. Questo ora ci darà il numero di BLE trovati che è equivalente alla dimensione dell'elenco di elementi in * lista_BLE_Dispositivi *.

Per fare ciò, aggiungi il blocco `mettere Stato_Connessione_Testo a`.

**Attenzione:** qui, puoi aggiungere del testo con le informazioni contenute in un componente. Quindi dovrai unire questi due elementi.

Per fare questo, usa la struttura "allegato" nella scheda _Testo_

Quindi puoi allegare un testo e la lunghezza dell'elenco dei dispositivi trovati.

Per fare ciò, aggiungi un campo `Testo` contenente" Numero di BLE trovati: "e annidalo nella prima parte del blocco` allegato`.

Per la seconda parte di questa struttura, useremo la struttura `dimensione della lista` (in _Lista_) e aggiungeremo la lista desiderata (quella di cui vogliamo conoscere la dimensione):` Lista_BLE Elementi`.

Una volta apportate queste modifiche possiamo visualizzare l'elenco dei dispositivi trovati aggiungendo la struttura `chiamata LISTA_BLE apertura`.

<div align="center">
<img align="center" src="images/cnx_3.png" width="600px">
</div>

### Scelta del dispositivo

Una volta visualizzato l'elenco dei dispositivi disponibili, dovremo selezionare quello che ci interessa.

#### Inizializzazione degli indirizzi

L'indirizzo di questo dispositivo verrà memorizzato in una variabile (scheda arancione * Variabili * nel pannello laterale). È quindi importante soprattutto inizializzare questa variabile, dire che esiste e darle un nome.

Per fare ciò, aggiungi il blocco `inizializza nome_globale_ a` e modifica il nome in modo che la nostra variabile si chiami _Indirizzo_.

Poiché questo indirizzo non ha ancora valore, possiamo assegnargli un campo di testo vuoto.

Quindi annida un campo _Testo_ vuoto in questa struttura.

<div align="center">
<img align="center" src="images/cnx_4.png" width="600px">
</div>

#### Recupero dell'indirizzo

Quindi ci occuperemo di ciò che accade dopo aver scelto il tuo dispositivo.

Per questo, useremo la struttura `quando LISTA_BLE Dopo aver preso`.

Per prima cosa modificheremo il valore della nostra variabile * Indirizzo * assegnandogli quella selezionata nell'elenco.

Quindi annidate il blocco `mettere ... a` (che è nella categoria _Variabili_ poiché vogliamo modificare il valore di_Indirizzo_). Quindi usa la struttura `Lista_BLE Selezione` per aggiornare la nostra variabile con il dispositivo selezionato dalla lista.
#### Cambio dell'indicatore

Quindi modificheremo il nostro cookie per notificare all'utente la modifica dello stato dell'applicazione.

Come abbiamo fatto in precedenza, cambieremo il `Testo` dell'etichetta` Stato_connessione`.

Come prima vorremo indicare a quale dispositivo vogliamo connetterci. Per questo useremo i blocchi `allegato` .

Quindi aggiungi un blocco allegato che sarà composto da un _Testo_: "Connessione a", seguito dall'indirizzo del nostro dispositivo memorizzato nella nostra variabile _Indirizzo_ recuperata utilizzando la struttura `prendi indirizzo globale`.

Tuttavia, la connessione non è ancora terminata, specificheremo che è in corso.

Aggiungi per questo una struttura `allegato` che assocerà il contenuto della nostra variabile _Indirizzo_ con un _Testo_ contenente" in corso ".

Quindi cambieremo il colore di sfondo cambiandolo in giallo.

Quindi usa la struttura `mettere Stato_connessione Colore di sfondo a` e ritaglia il colore _Arancione_ lì.

#### Connetti al dispositivo

Una volta selezionata la carta, è necessario stabilire la connessione tra essa e l'applicazione.

Per questo useremo la struttura `chiamata BluetoothLE1 Indice di Connessione`. 

L'indice rappresenta il numero del dispositivo nel nostro elenco di dispositivi disponibili. È quindi quello che ha il nome del nostro * Indirizzo * negli elementi di Lista_BLE.

Dobbiamo quindi aggiungere la struttura `indice nella lista` che combinerà l'indirizzo del dispositivo, quindi la nostra variabile _Indirizzo_ (recuperata grazie al blocco` prendi`) e` Lista_BLE Elementi`.

<div align="center">
<img align="center" src="images/cnx_5.png" width="600px">
</div>

Alla fine dovresti ottenere un codice come questo:

<div align="center">
<img align="center" src="images/code_cnx.png" width="600px">
</div>


## 4. Progettazione dello schermo di controllo

Come per la schermata di accesso, dobbiamo definire quali componenti saranno necessari per accendere il LED e misurare la temperatura.

Vogliamo ottenere il seguente risultato:

<div align="center">
<img align="center" src="images/deuxieme_ecran.png" width="300px">
</div>

Per questo, avremo bisogno di un'etichetta per indicare all'utente che la connessione è stabilita, un pulsante di commutazione che ci permetterà di accendere e spegnere il LED, un'etichetta per visualizzare "Temperatura:" (da pura estetica) e un'ultima etichetta che servirà per visualizzare la temperatura corrente.

Tuttavia, riutilizzeremo l'etichetta * Stato_connessione * creata in precedenza per visualizzare il successo del collegamento con la scheda.

Useremo una _Disposizione_Disposizione Verticale_ per memorizzare tutti questi elementi (eccetto l'etichetta _Stato_Connessione_), che aggiungeremo sotto la disposizione _Disposizione_connessione_. 

Ora trascineremo uno _Switch_ in questa _ Disposizione verticale_, quindi aggiungeremo una _ Disposizione orizzontale_.

In quest'ultima inseriremo due _Etichetta_: una contenente "Temperatura:" e l'altra contenente la temperatura in tempo reale.

Inoltre, aggiungeremo un _Pulsante_ per disconnettersi dalla mappa.

Dopo aver aggiunto di nuovo questi elementi, dovresti ottenere questo:
<div align="center"> <img align="center" src="images/component2.png" width="700px"> </div>

**Attenzione:** Questi elementi devono essere invisibili finché non ti colleghi a una scheda. Fare quindi clic sulla disposizione * Caratteristiche * e deselezionare il campo * Visibile * nelle * Proprietà *. Allo stesso modo, per il pulsante * Disconnetti *. Che ti darà il seguente risultato:

<div align="center"> <img align="center" src="images/invisible.png" width="700px"> </div>

Ovviamente non possiamo eliminare gli elementi necessari alla connessione. Quindi, per ottenere l'aspetto che vogliamo, dobbiamo pensare a cosa rendere invisibile. Infatti, una volta stabilita la connessione, non avremo più bisogno delle etichette * Conseil_1 * e * Conseil_2 * così come del pulsante * Connessione *.


## 5. Creazione della connessione e cambio di schermata

Ora che questa nuova pagina è stata creata, programmeremo questo cambio di schermata.
Questa modifica avverrà una volta stabilita definitivamente la connessione con il dispositivo.

Quindi aggiungi un blocco `quando BluetoothLE1 Connesso`. 

È quindi necessario nascondere gli elementi correnti (tranne l'etichetta * Stato_connessione *) e visualizzare gli elementi della nuova pagina.

#### Cambio di pagina

Vogliamo quindi rendere invisibili gli elementi * Advice_1 *, * Advice_2 * e * Connessione *.

Per ciascuno di questi componenti, utilizzare la direttiva `settare ... Visibile a` e annidare il campo * Logico *` falso` lì.

Al contrario, vogliamo vedere gli elementi relativi a * Funzionalità * e il pulsante * Disconnetti *.

Quindi modifica il campo _Visibile_ di questi elementi lì, ma questa volta annida il valore logico su "vero".

Questi elementi diventeranno quindi visibili.

Tra questi due cambi di stato cambieremo l'indicatore di connessione `Stato_connessione` passando il suo sfondo a _Verde_ e il suo messaggio a" Connessione a .. riuscita "come abbiamo fatto in precedenza.

#### Impostazioni della carta

 Quindi imposteremo `UUID`. Gli UUID sono chiavi univoche che consentono l'identificazione tra due oggetti BLE. Questo meccanismo è specifico per la connessione BLE.
  Dovremo definirne un certo numero, uno per collegare l'STM32 e il telefono e altri per gestire le informazioni (LED, temperatura e altri sensori). Nel nostro caso, useremo un sensore di temperatura e manipoleremo i LED in modo da utilizzare 2 UUID aggiuntivi, o 3 in tutto.

Dovremo quindi definire 3 variabili proprio all'inizio del codice, come abbiamo fatto per `Indirizzo globale`.

Queste 3 variabili saranno chiamate `Service_UUID` e` SerialPORT_Temp_UUID` (per la temperatura) e` SerialPORT_LED_UUID` (per i LED). Questi UUID sono parametri specifici della carta che troverai nel codice C associato. I valori sono quindi i seguenti:w

<div align="center">
<img align="center" src="images/UUID.png" width="500px">
</div>

Il trattamento termico è un po 'speciale. Infatti, dobbiamo specificare nel programma che vogliamo ricevere float provenienti dall'UUID * SerialPORT_Temp_UUID *.

 Aggiungeremo quindi la struttura `chiamata BluetoothLE1 RegistriPerFloats` che indicherà che vogliamo ricevere _Floats_ (quelli reali).

 Ora assoceremo il campo _serviceUuid_ a quello che abbiamo definito in precedenza utilizzando la struttura `prendi Service_UUID globale`.

 Quindi devi indicare quale particolare UUID deve essere utilizzato (quello della temperatura): associa quindi il campo _characteristicheUuid_ alla nostra variabile _SerialPORT_Temp_UUID_.

 Infine, intendiamo solo che vogliamo avere i numeri interi, quindi imposta il campo _shortFloat_ su _false_. 

 Infine, dovresti ottenere il seguente codice:

<div align="center">
<img align="center" src="images/code_cnx2.png" width="600px">
</div>

## 6. Implementazione del sensore

 Ora dobbiamo associare il codice che prende la temperatura e quello che accende il LED ai componenti corrispondenti.


### LED

 Poiché utilizziamo un * interruttore * per accendere e spegnere il LED, faremo attenzione se il pulsante cambia o meno.

 Quindi usa un blocco `quando il LED è cambiato`.


In questo blocco distingueremo due casi: se vogliamo accendere il LED e se vogliamo spegnerlo. Verificheremo quindi se l' * Interruttore * è premuto o meno e faremo un trattamento diverso a seconda dello stato dell' * Interruttore *.

 Aggiungi la struttura `if ... then ... else` che si trova nella scheda _Controlli_

Quello che testeremo è lo stato del nostro * Interruttore *.

 Annidare il blocco "LED On" nell' _if_ del blocco precedente.

<div align="center"><img align="center" src="images/led.png" width="600px"></div>

#### Accendi il LED

Se viene premuto l '* interruttore *, diremo alla scheda che il LED dovrebbe essere acceso ora.

Per questo, scriveremo sulla scheda utilizzando la struttura `chiamata BluetoothLE1 WriteBytes`.

Questa funzione vi permetterà di designare con quale sensore sta parlando la scheda.

Come prima, il valore di _service UUID _ è contenuto nella nostra variabile globale _Service_UUID _ (recuperata grazie alla funzione `prendi Service_UUID globale`).

Il valore di _characteristicheUUID_ è contenuto nella variabile _SerialPORT_LED_UUID_ che otteniamo come prima.

Abbiamo impostato il campo _signed_ a `vero` (avremmo potuto impostarlo a falso, questo non ha effetto sul nostro codice, indica solo alla carta se i valori che le passiamo possono essere negativi o meno) . 

Siamo nel caso in cui il LED deve essere acceso quindi definiamo il valore _value_ a `1` (campo` 0` nella categoria _Maths_, che modifichiamo a 1).

<div align="center"><img align="center" src="images/led_1.png" width="600px"></div>

####  Spegni il LED

Al contrario, quando vogliamo spegnere il LED, faremo la stessa cosa ma scriveremo il valore 0.

Fare clic con il pulsante destro del mouse sul blocco `chiamato BluetoothLE1 WriteBytes` e fare clic su _Duplicare_. Annidate questo blocco duplicato in _else_ e cambiate il valore "1" solo con il valore "0".

<div align="center">
<img align="center" src="images/led_2.png" width="600px">
</div>


### Termometro

Il sensore di temperatura misurerà un valore e lo invierà alla scheda. Dobbiamo recuperare questo valore e visualizzarlo sullo schermo.

Per questo abbiamo bisogno della struttura `quando BluetoothLE1 FloatsRicevuti` che indica che il sensore di temperatura ha ricevuto dati.

Andiamo ora a modificare la nostra etichetta  _Temperatura_ utilizzando la funzione `metti Testo Temperatura a` alla quale assoceremo il valore contenuto nella variabile _floatValues_ grazie al blocco `prendi floatValues` (_floatValues_ contiene i valori ricevuti dalla scheda grazie al sensore di temperatura ).

<div align="center">
<img align="center" src="images/temperature.png" width="500px">
</div>

Dovresti ottenere il codice seguente:

<div align="center">
<img align="center" src="images/led_tmp.png" width="500px">
</div>


## 7. Disconnettersi

Una volta che l'utente fa clic sul pulsante _Disconnessione_, dobbiamo disconnettere l'app dal dispositivo e tornare alla home page.
Per quanto riguarda il pulsante * Connessione *, la disconnessione avviene in 2 fasi: un blocco per premere il pulsante e un blocco per disconnettere il BLE.

Per prima cosa, devi usare un blocco `quando fai clic su disconnessione`. 

In questo blocco chiameremo semplicemente la funzione `chiamata BluetoothLE1 Disconnessione`.

<div align="center">
<img align="center" src="images/dcnx_1.png" width="500px">
</div>

Il secondo blocco sarà quindi costruito attorno alla struttura `quando BluetoothLE1 Disconnesso`.

In questo blocco faremo:
  Cambia il colore di sfondo del nostro indicatore di connessione in rosso
  Cambia il messaggio dell'indicatore in "Nessun dispositivo connesso"
  Rendi visibili le _Etichette_ `Advice`,` Advice2` e il _Bottone_ `Connessione`
  Rendi invisibili il _Bottone_ `Disconnessione` e la_ Disposizione verticale_` Funzionalità`.

<div align="center">
<img align="center" src="images/dcnx_2.png" width="500px">
</div>

## 8. Versione finale dell'applicazione 

### Download file

Per verificare di aver compreso correttamente tutte le nostre istruzioni, scarica la versione finale della nostra app: <a href="../../../assets/MITAppInventor/MyApply.aia" download>MyApply.aia</a>. 

**Attenzione:** dato che non puoi aprire 2 progetti contemporaneamente, è consigliabile scattare una foto dei blocchi della versione corretta per confrontarli con la tua versione. 

### Apri un progetto su MIT App Inventor

Sul sito Web di MIT App Inventor, troverai il seguente menu: 
<div align="center">
<img align="center" src="images/menu_app_inventor.png" width="500px">
</div>

Per importare il file precedentemente scaricato, è necessario fare clic sulla sezione * I miei progetti * del menu. Potrai quindi vedere comparire le seguenti scelte: 
<div align="center">
<img align="center" src="images/menu_1.png" width="200px">
</div>

Dovrai infine cliccare su * Importa progetto (.aia) dal mio computer ... * e trovare il file scaricato.

Ora hai aperto il file scaricato.

## 9. Utilizzando l'applicazione creata

### Download dell'app MIT AI2 Companion

Per poter utilizzare l'applicazione che hai appena creato e quindi poter comunicare con la scheda, dovrai prima scaricare l'applicazione ** MIT AI2 Companion ** che troverai sul Play Store: 
<div align="center">
<img align="center" src="images/mit_play_store.jpg" width="250px">
</div>

### Lancio dell'applicazione creata sul nostro telefono

Per poter utilizzare la nostra app, dovrai esportarla sul tuo telefono. Per questo, dovrai aprire il menu * Connetti * nel seguente menu: 
<div align="center">
<img align="center" src="images/menu_app_inventor.png" width="500px">
</div>

Ora devi fare clic sulla sezione * AI Companion * in questo menu:
<div align="center">
<img align="center" src="images/menu_2.png" width="200px">
</div>

Questo aprirà la seguente finestra per te:
<div align="center">
<img align="center" src="images/connect_1.png" width="300px">
</div>

Sul telefono, apri l'app ** MIT AI2 Companion **. Vedrai apparire questa finestra: 
<div align="center">
<img align="center" src="images/mit_app.jpg" width="250px">
</div>

Quindi puoi vedere che ci sono 2 modi per utilizzare la tua applicazione:
* un codice di 6 lettere o
* un codice QR.

Tuttavia, sul tuo computer, viene visualizzato un codice QR e alla sua destra un codice di 6 lettere.

Quindi puoi connetterti come preferisci. 

Una volta che il codice QR è stato scansionato, o il codice di 6 lettere inserito, vedrai una barra di caricamento visualizzata sullo schermo del tuo computer:
<div align="center">
<img align="center" src="images/connect_2.png" width="500px">
</div>

Una volta completato, sarai in grado di vedere la tua applicazione sul tuo telefono:
<div align="center">
<img align="center" src="images/app.jpg" width="250px">
</div>

**Quindi ora puoi usare la tua app!**