---
title: AppInventor
description: Come utilizzare il kit STM32 MicroPython con MIT App Inventor
---
# Come utilizzare il kit STM32 MicroPython con MIT App Inventor ?

## MIT App Inventor

<img src="images/Mit_app_inventor.png" width="200" alt="MIT App Inventor">

[MIT App Inventor](http://appinventor.mit.edu/) è un ambiente di programmazione visuale intuitivo che consente a chiunque, anche ai bambini, di creare app completamente funzionali per smartphone e tablet. Il linguaggio di programmazione utilizzato è [Scratch](https://scratch.mit.edu/).

## Sommario

In questa parte troverai gli esercizi che ti consentono di creare applicazioni MIT App Inventor per smartphone e tablet per poter comunicare con la tua scheda STM32 tramite il tuo smartphone o tablet.
 - [Creazione dell'applicazione con MIT App Inventor](application)
 - Programmazione di un mini-robot e controllo dei suoi motori DC con MIT App Inventor da uno smartphone (presto)
