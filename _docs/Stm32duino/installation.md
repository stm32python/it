---
title: Stm32duino
description: Come installare Stm32duino
---
# Installazione di Stm32duino

## Installazione dell'ambiente Arduino
Innanzitutto, è necessario utilizzare l'ambiente di sviluppo Arduino.


Scarica e installa Arduino IDE dal link [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)

### Installa il pacchetto STM32
Una volta installato Arduino IDE, avvia Arduino IDE quindi vai su "File" > "Preferenze"

Si apre una finestra:

![Image](exercices/images/1_installation/1.png)


In "URL aggiuntivo di Map Manager", inserisci il seguente URL:
https://raw.githubusercontent.com/stm32duino/BoardManagerFiles/master/STM32/package_stm_index.json

Inserisci "OK"

Quindi : "Strumenti" > "Tipi di documenti :" > "Gestione documenti"
![Image](exercices/images/1_installation/2.png)

Entrare nella barra di ricerca "STM32" o "stm" e scaricare il pacchetto.
![Image](exercices/images/1_installation/3.png)

Fare clic su installa.
![Image](exercices/images/1_installation/4.png)

### Scarica e installa STM32CubeIDE

Per poter utilizzare il tuo STM32 con l'ambiente Arduino, devi installare lo strumento STM32CubeIDE.
Scarica e installa STM32CubeIDE disponibile a questo indirizzo [https://www.st.com/en/development-tools/stm32cubeide.html
](https://www.st.com/en/development-tools/stm32cubeide.html).

> Attenzione: è necessario creare un account my.st.com

Avvia arduino IDE con un STM32 collegato USB sul computer.

Vai su Strumenti> "Tipo di scheda:" e scegli STM32 Nucleo 64 come mostrato nell'immagine sotto.

![Image](exercices/images/1_installation/5.png)

Quindi vai su Strumenti> "Numero parte scheda:" e scegli la scheda STM32 corrispondente
alla tua attrezzatura (nel nostro esempio il Nucleo F446RE).

![Image](exercices/images/1_installation/6.png)

In Strumenti> "Metodo di caricamento:"

![Image](exercices/images/1_installation/7.png)

Quindi: Strumenti> "Porta: e scegli la porta COM corrispondente al tuo dispositivo STM32.

![Image](exercices/images/1_installation/8.png)


È quindi possibile programmare il file STM32

Congratulazioni!
Questo tutorial è finito.