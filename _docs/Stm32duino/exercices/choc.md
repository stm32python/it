---
title: Esercizio con il sensore di shock (inclinazione) in C / C ++ per Stm32duino
description: Esercizio con il sensore di shock (inclinazione) in C / C ++ per Stm32duino
---
# Esercizio con il sensore di shock (inclinazione) in C / C ++ per Stm32duino

- **Prerequisiti :**

Il kit del sensore fornisce uno *shield* (immagine sotto). Sarà utilizzato per collegare un sensore alla scheda.
Basta collegarlo alla scheda.
Collegare questo sensore alla schermatura sul pin D4. I pin Dx vengono utilizzati per elaborare un segnale digitale (0 o 1) e i pin Ax gestiscono i segnali analogici.

![Image](images/4_tilt_sensor/shield.png)        ![Image](images/4_tilt_sensor/shield_carte.png)  

Apri Arduino e verifica che la porta sia collegata: Strumenti/Porta, dovrebbe essere selezionato "COM3".
- **Sensore di inclinazione::**

![Image](images/4_tilt_sensor/tiltsensorim.png)

Il "tilt-sensor", sensore di inclinazione in italiano, è un sensore che misura la posizione di inclinazione in relazione alla gravità. La palla nel sensore (influenzata dal movimento del sensore) rotola e fa contatto.

*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600); // inizializzazione della connessione seriale
  pinMode(D4,INPUT);
}

void loop() {
  boolean etatContact=digitalRead(D4);
  if (etatContact)
    Serial.println("Contact");
  else
    Serial.println("Pas de contact");
}
```

Controlla e carica.
Per visualizzare lo stato del sensore cliccare sul * monitor seriale *

![Image](images/4_tilt_sensor/moniteur_serie.png)

> Credito d'immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
