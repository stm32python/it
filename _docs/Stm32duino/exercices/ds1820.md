---
title: Esercizio con la sonda impermeabile Grove DS1820 in C / C ++ con Stm32duino
description: Esercizio con la sonda impermeabile Grove DS1820 in C / C ++ con Stm32duino
---

# Esercizio con la sonda impermeabile Grove DS1820 in C / C ++ con Stm32duino

<div align="center">
<img alt="Grove - DS1820" src="images/grove-ds1820.jpg" width="400px">
</div>

Prossimamente...

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
