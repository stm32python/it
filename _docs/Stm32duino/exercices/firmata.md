---
title: Esercizio di comunicazione con Firmata
description: Esercizio di comunicazione con Firmata
---

# Esercizio di comunicazione con Firmata

[Firmata](http://firmata.org/wiki/Main_Page) è un protocollo di comunicazione tra un PC e una scheda microcontrollore come Arduino. Questo protocollo viene utilizzato per configurare i GPIO come input o output in modo che un programma sul PC possa leggere gli input e posizionare gli output della scheda..

In questo esempio, utilizzerai Firmata con la tua scheda STM32.

Chiudi la console seriale.

Apri l'esempio denominato `Firmata` facendo: `Fichier > Exemples > Firmata > StandardFirmata`

Quindi caricare il programma come fatto in precedenza.

![Image](images/2_firmata/1.png)

Scarica l'eseguibile del client Firmata (corrispondente al tuo sistema operativo) a questo indirizzo:

http://firmata.org/wiki/Main_Page

Fare clic sull'eseguibile (non è richiesta installazione), il client Firmata si apre:

Fare clic su `port` e selezionare la porta COM corrispondente al proprio dispositivo.

![Image](images/2_firmata/2.png)

Riavvia il tuo STM32 premendo il pulsante di colore nero.
Un LED dovrebbe lampeggiare rapidamente sul tuo STM32 subito dopo averlo riavviato,
significa che Firmata è pronto per essere utilizzato!

Viene visualizzato il client Firmata e gli ingressi/uscite possono essere configurati sulla scheda.

![Image](images/2_firmata/3.png)

