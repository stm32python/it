---
title: Esercizio con il sensore di luce Grove in C / C ++ per Stm32duino
description: Esercizio con il sensore di luce Grove in C / C ++ per Stm32duino
---
# Esercizio con il sensore di luce Grove in C / C ++ per Stm32duino


**Vedere il tutorial del sensore di inclinazione per i prerequisiti**


- **Sensore di luce :**

![Image](images/6_luminosite/capteur-de-lumiere.png)

Questo sensore utilizza una fotoresistenza per rilevare l'intensità della luce dell'ambiente circostante. Il valore di questo resistore diminuisce quando è illuminato.


- *Ecco il codice su Arduino*

```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,INPUT);

}
void loop() {
  Serial.println(digitalRead(D4));
  delay(500);
}
```

Controlla e carica.
Per visualizzare lo stato del sensore fare clic su *monitor seriale*.

![Image](images/6_luminosite/moniteur_serie.png)

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
