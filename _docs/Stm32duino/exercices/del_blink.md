---
title: Esercizio con il LED utente in C / C ++ per Stm32duino
description: Esercizio con il LED utente in C / C ++ per Stm32duino
---

# Esercizio con il LED utente in C / C ++ per Stm32duino

In questo esempio, vogliamo far lampeggiare un LED utente sul tuo STM32.

Una volta configurato l'IDE di Arduino per il tuo file STM32.

Apri la bozza `Blink` in `Fichier > Exemples > 01.Basics > Blink`

![Image](images/2_led_clignote/1.png)

Si aprirà quindi il programma per far lampeggiare un LED sul tuo STM32.

![Image](images/2_led_clignote/2.png)

Fare clic sul pulsante `Téléverser` (représenté par une flèche allant de gauche a droite)

![Image](images/2_led_clignote/3.png)

Fare clic sul pulsante Un LED sul tuo STM32 dovrebbe lampeggiare, nel nostro esempio (STM32 F446RE) è `LED2`, un LED di colore verde.
