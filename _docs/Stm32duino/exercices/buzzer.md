---
title: Esercizio con il 'buzzer' in C / C ++ per Stm32duino
description: Esercizio con il 'buzzer' in C / C ++ per Stm32duino
---
# Esercizio con il 'buzzer' in C / C ++ per Stm32duino

**Stesso prerequisito del sensore di inclinazione.**


**Il Buzzer :**


Il buzzer vibra e produce un suono quando viene trasmessa la tensione. È possibile modificare la frequenza del suono.

![Image](images/12_buzzer/buzzer.png)


*Ecco il codice su Arduino*
```c
int frequence[] = {262, 294, 330, 349, 370, 392, 440, 494};      

void setup()
{
  Serial.begin(9600);
  pinMode(D4,OUTPUT);             //il pin del buzzer è definito in uscita
}

void loop()
{
  for (int i = 0; i <= 8; i++)    //passiamo attraverso le 8 frequenze definite nella tabella sopra
  {
  tone(D4, frequence[i], 500);    //tone(Pin, frequenza, durata)
  noTone(D4);                     //fermare il suono sul pino interessato                      
  delay(500);                                                                                   
  }
}
```

> Credito immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
