---
title: Esercizio con la scheda di comunicazione LoRa / LoRaWAN I-NUCLEO-LRWAN1 in C / C ++ per Stm32duino
description: Esercizio con la scheda di comunicazione LoRa / LoRaWAN I-NUCLEO-LRWAN1 in C / C ++ per Stm32duino
---
# Esercizio con la scheda di comunicazione LoRa / LoRaWAN I-NUCLEO-LRWAN1 in C / C ++ per Stm32duino

Devi avere il file [carte de communication LoRa/LoRaWAN I-NUCLEO-LRWAN1](https://www.st.com/en/evaluation-tools/i-nucleo-lrwan1.html) per continuare questi esercizi.

Devi anche avere l'account su una rete LoRaWAN (TTN, CampusIoT, Orange, Objetnious) per registrare la tua carta LoRaWAN.

## Avvio

La scheda I-NUCLEO-LRWAN1 è una scheda di comunicazione LoRa / LoRaWAN dotata di modem AT USI. È dotato di un connettore Grove I2C e dei seguenti sensori MEMS di ST Microelectronics:

* LSM303AGR:  accelerometro e magnetometro
* LPS22HB: barometro
* HTS221: umidità e temperatura

Il connettore Grove e questi sensori sono collegati al bus I2C della scheda P-NUCLEO-WB55.

![image](images/i-nucleo-lrwan1.jpg)

Lo schema della mappa è disponibile [ici](https://github.com/USIWP1Module/USI_I-NUCLEO-LRWAN1/blob/master/Schematics/USI%20LoRa%20Arduino%20shield_SCH_20161115-1.pdf).


## Connessione

La scheda I-NUCLEO-LRWAN1 non può essere direttamente su più Nucleo 64 (F411RE ad esempio): i 6 pin della scheda I-NUCLEO-LRWAN1 devono essere collegati ai pin dei connettori Morpho del Nucleo come segue:

    I-NUCLEO-LRWAN1  --> Nucleo-64
    GND              --> GND   (CN7 Broche 16)
    3.3V             --> 3.3V  (CN7 Broche 20)
    RX/D0            --> PA_12 (CN10 Broche 12)
    TX/D1            --> PA_11 (CN10 Broche 14)
    SCL/D15          --> SCL (CN10 Broche 1)
    SDA/D14          --> SDA (CN10 Broche 3)

## Installazione delle librerie per la scheda di espansione IKS01A3

### Su Windows
Fare

### Su Linux
Fare

### Su MacOS
Immettere i seguenti comandi:
```bash
cd ~/Documents/Arduino/libraries/
for lib in I-NUCLEO-LRWAN1 LSM303AGR LPS22HB HTS221
do
wget https://codeload.github.com/stm32duino/${lib}/zip/master -O ${lib}-master.zip
unzip ${lib}-master.zip
done 
```

## Utilizzo

Avvia l'IDE di Arduino (precedentemente configurato per Stm32duino).

> Riavvia l'IDE di Arduino se gli esempi dalla libreria non compaiono nel menu `Fichier > Exemples`.

Apri la bozza di esempio `Fichier > Exemples > STM32Duino I-NUCLEO-LRWAN1 > getInfo`.

Modificare le seguenti righe.
```c
//HardwareSerial SerialLora(D0, D1);
HardwareSerial SerialLora(PA_12, PA_11);
```

Apri la console seriale che mostra le seguenti tracce:
```
-- Get Info sketch --
Firmware version: 1.1

LoRa stack version: 1.0.1
Unique DevEUI: 0xe24f43fffe441234
Application EUI: 0x2fa6ef9aa9df9ed8
Application key: 0x12345678123456781234567812345678
Network session Key: 0x75680f808410150f9c1fe349df6cc20d
Application session key: 0x11798c97be2ecbc596fd99a6e4d4a1d8
Device address: 0x12345678

```

Notare il ModEUI del modem.

Registra nuove apparecchiature sul server di rete utilizzando il tuo account con DevEUI, AppEUI dall'app e AppKey che scegli a caso.

Modifica un nuovo schizzo in base all'esempio `Fichier > Exemples > STM32Duino I-NUCLEO-LRWAN1 > LoRaWANOTAA`.

```c
#include "LoRaWANNode.h"

#define FRAME_DELAY 300000  // in ms. Every 5 minutes by default.

// Serial port use to communicate with the USI shield.
// By default, use D0 (Rx) and D1(Tx).
// For Nucleo64, see "Known limitations" chapter in the README.md

//HardwareSerial SerialLora(D0, D1);
HardwareSerial SerialLora(PA_12, PA_11);

// AppKey and AppEUI.
const char appKey[] = "12345678123456781234567812345678";
const char appEUI[] = "2fa6ef9aa9df9ed8";

// Data send
char frameTx[] = "Hello world!";

void setup()
{
  Serial.begin(115200);
  Serial.println("\n-- LoRaWAN OTAA sketch --");

  // Enable the USI module and set the radio band.
  while(!loraNode.begin(&SerialLora, LORA_BAND_EU_868)) {
    Serial.println("Lora module not ready");
    delay(1000);
  }

  String str = "DevEUI:  0x";
  loraNode.getDevEUI(&str);
  Serial.println(str);

  Serial.println("Joining ...");
  // Send a join request and wait the join accept
  while(!loraNode.joinOTAA(appKey, appEUI)) {
    Serial.println("joinOTAA failed!!");
    delay(1000);
  }

  Serial.println("Lora module ready, OTAA join accepted.\n");

  str = "AppEUI:  0x";
  loraNode.getAppEUI(&str);
  Serial.println(str);
  
  str = "AppKey:  0x";
  loraNode.getAppKey(&str);
  Serial.println(str);

  str = "DevAddr: 0x";
  loraNode.getDevAddr(&str);
  Serial.println(str);
}

void loop()
{
  receive();
  transmit();
  delay(FRAME_DELAY);
}

void receive(void) {
  uint8_t frameRx[64];
  uint8_t len;
  uint8_t port;

  // Check if data received from a gateway
  if(loraNode.receiveFrame(frameRx, &len, &port)) {
    uint8_t n = 0;
    Serial.print("frame received: 0x");
    while(len > 0) {
      Serial.print(frameRx[n], HEX);
      Serial.print(',');
      len--;
      n++;
    }
    Serial.print(" on port "); Serial.println(port);
  } else {
    Serial.println("No downlink data");
  }
}

void transmit(void) {
  // Send unconfirmed data to a gateway (port 1 by default)
  int status = loraNode.sendFrame(frameTx, sizeof(frameTx), UNCONFIRMED);
  if(status == LORA_SEND_ERROR) {
    Serial.println("Send frame failed!!!");
  } else if(status == LORA_SEND_DELAYED) {
    Serial.println("Module busy or duty cycle");
  } else {
    Serial.println("Uplink frame sent");
  }
}
```

Modificare il valore di AppKey e l'identificatore dell'applicazione AppEUI nello schizzo.

Compila e carica il binario prodotto.

Apri la console seriale che mostra le seguenti tracce:
```
DevEUI:  0xe24f43fffe441234
Joining ...
joinOTAA failed!!
joinOTAA failed!!
Lora module ready, OTAA join accepted.

AppEUI:  0x2fa6ef9aa9df9ed8
AppKey:  0x12345678123456781234567812345678
DevAddr: 0x12345678
No downlink data
Uplink frame sent
```

## Invio delle misurazioni del sensore tramite LoRaWAN

Crea un nuovo schizzo con il codice sottostante modificando i valori di AppKey e AppEUI.

```c
#include "LoRaWANNode.h"

#include <HTS221Sensor.h>
#include <LPS22HBSensor.h>
#include <LSM303AGR_ACC_Sensor.h>
#include <LSM303AGR_MAG_Sensor.h>

#define FRAME_DELAY 300000  // in ms. Every 5 minutes by default.

// Serial port use to communicate with the USI shield.
// By default, use D0 (Rx) and D1(Tx).
// For Nucleo64, see "Known limitations" chapter in the README.md

//HardwareSerial SerialLora(D0, D1);
HardwareSerial SerialLora(PA_12, PA_11);

#define DEV_I2C Wire

// Components.
HTS221Sensor  *HumTemp;
LSM303AGR_ACC_Sensor *Acc;
LSM303AGR_MAG_Sensor *Mag;
LPS22HBSensor *PressTemp;


// AppKey and AppEUI.
const char appKey[] = "e24f43fffe44cf482fa6ef9aa9df9ed8";
const char appEUI[] = "2fa6ef9aa9df9ed8";

// Data send
struct __attribute__((packed)) frameTx_t {
  int16_t temperature;
  uint8_t humidity;
};
struct frameTx_t frameTx;

// Data send
//char frameTx[] = "Hello world!";

void setupSensors() {
  // Initialize I2C bus.
  DEV_I2C.begin();

  // Initlialize components.
  HumTemp = new HTS221Sensor (&DEV_I2C);
  HumTemp->Enable();

  PressTemp = new LPS22HBSensor(&DEV_I2C);
  PressTemp->Enable();

  Acc = new LSM303AGR_ACC_Sensor(&DEV_I2C);
  Acc->Enable();
  Acc->EnableTemperatureSensor();
  Mag = new LSM303AGR_MAG_Sensor(&DEV_I2C);
  Mag->Enable();

}

void readSensors() {
  
  // Read humidity and temperature.
  float temp1, hum;
  HumTemp->GetTemperature(&temp1);
  HumTemp->GetHumidity(&hum);

  Serial.print("HTS221   : ");
  Serial.print("Hum[%]: ");
  Serial.print(hum, 0);
  Serial.print(" | Temp[C]: ");
  Serial.print(temp1, 2);
  Serial.println(" |");

  frameTx.humidity = hum;
  frameTx.temperature = temp1 * 100;

  float pressure, temp2;
  PressTemp->GetPressure(&pressure);
  PressTemp->GetTemperature(&temp2);

  Serial.print("LPS22HB  : ");
  Serial.print("Pres[hPa]: ");
  Serial.print(pressure, 2);
  Serial.print(" | Temp[C]: ");
  Serial.print(temp2, 2);
  Serial.println(" |");

  // TODO add the frameTx

  // Read accelerometer LSM303AGR.
  int32_t accelerometer[3];
  Acc->GetAxes(accelerometer);

  // Read temperature LSM303AGR.
  float temp3;
  Acc->GetTemperature(&temp3);
  
  // Read magnetometer LSM303AGR.
  int32_t magnetometer[3];
  Mag->GetAxes(magnetometer);

  // Output data.
  Serial.print("LSM303AGR: ");
  Serial.print("Acc[mg]: ");
  Serial.print(accelerometer[0]);
  Serial.print(" ");
  Serial.print(accelerometer[1]);
  Serial.print(" ");
  Serial.print(accelerometer[2]);
  Serial.print(" | Mag[mGauss]: ");
  Serial.print(magnetometer[0]);
  Serial.print(" ");
  Serial.print(magnetometer[1]);
  Serial.print(" ");
  Serial.print(magnetometer[2]);
  Serial.print(" | Temp[C]: ");
  Serial.print(temp3, 2);
  Serial.println(" |");

  // TODO add the frameTx
}
void setup()
{
  Serial.begin(115200);
  Serial.println("\n-- LoRaWAN OTAA sketch --");

  setupSensors();

  // Enable the USI module and set the radio band.
  while(!loraNode.begin(&SerialLora, LORA_BAND_EU_868)) {
    Serial.println("Lora module not ready");
    delay(1000);
  }

  String str = "DevEUI:  0x";
  loraNode.getDevEUI(&str);
  Serial.println(str);

  Serial.println("Joining ...");
  // Send a join request and wait the join accept
  while(!loraNode.joinOTAA(appKey, appEUI)) {
    Serial.println("joinOTAA failed!!");
    delay(1000);
  }

  Serial.println("Lora module ready, OTAA join accepted.\n");

  str = "AppEUI:  0x";
  loraNode.getAppEUI(&str);
  Serial.println(str);
  
  str = "AppKey:  0x";
  loraNode.getAppKey(&str);
  Serial.println(str);

  str = "DevAddr: 0x";
  loraNode.getDevAddr(&str);
  Serial.println(str);
}

void loop()
{
  receive();
  transmit();
  delay(FRAME_DELAY);
}

void receive(void) {
  uint8_t frameRx[64];
  uint8_t len;
  uint8_t port;

  // Check if data received from a gateway
  if(loraNode.receiveFrame(frameRx, &len, &port)) {
    uint8_t n = 0;
    Serial.print("frame received: 0x");
    while(len > 0) {
      Serial.print(frameRx[n], HEX);
      Serial.print(',');
      len--;
      n++;
    }
    Serial.print(" on port "); Serial.println(port);
  } else {
    Serial.println("No downlink data");
  }
}

void transmit(void) {
  readSensors();
  
  // Send unconfirmed data to a gateway (port 1 by default)
  int status = loraNode.sendFrame((char*)&frameTx, sizeof(frameTx), UNCONFIRMED);
  //int status = loraNode.sendFrame(frameTx, sizeof(frameTx), UNCONFIRMED);
  if(status == LORA_SEND_ERROR) {
    Serial.println("Send frame failed!!!");
  } else if(status == LORA_SEND_DELAYED) {
    Serial.println("Module busy or duty cycle");
  } else {
    Serial.println("Uplink frame sent");
  }
}
```


Apri la console seriale che mostra le seguenti tracce:
```
-- LoRaWAN OTAA sketch --
DevEUI:  0xe24f43fffe441234
Joining ...
joinOTAA failed!!
joinOTAA failed!!
Lora module ready, OTAA join accepted.

AppEUI:  0x2fa6ef9aa9df9ed8
AppKey:  0x12345678123456781234567812345678
DevAddr: 0x12345678
No downlink data
HTS221   : Hum[%]: 0 | Temp[C]: 17.00 |
LPS22HB  : Pres[hPa]: 31.96 | Temp[C]: -1.30 |
LSM303AGR: Acc[mg]: 0 0 0 | Mag[mGauss]: 0 0 0 | Temp[C]: 26.67 |
Uplink frame sent
```


## Documentazione

Le sorgenti della libreria sono in [https://github.com/stm32duino/I-NUCLEO-LRWAN1](https://github.com/stm32duino/I-NUCLEO-LRWAN1)

È disponibile il datasheet del modulo I-NUCLEO-LRWAN1 [ici](https://github.com/USILoRaModule/USI_I-NUCLEO-LRWAN1)


[LoRaWAN standard](https://www.lora-alliance.org)

