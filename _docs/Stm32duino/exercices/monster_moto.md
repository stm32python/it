---
title: Esercizio di guida a motore con la scheda SparkFun Monster Moto Shield in C / C ++ con Stm32duino
description: Esercizio di guida a motore con la scheda SparkFun Monster Moto Shield in C / C ++ con Stm32duino
---

# Esercizio di guida a motore con la scheda SparkFun Monster Moto Shield in C / C ++ con Stm32duino

## La SparkFun Monster Moto Shield Card
La scheda [SparkFun Monster Moto Shield](https://www.sparkfun.com/products/retired/10182) è una scheda per pilotare due potentissimi motori DC. È dotato di due componenti di controllo del motore [VNH2SP30](http://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/10832.pdf) di ST per l'industria automobilistica.

Sfortunatamente, non è più prodotto e distribuito da Sparkfun. Tuttavia, è ancora distribuito da diversi fornitori. Il suo schema è disponibile in open-source.

<div align="center">
<img alt="SparkFun Monster Moto Shield" src="https://air.imag.fr/images/2/28/MonsterMotoShield%2BSTNucleoF401.jpg" width="800px">
</div>

## Avvio
Collega la scheda SparkFun Monster Moto Shield alla scheda Nucleo.

Compila e scarica la bozza [MonsterMoto.ino](https://gitlab.com/stm32python/stm32duino/-/tree/master/MonsterMoto) sulla carta Nucleo.

Visualizza il monitor seriale (configurato a 9600 baud).

## Suggerimenti per gli esercizi
* Aggiungere alla bozza precedente la scheda MEMS IKS01Ax per correggere la traiettoria del robot utilizzando il magnetometro LIS2MDL.
* Aggiungere alla bozza precedente il sensore a ultrasuoni per evitare gli ostacoli.
* Crea un'applicazione di controllo remoto del motore con MIT App Inventor.

## Riferimenti

* [Fonti di codice su GitHub](https://github.com/sparkfun/Monster_Moto_Shield/tree/Hv12Fv10)

> Credito d'immagine : [Air](https://air.imag.fr/images/2/28/MonsterMotoShield%2BSTNucleoF401.jpg)
