---
title: Esercizi con Stm32duino in C / C ++
description: Esercizi con Stm32duino in C / C ++

---
# Esercizi con Stm32duino in C / C ++

Ecco l'elenco degli esercizi con Stm32duino in C / C ++ :

- [LED](del_blink)
- [Sensore Grove](grove)
- [Shock](choc)
- [LED - Lampeggiante](del)
- [Luminosità](luminosite)
- [Potenziometro](potentiometre)
- [Temperatura](temperature)
- [Joystick](joystick)
- [Sensore tattile](toucher)
- [Distanza degli ultrasuoni](ultrason)
- [Buzzer](buzzer)
- [Sensore del suono](bruit)
- [Scheda di espansione MEMS IKS01A1](iks01a1)
- [Scheda di espansione MEMS IKS01A3](iks01a3)
- [Scheda GNSS X-NUCLEO-GNSS1A1](x-nucleo-gnss1a1)
- [Scheda di comunicazione LoRa I-NUCLEO-LRWAN1](i-nucleo-lrwan1)
- [Comunicazione con Firmata](firmata)
- [Gestione dei motori con la scheda SparkFun Monster Moto Shield](monster_moto)
- [Display LCD a 16 caratteri x 2 righe](lcd_16x2) (prossimamante)
- [Modulo GPS UART SIM28](gps) (prossimamente)
- [Nintendo Nunchuk](nunchuk)
- [Nintendo SNES](snes)
- [Sonda di temperatura impermeabile DS1820](ds1820) (prossimamente)
