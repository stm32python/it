---
title: Esercizio con il sensore di distanza a ultrasuoni Grove in C / C ++ per Stm32duino
description: Esercizio con il sensore di distanza a ultrasuoni Grove in C / C ++ per Stm32duino
---
# Esercizio con il sensore di distanza a ultrasuoni Grove in C / C ++ per Stm32duino

**- Prerequisito :**


**- Il sensore a ultrasuoni (Ultrasonic Ranger):**

Questo sensore consente la misurazione precisa delle distanze tramite ultrasuoni. Un trasduttore a ultrasuoni emette ultrasuoni che poi rimbalzano sulle pareti, un secondo trasduttore ricevente riceve queste onde. È quindi possibile calcolare la distanza percorsa dagli ultrasuoni. Puoi misurare distanze da pochi centimetri a uno o due metri.
Questo sensore è simile ai sensori di retromarcia delle auto.


![Image](images/11_ultrason/capteur_ultrason.png)

 *T --> trasduttore del trasmettitore (transmetteur)*  |  *R --> trasduttore ricevitore *

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
