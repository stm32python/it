---
title: Esercizio con il display LCD Grove 16x2 in C / C ++ con Stm32duino
description: Esercizio con il display LCD Grove 16x2 in C / C ++ con Stm32duino
---
# Esercizio con il display LCD Grove 16x2 in C / C ++ con Stm32duino

<div align="center">
<img alt="Grove - LCD 16x2 Display" src="images/grove-lcd_16x2_display.jpg" width="600px">
</div>

Prossimamente

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
