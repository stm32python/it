---
title: Esercizio con il modulo GPS SIM28 Grove in C / C ++ con Stm32duino
description: Esercizio con il modulo GPS SIM28 Grove in C / C ++ con Stm32duino
---

# Esercizio con il modulo GPS SIM28 Grove in C / C ++ con Stm32duino

<div align="center">
<img alt="Grove - GPS" src="images/grove-gps.jpg" width="400px">
</div>

Prossimamente

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
