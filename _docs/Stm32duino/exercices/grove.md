---
title: Esercitazione con i sensori Grove in C / C ++ per Stm32duino
description: Esercitazione con i sensori Grove in C / C ++ per Stm32duino
---
# Esercitazione con i sensori Grove in C / C ++ per Stm32duino

- **Prerequisito :**

Il kit del sensore fornisce uno *shield* (immagine sotto). Sarà utilizzato per collegare un sensore alla scheda.
Basta collegarlo alla scheda.
![Image](images/3_capteurs_grove/shield.png)   ![Image](images/3_capteurs_grove/shield_carte.png)  

Apri Arduino e verifica che la porta sia collegata: Strumenti / Porta, COM3 dovrebbe essere selezionato.
- **Sensore di inclinazione :**

![Image](images/3_capteurs_grove/tiltsensorim.png)

Il "tilt-sensor", sensore di inclinazione in Italiano, è un sensore che misura la posizione di inclinazione in relazione alla gravità. La palla nel sensore (influenzata dal movimento del sensore) rotola e fa contatto.

*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600); // inizializzazione della connessione seriale
  pinMode(D4,INPUT);
}

void loop() {
  boolean etatContact=digitalRead(D4);
  if (etatContact)
    Serial.println("Contact");
  else
    Serial.println("Pas de contact");
}
```

Controlla e carica.
Per visualizzare lo stato in cui si trova il sensore, fare clic su *monitor seriale*

![Image](images/3_capteurs_grove/moniteur_serie.png)

> Credito d'immagine: [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
