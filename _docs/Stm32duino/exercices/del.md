---
title: Esercitazione con Grove LED in C / C ++ per Stm32duino
description: Esercitazione con Grove LED in C / C ++ per Stm32duino
---
# Esercitazione con Grove LED in C / C ++ per Stm32duino

- **Prerequisiti :**

*Fare riferimento al tutorial sul sensore di inclinazione *: collegheremo il led al pin D4.


![Image](images/5_LED_externe/led.png)  


Il codice proposto è molto semplice e consiste nel far lampeggiare il nostro led in un loop dopo un'inizializzazione del pin.


*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,OUTPUT);
}

void loop() {
  digitalWrite(D4,HIGH);    // accensione
  delay(1000);              // maggiore è il valore del parametro, più lento lampeggia il LED
  digitalWrite(D4,LOW);     // spegnimento
  delay(1000);
}
```

*Nota*: Se il led utilizzato è un led a infrarossi, è possibile verificare che stia lampeggiando con la fotocamera del telefono!

> Credito di immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
