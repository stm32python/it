---
title: Esercizio con il controller Nintendo SNES in C / C ++ con Stm32duino
description: Esercizio con il controller Nintendo SNES in C / C ++ con Stm32duino
---

# Esercizio con il controller Nintendo SNES in C / C ++ con Stm32duino

<h2>Descrizione</h2>
La console Nintendo Super NES (o SNES) è controllata da un controller con le seguenti caratteristiche:
  - Una croce direzionale (su, giù, destra, sinistra)
  - Quattro pulsanti alla sua destra (A, B, X e Y)
  - Due pulsanti al centro (START e SELECT)
  - Due pulsanti sul bordo anteriore (L e R)

Comunica col protocollo [SPI](https://stm32python.gitlab.io/fr/docs/Micropython/glossaire) con la console. È un protocollo di comunicazione che funziona sul principio del master che invia le informazioni (qui il controller) e dello slave che le riceve (qui la console sostituita dalla scheda Nucleo).


<h2>Montaggio</h2>

Per collegare il controller alla scheda Nucleo, utilizziamo 5 fili e una resistenza da 10kΩ. Possiamo benissimo collegare il controller alla scheda Nucleo senza usare resistenza. Tuttavia, nel nostro caso, verrà utilizzato per rilevare che il controller è disconnesso.

La connessione del controller SNES è organizzata come segue:

<div align="center">
<img alt="Schéma de Montaggio manette SNES" src="images/snes-schema.png" width="800px">
</div>


| Controller SNES   | ST Nucleo         |
| :-------------:   | :---------------: |
|        5V         |         5V        |
|      CLOCK        |         D3        |
|      LATCH        |         D2        |
|      DATA         |         D4        |
|      GND          |         GND       |

Senza dimenticare la resistenza che si pone tra GND e DATA.


<h2>Programma</h2>

Per semplificare il codice utilizziamo una libreria esterna. Per recuperare questa libreria, segui questo [link](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Snes/Library/Snes.zip).È contenuto in un file .zip che deve essere decompresso e quindi spostato nella cartella con il suo contenuto per inserirlo in *\Documents\Arduino\libraries*. Il file di progetto Arduino è disponibile seguendo questo [link](https://gitlab.com/stm32python/fr/-/blob/master/assetsScript/Snes/snes_controller.ino).

**Passo 1 :** Per far funzionare il programma, dobbiamo prima importare la libreria scaricata in precedenza. Per farlo, dobbiamo importarlo all'inizio del nostro codice in questo modo:
```
#include <Snes.h>
```

Creiamo anche solo una variabile * snes * che recupera le informazioni restituite dalla libreria.
```
Snes snes;
```

**Passo 2 :** Quindi inizializziamo la libreria che sarà quindi responsabile dell'attivazione della comunicazione seriale e dell'inizializzazione della comunicazione con il controller.
```
void setup()
{
  snes.init();
}
```

**Passo 3 :** Infine visualizziamo i dati acquisiti. La libreria è responsabile dell'acquisizione dei dati e della visualizzazione diretta del risultato.
```
void loop()
{
  //Per recuperare i dati e visualizzarli
  snes.print();
}
```

**Passo 3 (bis) :** Se vogliamo recuperare i dati visualizzando solo il valore grezzo ricevuto:
```
void loop()
{
  //Per recuperare solo i dati
  int donnee = snes.data();
  Serial.println(donnee);
  delay(250);
}
```
Notare che in questo caso il valore recuperato sarà un multiplo di 2 (binario) e ogni valore corrisponderà a un pulsante specifico.


<h2>Risultato</h2>

Tutto quello che devi fare è premere il pulsante *upload* per trasferire il programma, quindi andare al menu *strumenti* quindi *monitor seriale* per osservare il risultato!

Possiamo ora osservare i dati estratti dal controller SNES in questa forma:


<div align="center">
<img alt="Affichage des données de la manette SNES" src="images/snes-c.png" width="400px">
</div>


Premi i diversi pulsanti sul controller e il pad direzionale e dovresti vedere il feedback sullo schermo.

