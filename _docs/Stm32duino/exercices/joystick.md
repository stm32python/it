---
title: Esercizio con il joystick Grove in C / C ++ per Stm32duino
description: Esercizio con il joystick Grove in C / C ++ per Stm32duino
---
# Esercizio con il joystick Grove in C / C ++ per Stm32duino

- **Prerequisiti :**

Guarda il tutorial sul sensore di inclinazione. Tuttavia è necessario collegare il joystick al pin A0.

- **Il joystick del pollice :**


![Image](images/9_joystick/joystick.png)


Questo joystick è simile a quelli che si possono trovare su un controller PS2. Ciascuno degli assi è collegato a un potenziometro che fornirà una tensione corrispondente. Inoltre, il joystick ha un pulsante.

*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                //en branchant le joystick sur A0,l'axe Y sera lu par A0 et l'axe X par A1
  pinMode(A1,INPUT);                //sur A1, Y sera lu par A1 et X par A2; sur A2, Y sera lu par A2 et X par A3...
}

void loop() {

  int X=analogRead(A0);
  int Y=analogRead(A1);
  if(X<=780 && X>=750)
  {
    Serial.println("Haut");
  }
  if(X<=280 && X>=240)
  {
    Serial.println("Bas");
  }
  if(Y<=780 && Y>=750)
  {
    Serial.println("Gauche");
  }
  if(Y<=280 && Y>=240)
  {
    Serial.println("Droite");
  }
  if(X>=1000)                     //en appuyant sur le joystick, la sortie de l'axe X se met à 1024, le maximum.
  {
    Serial.println("Appuyé");
  }
  delay(1000);

}
```

Possiamo quindi controllare il nostro input nel monitor seriale.

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
