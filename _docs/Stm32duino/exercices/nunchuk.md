---
title: Esercizio con l'adattatore Nintendo Nunchuk Grove in C / C ++ con Stm32duino
description: Esercizio con l'adattatore Nintendo Nunchuk Grove in C / C ++ con Stm32duino
---

# Esercizio con l'adattatore Nintendo Nunchuk Grove in C / C ++ con Stm32duino

<h2>Descrizione</h2>
Il Nunchuk è un controller aggiuntivo che si collega al WiiMote (un controller progettato per essere utilizzato per la console Nintendo Wii).
Il Nunchuk è costituito dalle seguenti caratteristiche :
 - Joystick a 2 assi (x, y)
 - Accelerometro a 3 assi (x, y, z)
 - Pulsanti (C et Z)

Comunica col protocollo [I2C](https://stm32python.gitlab.io/fr/docs/Micropython/glossaire) attraverso il Wiimote. Una modalità di comunicazione che richiede solo 4 fili(5V, GND, SDA, SCL).


<h2>Montaggio</h2>

Per poter utilizzare il controller utilizziamo un connettore UEXT to Grove come questo: 
<div align="center">
<img alt="Grove - Nintendo Nunchuk Adapter" src="images/grove-nunchuk.jpg" width="200px">
</div>

Devi quindi venire e collegare il controller a un connettore I2C al Nucleo utilizzando le porte Arduino. Veniamo a connetterci in questo modo:

| Connettore UEXT   | ST Nucleo         |
| :-------------:   | :---------------: |
|       pwr         |        3.3V       |
|       gnd         |        GND        |
|        d          |        D14        |
|        c          |        D15        |




<h2>Programma</h2>
Per semplificare il codice utilizziamo una libreria esterna. Per fare ciò, vai all'IDE di Arduino, seleziona il menu * Strumenti * quindi fai clic sul menu * Gestisci librerie *. Si apre una nuova finestra, nella barra di ricerca digita: * estensione nintendo ctrl * quindi installa la libreria proposta (qui nella versione 0.8.1). Il file di progetto Arduino è disponibile tramite questo [link](https://gitlab.com/stm32python/fr/-/blob/master/assets/Script/Nunchuk/Nunchuk.ino).

**Passo 1 :** Per far funzionare il programma, dobbiamo prima importare la libreria scaricata in precedenza. Per fare ciò, dobbiamo importarlo all'inizio del nostro codice in questo modo: 
```
#include <NintendoExtensionCtrl.h>
```

Creiamo anche una variabile *nchuck* che recupera le informazioni restituite dalla libreria.
```
Nunchuk nchuk;
```

**Passo 2 :** Quindi configuriamo il ciclo * setup * inizializzando il collegamento seriale in modo da avere le informazioni nel monitor seriale e inizializziamo anche la libreria. 
```
void setup() {
  Serial.begin(115200);
  nchuk.begin();

  while (!nchuk.connect()) {
    Serial.println("Controller non rilevato !");
    delay(1000);
  }
}
```

**Passo 3 :** Enfin nous mettons à jour les données acquises et on vient les afficher ou non en fonction de la connection du Nunchuk
```
void loop() {
  boolean success = nchuk.update();     //Ripristino dei dati del controller

  if (success == true) {                //Se la connessione ha esito positivo
    nchuk.printDebug();                 //Visualizzazione dei valori
    delay(500);
  }
  else {                                //Se il controller è scollegato
    Serial.println("Controller disconnesso!");
    delay(1000);
    nchuk.connect();
  }
}
```


<h2>Risultato</h2>

Tutto quello che devi fare è premere il pulsante *upload* per trasferire il programma, quindi andare al menu *strumenti* quindi *monitor seriale* per osservare il risultato !

Possiamo ora osservare i dati estratti dal Nunchuk in questa forma:

<div align="center">
<img alt="Affichage des données du Nunchuk" src="images/nunchuk-c.png" width="800px">
</div>


Fai un movimento con il Nunchuk e dovresti vedere i valori dell'accelerometro variare a seconda dell'asse su cui lo hai spostato. Puoi anche muovere il joystick e vedere dove si trova. Inoltre hai un feedback sullo stato dei pulsanti.

**Nota :** In caso di problemi, controlla di essere connesso alla porta COM corretta nell'IDE di Arduino. Se il problema proviene dal monitor seriale, verificare che sia correttamente configurato a 115200 baud, altrimenti verificare il collegamento del controller.


<h2>Per andare oltre</h2>

In questo esercizio abbiamo visto la visualizzazione dei dati del Nunchuk. Per andare oltre possiamo, ad esempio, recuperare questi dati ed elaborarli per attivare i servomotori in base alla direzione del joystick.

(presto)


> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)

