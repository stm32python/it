---
title: Esercizio con il sensore del suono in C / C ++ per Stm32duino
descripttion: Esercizio con il sensore del suono in C / C ++ per Stm32duino
---
# Esercizio con il sensore del suono in C / C ++ per Stm32duino

- **Prerequisiti:**

Guarda il tutorial sul sensore di inclinazione. Tuttavia, è necessario collegare il sensore al pin A0.


- **Sensore del suono:**

![Image](images/13_capteur_son/capteur_sonore.png)


Questo sensore può essere utilizzato come rilevatore di livello sonoro, infatti la sua uscita è proporzionale al livello sonoro circostante.

*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                //ingresso rilevatore di suono per leggere il suo valore

}

void loop() {
  Serial.println(analogRead(A0));   //ne mostriamo il valore (compreso tra 0 e 1024).
  delay(100);                       //In effetti il ​​risultato non è in decibel.

}
```

È possibile osservare l'evoluzione dei valori restituiti dal sensore sonoro tramite il plotter seriale accessibile in Strumenti / Plotter seriale o utilizzando la scorciatoia Ctrl + Maiusc + N.

![Image](images/13_capteur_son/graphe_capteur_sonore.png)

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
