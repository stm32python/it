---
title: Esercizio con la scheda di espansione IKS01A3 in C / C ++ per Stm32duino
description: Esercizio con la scheda di espansione IKS01A3 in C / C ++ per Stm32duino
---
# Esercizio con la scheda di espansione IKS01A3 in C / C ++ per Stm32duino

Avrai bisogno della scheda di espansione IKS01A3 per continuare questi esercizi.

## Avvio

La scheda di espansione IKS01A3 è una scheda dimostrativa di diversi sensori MEMS di ST Microelectronics. La sua versione A3 contiene i seguenti sensori:

* LSM6DSO : Accelerometro 3D + Giroscopio 3D
* LIS2MDL : Magnetometro 3D
* LIS2DW12 : Accelerometro 3D
* LPS22HH : Barometro (260-1260 hPa)
* HTS221 : Sensore di umidità relativa
* STTS751 : Termometro (–40 °C to +125 °C) 

Questi sensori sono collegati al bus I2C della scheda P-NUCLEO-WB55.

La scheda di espansione IKS01A3 ha anche uno slot in formato DIL a 24 pin per aggiungere ulteriori sensori I2C (ad esempio, il giroscopio [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a3.png)

## Connessione

Collegare la scheda IKS01A3, fare attenzione a rispettare la marcatura del connettore:CN9 -> CN9, CN5 -> CN5, etc...

Una volta collegata correttamente la scheda di espansione, ecco l'elenco dei sensori con i quali è possibile comunicare tramite I2C.

## Installazione delle librerie per la scheda di espansione IKS01A3

### Su Windows
Fare

### Su Linux
Fare

### Sur MacOS
Immettere i seguenti comandi:
```bash
cd ~/Documents/Arduino/libraries/
for lib in X-NUCLEO-IKS01A3 LPS22HH LSM6DSO LIS2DW12 LIS2MDL LPS22HH STTS751 HTS221
do
wget https://codeload.github.com/stm32duino/${lib}/zip/master -O ${lib}-master.zip
unzip ${lib}-master.zip
done 
```

## Utilizzo

Avvia l'IDE di Arduino (precedentemente configurato per Stm32duino)

Apri la bozza di esempio `Fichier > Exemples > STM32Duino X-NUCLEO-IKS01A3 > X_NUCLEO_IKS01A3_HelloWorld`.

Collegare la scheda Nucleo fornita con la scheda IKS01A3.

Compila e carica il binario prodotto.

Apri la console seriale che mostra le seguenti tracce:
```
| Hum[%]: 73.30 | Temp[C]: 18.10 | Pres[hPa]: 984.41 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -96 26 1008 | Gyr[mdps]: 350 840 -560 | Acc2[mg]: 14 111 962 | Mag[mGauss]: -214 456 -574 |
| Hum[%]: 73.30 | Temp[C]: 18.10 | Pres[hPa]: 984.51 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -83 27 1005 | Gyr[mdps]: 770 210 -630 | Acc2[mg]: 14 96 952 | Mag[mGauss]: -214 462 -573 |
| Hum[%]: 73.60 | Temp[C]: 18.10 | Pres[hPa]: 984.44 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -87 21 1010 | Gyr[mdps]: 280 490 -560 | Acc2[mg]: 7 101 963 | Mag[mGauss]: -196 469 -567 |
| Hum[%]: 73.60 | Temp[C]: 18.10 | Pres[hPa]: 984.40 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -93 -2 1022 | Gyr[mdps]: 1470 -1960 -840 | Acc2[mg]: -15 102 974 | Mag[mGauss]: -199 469 -570 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.54 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -26 45 984 | Gyr[mdps]: 1960 -70 -350 | Acc2[mg]: 34 37 936 | Mag[mGauss]: -211 468 -568 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.46 | Temp2[C]: 18.52 | Temp3[C]: 19.00 | Acc[mg]: -93 31 1004 | Gyr[mdps]: 0 980 -490 | Acc2[mg]: 18 108 959 | Mag[mGauss]: -202 451 -570 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.41 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -88 50 1005 | Gyr[mdps]: 1330 -280 -630 | Acc2[mg]: 39 98 964 | Mag[mGauss]: -213 466 -568 |
| Hum[%]: 73.70 | Temp[C]: 18.10 | Pres[hPa]: 984.46 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -57 42 1017 | Gyr[mdps]: 420 -280 -560 | Acc2[mg]: 31 76 965 | Mag[mGauss]: -213 454 -570 |
| Hum[%]: 73.80 | Temp[C]: 18.20 | Pres[hPa]: 984.53 | Temp2[C]: 18.53 | Temp3[C]: 19.00 | Acc[mg]: -91 22 1005 | Gyr[mdps]: 350 840 -420 | Acc2[mg]: 9 104 955 | Mag[mGauss]: -199 468 -565 |
| Hum[%]: 73.80 | Temp[C]: 18.20 | Pres[hPa]: 984.43 | Temp2[C]: 18.54 | Temp3[C]: 19.00 | Acc[mg]: -75 18 1025 | Gyr[mdps]: 420 -630 -490 | Acc2[mg]: 4 87 974 | Mag[mGauss]: -217 460 -564 |

```

Spostare la scheda per variare i valori dei sensori Acc, Gyr, Acc2, Mag.






