---
title: Esercizio con il sensore di temperatura Grove in C / C ++ per Stm32duino
description: Esercizio con il sensore di temperatura Grove in C / C ++ per Stm32duino
---
# Esercizio con il sensore di temperatura Grove in C / C ++ per Stm32duino

Come per gli altri sensori, useremo lo scudo che inseriremo nella scheda.
Apri Arduino e verifica che la porta sia collegata: Strumenti/Porta, COM3 dovrebbe essere selezionato.

- **Termometro :** *- Temperature sensor*

![Image](images/8_temperature/capteur_temp.png)

Questo sensore di temperatura utilizza un termistore (resistenza che diminuisce con il calore) per misurare la temperatura ambiente. La resistenza cambia l'uscita di un partitore di tensione che viene misurata da un pin di ingresso analogico. Questo valore dovrà essere convertito in un valore di temperatura nel nostro programma.
L'intervallo operativo è compreso tra -40 e 125 ° C, con una precisione di 1,5 ° C.

Il sensore deve essere collegato al pin A0

*Ecco il codice su Arduino IDE*
```c
//definizione delle costanti necessarie per la conversione.
float R1 = 10000;
float logR2, R2, T;
float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741; //Coefficienti di Steinhart-Hart per la conversione

void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                                  // mettiamo il sensore in ingresso analogico
}

void loop() {
  int Vo = analogRead(A0);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);               //calcolare la resistenza del termistore
  logR2 = log(R2);                                    //abbiamo convertito Vo in float invece che int per il calcolo
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)); // imposta la temperatura in Kelvin.
  T = T - 273.15;                                     // convertire Kelvin in Celsius.
 // T = (T * 9.0)/ 5.0 + 32.0;                        // conversione da Celsius a Fahrenheit.

  Serial.print("Temperatura: ");
  Serial.print(T);
  //Serial.println(" K");
  Serial.println(" C");
  //Serial.println(" F");

  delay(500);
}
```

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
