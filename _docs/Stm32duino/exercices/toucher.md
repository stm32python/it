---
title: Esercizio con il sensore tattile Grove in C / C ++ per Stm32duino
description: Esercizio con il sensore tattile Grove in C / C ++ per Stm32duino
---
# Esercizio con il sensore tattile Grove in C / C ++ per Stm32duino

- **Prerequisito :**

**Sensore tattile (Touch sensor):**
Questo sensore ha solo 3 pin che devono essere collegati con i cavi sulla scheda di schermatura in modo che GND e VCC (rosso e nero) corrispondano bene. Il cavo giallo (inserito nel pin SIG) sarà collegato a D4.

![Image](images/10_toucher/capteur_tactile.png)

**Motore a vibrazione (vibration motor):**
Questo motore a vibrazione è esattamente come quelli che si possono trovare nei nostri telefoni. Sarà collegato a D3.
![Image](images/10_toucher/vibration_motor.png)


Il sensore tattile sarà configurato come ingresso e la vibrazione motore come uscita: se c'è contatto con il sensore tattile si attiverà la vibrazione motore.

*Ecco il codice su Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,INPUT);
  pinMode(D3,OUTPUT);

}

void loop() {

  if(digitalRead(D4))         
  {
    digitalWrite(D3,HIGH);
  }
  else
  {
    digitalWrite(D3,LOW);
  }
  delay(500);
}
```

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
