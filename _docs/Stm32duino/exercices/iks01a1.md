---
title: Esercizio con la scheda di espansione IKS01A1 in C / C ++ per Stm32duino
description: Esercizio con la scheda di espansione IKS01A1 in C / C ++ per Stm32duino
---
# Esercizio con la scheda di espansione IKS01A1 in C / C ++ per Stm32duino

Avrai bisogno del file [carte d’extension IKS01A1](https://www.st.com/en/ecosystems/x-nucleo-iks01a1.html) per continuare questi esercizi.

La [carte d’extension IKS01A1](https://www.st.com/en/ecosystems/x-nucleo-iks01a1.html) è la vecchia versione di [carte d’extension IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html)  

## Avvio

La scheda di espansione IKS01A1 è una scheda dimostrativa di diversi sensori MEMS di ST Microelectronics. La sua versione A3 contiene i seguenti sensori:

* LSM6DS0:  accelerometro 3D (±2/±4/±8 g) + giroscopio 3D (±245/±500/±2000 dps)
* LIS3MDL: magnetometro 3D (±4/ ±8/ ±12/ 16 gauss)
* LPS25HB: barometro 260-1260 hPa
* HTS221: umidità e temperatura


Questi sensori sono collegati al bus I2C della scheda P-NUCLEO-WB55.

La scheda di espansione IKS01A1 dispone anche di uno slot in formato DIL a 24 pin per aggiungere ulteriori sensori I2C (ad esempio, il giroscopio [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)) 

![image](images/iks01a1.png)

## Connessione

Collegare la scheda IKS01A1 facendo attenzione a rispettare la marcatura del connettore : CN9 -> CN9, CN5 -> CN5, ecc...

Una volta collegata correttamente la scheda di espansione, ecco l'elenco dei sensori con i quali è possibile comunicare tramite I2C.

## Installazione delle librerie per la scheda di espansione IKS01A3

### Su Windows
Fare

### Sur Linux
Fare

### Sur MacOS
Immettere i seguenti comandi:
```bash
cd ~/Documents/Arduino/libraries/
for lib in X-NUCLEO-IKS01A1 HTS221 LPS25HB LSM6DS0 LIS3MDL
do
wget https://codeload.github.com/stm32duino/${lib}/zip/master -O ${lib}-master.zip
unzip ${lib}-master.zip
done 
```

## Utilizzo

Avvia l'IDE di Arduino (precedentemente configurato per Stm32duino)

Apri la bozza di esempio `Fichier > Exemples > STM32Duino X-NUCLEO-IKS01A1 > X_NUCLEO_IKS01A1_HelloWorld`.

Collegare la scheda Nucleo fornita con la scheda IKS01A3.

Compila e carica il binario prodotto.

Apri la console seriale che mostra le seguenti tracce:
```
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 992.06 | Temp2[C]: 17.30 | Acc[mg]: -96 86 1011 | Gyr[mdps]: 910 -1260 1260 | Mag[mGauss]: -20 2 63 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.03 | Temp2[C]: 17.30 | Acc[mg]: -94 84 1012 | Gyr[mdps]: 700 -1050 1540 | Mag[mGauss]: -17 2 56 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.10 | Temp2[C]: 17.40 | Acc[mg]: -96 87 1014 | Gyr[mdps]: 980 -1120 1260 | Mag[mGauss]: -21 4 62 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.03 | Temp2[C]: 17.40 | Acc[mg]: -141 89 1229 | Gyr[mdps]: 23310 91000 -76440 | Mag[mGauss]: 4 -4 78 |
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 991.98 | Temp2[C]: 17.40 | Acc[mg]: 411 641 -147 | Gyr[mdps]: 76440 -271110 100940 | Mag[mGauss]: 212 -259 201 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.14 | Temp2[C]: 17.40 | Acc[mg]: 511 409 -833 | Gyr[mdps]: 11970 -18060 1960 | Mag[mGauss]: 104 -328 788 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.08 | Temp2[C]: 17.40 | Acc[mg]: 568 662 -499 | Gyr[mdps]: 10640 177660 -256060 | Mag[mGauss]: 341 -412 534 |
| Hum[%]: 67.90 | Temp[C]: 20.80 | Pres[hPa]: 992.17 | Temp2[C]: 17.40 | Acc[mg]: -159 988 28 | Gyr[mdps]: 1330 1050 7000 | Mag[mGauss]: 474 -164 386 |
| Hum[%]: 68.30 | Temp[C]: 20.80 | Pres[hPa]: 992.23 | Temp2[C]: 17.40 | Acc[mg]: -271 983 179 | Gyr[mdps]: 13440 -3080 5320 | Mag[mGauss]: 471 -163 373 |
| Hum[%]: 67.80 | Temp[C]: 20.80 | Pres[hPa]: 992.05 | Temp2[C]: 17.50 | Acc[mg]: 390 -1069 156 | Gyr[mdps]: 38220 27720 24850 | Mag[mGauss]: -192 -330 416 |
| Hum[%]: 68.00 | Temp[C]: 20.80 | Pres[hPa]: 992.15 | Temp2[C]: 17.50 | Acc[mg]: 68 296 1162 | Gyr[mdps]: -9520 22750 118580 | Mag[mGauss]: 95 -103 91 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.12 | Temp2[C]: 17.50 | Acc[mg]: 420 21 1003 | Gyr[mdps]: 3990 -4200 6300 | Mag[mGauss]: 4 31 69 |
| Hum[%]: 67.60 | Temp[C]: 20.80 | Pres[hPa]: 992.19 | Temp2[C]: 17.50 | Acc[mg]: 39 153 1008 | Gyr[mdps]: 840 -1120 1260 | Mag[mGauss]: 138 -20 93 |
| Hum[%]: 67.40 | Temp[C]: 20.80 | Pres[hPa]: 992.18 | Temp2[C]: 17.50 | Acc[mg]: 41 151 1010 | Gyr[mdps]: 910 -1120 1330 | Mag[mGauss]: 140 -22 92 |
| Hum[%]: 67.20 | Temp[C]: 20.80 | Pres[hPa]: 992.04 | Temp2[C]: 17.50 | Acc[mg]: 42 151 1007 | Gyr[mdps]: -630 -1190 1400 | Mag[mGauss]: 131 -2 91 |

```

Spostare la scheda per variare i valori dei sensori Acc, Gyr, Mag.






