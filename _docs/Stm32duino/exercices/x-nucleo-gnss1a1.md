---
title: Esercitazione con la mappa di geolocalizzazione GNSS X-NUCLEO-GNSS1A1 in C / C ++ per Stm32duino
description: Esercitazione con la mappa di geolocalizzazione GNSS X-NUCLEO-GNSS1A1 in C / C ++ per Stm32duino
---
# Esercitazione con la mappa di geolocalizzazione GNSS X-NUCLEO-GNSS1A1 in C / C ++ per Stm32duino

Devi avere il file [carte de géolocalisation GNSS X-NUCLEO-GNSS1A1](https://www.st.com/en/ecosystems/x-nucleo-gnss1a1.html) per continuare questo esercizio.

## Avvio

La scheda X-NUCLEO-GNSS1A1 è una scheda di geolocalizzazione GNSS dotata di un modulo GNSS. È dotato di un modulo Teseo-LIV3F di ST Microelectronics.

![image](images/x-nucleo-gnss1a1.jpg)

## Connessione

Collegare la scheda X-NUCLEO-GNSS1A1, fare attenzione a rispettare la marcatura del connettore: CN9 -> CN9, CN5-> CN5, etc...

## Installazione delle librerie per la scheda di espansione IKS01A3

### Su Windows
Fare

### Su Linux
Fare

### Su MacOS
Immettere i seguenti comandi :
```bash
wget https://github.com/stevemarple/MicroNMEA/zip/master -O MicroNMEA-master.zip
unzip MicroNMEA-master.zip
wget https://github.com/stm32duino/X-NUCLEO-GNSS1A1/zip/master -O X-NUCLEO-GNSS1A1-master.zip
unzip X-NUCLEO-GNSS1A1-master.zip
```

## Funzionamento tramite UART

Avvia l'IDE di Arduino (precedentemente configurato per Stm32duino).

> Riavvia l'IDE di Arduino se gli esempi dalla libreria non vengono visualizzati nel menu `File > Esempi`.

Apri lA bozza di esempio `File > Esempi > STM32Duino X-NUCLEO-GNSS1A1 > X_NUCLEO_GNSS1A1_MicroNMEA_UART`.

Apri la console seriale che mostra le seguenti tracce:
```

```

## Utilizzo per l'I2C

Avvia l'IDE di Arduino (precedentemente configurato per Stm32duino).

> Riavvia l'IDE di Arduino se nel menu non compare gli esempi dalla libreria `File > Esempi`.

Apri la bozza di esempio `File > Esempi > STM32Duino X-NUCLEO-GNSS1A1 > X_NUCLEO_GNSS1A1_MicroNMEA_I2C`.

Apri la console seriale che mostra le seguenti tracce:
```

```

## Documentazione

* https://github.com/stm32duino/X-NUCLEO-GNSS1A1

* TESEO-LIV3F datasheet https://www.st.com/content/st_com/en/products/positioning/gnss-modules/teseo-liv3f.html
