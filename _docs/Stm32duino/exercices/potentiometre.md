---
title: Esercizio con il potenziometro Grove in C / C ++ per Stm32duino
description: Esercizio con il potenziometro Grove in C / C ++ per Stm32duino
---

# Esercizio con il potenziometro Grove in C / C ++ per Stm32duino

- **Prerequisito :**

Per questo sensore sarà necessario collegarlo al pin A0.
Apri Arduino e verifica che la porta sia collegata: Strumenti / Porta, COM3 dovrebbe essere selezionato.

- **Il potenziometro :**

![Image](images/7_potentiometre/potentiometre.png)

Il potenziometro, *rotary angle sensor* in inglese, restituisce un valore compreso tra 0 e 1024. Permette di modulare manualmente i valori da utilizzare (ad esempio per variare l'intensità sonora di un buzzer *vedi esempio di tp*) .

*Ecco il codice su Arduino*

```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);

}

void loop() {

  Serial.println(analogRead(A0));
  delay(500);

}
```
Il valore letto sul pin A0 viene visualizzato ogni 500ms.

> Credito d'immagine : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
