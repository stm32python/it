---
title: Stm32duino
description: Sezione Stm32duino

---
# Stm32duino

[Stm32duino](https://github.com/stm32duino) è un progetto che riunisce le librerie Arduino per le schede di sviluppo STM32 (Nucleo e Discovery) e per i componenti MEMS di ST Microelectronics. Permette di sviluppare e compilare programmi (es. _sketch_ ) dall'ambiente di sviluppo IDE di Arduino.


# Sommario

In questa parte troverai tutti gli esercizi [Stm32duino](https://github.com/stm32duino) per il kit didattico Stm32Python.

Prima di tutto, assicurati di avere un'installazione funzionale. Il protocollo da seguire per farlo è dettagliato nella sezione [Installazione](installazione).

 - [Installazione](installazione)
 - [Esercizi](esercizi)
