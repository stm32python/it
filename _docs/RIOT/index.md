---
title: Come utilizzare il kit STM32 MicroPython utilizzando RIOT OS 
description: Come utilizzare il kit STM32 MicroPython utilizzando RIOT OS
---
# Come utilizzare il kit STM32 MicroPython Kit utilizzando RIOT OS

## RIOT

[RIOT OS](http://riot-os.org/) è un sistema operativo in tempo reale per microcontrollori a memoria e a potenza limitata. RIOT OS supporta un gran numero di schede Nucleo e Discovery STM32. I programmi possono essere sviluppati in C, Lua, Javascript e MicroPython.
## Sommario

Troverete in questa parte esercizi da programmare in MicroPython con RIOT OS sul kit educativo Stm32Python.

Prima di tutto, assicuratevi di avere un'installazione ottimale. Il protocollo da seguire è nella sezione Installazione. 
Il protocollo da seguire è nella sezione [Installation](installazione).

 - [Installazione](installation)
 - [Esercizi](exercices)
