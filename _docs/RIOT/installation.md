---
title: RIOT
description: Come installare MicroPython su RIOT OS
---
# Installation de MicroPython sur RIOT OS

**Strumenti richiesti per l'installazione di MicroPython :**  
È necessario utilizzare un file __con una macchina virtuale Linux su Computer Windows__ installata o un file _direttamente su computer Linux_.

## Installazione di MicroPython RIOT per STM32 su un computer Linux  

Dal desktop Linux, apri un terminale facendo
  Fare clic con il pulsante destro del mouse, quindi "Apri un terminale qui"
  Quindi inserisci i seguenti comandi uno per uno
  per installare il software prerequisito.  
```
sudo apt-get install git
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install gcc-arm-none-eabi
sudo apt-get install cmake
sudo apt-get install lisusb-1.0
```
È inoltre necessario installare lo strumento __ST-LINK-UTILITY__
```
git clone https://github.com/texane/stlink
cd stlink
make release
cd build/Release
sudo make install
cd
sudo ldconfig
```
Per verificare se lo strumento è installato correttamente, puoi utilizzare il comando __*`st-info`*__


Una volta installato il prerequisito software, è necessario recuperare il progetto MicroPython su RIOT dallo strumento git scrivendo in un terminale (aperto da desktop come prima) i seguenti comandi:

```
git clone https://github.com/RIOT-OS/micropython
```
Prima di creare il firmware per una data scheda, è necessario creare il cross-compilatore MicroPython.
```
cd micropython
make -C mpy-cross
```
I sottomoduli devono essere prima ottenuti utilizzando:

```
cd ports/stm32
make submodules
```
Quindi, per creare una mappa:
```
make BOARD={Your-Board-Name}
```
È necessario sostituire __{Your-Board-Name}__ dal nome della scheda STM32 utilizzata.
Ad esempio, se utilizzi un file *NUCLEO F446RE*, sarà necessario scrivere il comando :  
__`make BOARD=NUCLEO_F446RE`__

Puoi verificare se la tua scheda è compatibile [ici](https://github.com/RIOT-OS/micropython/tree/master/ports/stm32/boards)

Dove sul terminale, nella directory *__micropython/ports/stm32/boards__*

**Applicare Micropython alla scheda**

È necessario inserire la scheda STM32 nel file  __mode DFU__ (*bootloader*).  Normalmente, devi collegare i pin 3v3 e BOOT0. L'immagine mostra come eseguire questa operazione per la scheda STM32F446RE.</p>


![Image](images/Cablage_bootloader_stm32f446.png)

Ancora in repertorio __*ports/stm32*__, possiamo eseguire il flashing del micropython sulla scheda con il comando:
```
make BOARD={Your-Board-Name} deploy-stlink
```
Il programma *st-flash* dovrebbe rilevare automaticamente la connessione USB alla scheda. In caso contrario, vai su __lsusb__ per determinare il suo bus USB e il numero di dispositivo e impostare la variabile di ambiente: __STLINK_DEVICE__, utilizzando il formato __<USB_BUS>: <USB_ADDR>__, come nell'esempio  :
```
lsusb
[...]
Bus 002 Device 035: ID 0483:3748 STMicroelectronics ST-LINK/V2
export STLINK_DEVICE="002:0035"
make BOARD=NUCLEOF446RE deploy-stlink
```
 Se tutto è andato bene, dovrebbe apparire un messaggio.
```
2020-02-05T10:27:32 INFO common.c: Starting verification of write complete
2020-02-05T10:27:35 INFO common.c: Flash written and verified! jolly good!
```
Ora, il micropython è nella tua scheda! Puoi aprire un terminale, in un altro sistema operativo che desideri, come PuTTY, e se premi il pulsante *Restart* dalla scheda, o *__CTRL+D__* sul terminale, ti mostrerà il seguente messaggio:


![Image](images/micropython_riot_putty.png)
