---
title: Esercizi con RIOT OS per il board P-NUCLEO-WB55
description: Esercizi con RIOT OS per il board P-NUCLEO-WB55

---
# Esercizi con RIOT OS per il board P-NUCLEO-WB55

Gli esercizi con RIOT OS per la tavola P-NUCLEO-WB55 stanno arrivando molto velocemente!

Nel frattempo puoi seguire gli esercizi del [Classi RIOT](https://github.com/RIOT-OS/riot-course/blob/master/README.md) utilizzando un'altra scheda Nucleo.

