---
title: Come utilizzare Fritzing per descrivere i montaggi per il kit STM32 MicroPython?
description: Come utilizzare Fritzing per descrivere i montaggi per il kit STM32 MicroPython?
---
# Come utilizzare Fritzing per descrivere i montaggi per il kit STM32 MicroPython?

## Fritzing
[Fritzing](https://fritzing.org/) è un'applicazione che permette di descrivere assemblaggi elettronici in modo educativo. È disponibile un'ampia libreria di componenti e mappe (ovvero _parts_).

## Sommario
In questa sezione troverai un tutorial sull'uso di [Fritzing](https://fritzing.org/) con il kit STM32 MicroPython.

## Installazione fritzing
Per scrivere

## Installazione della libreria _part_ STM32 Nucleo WB55 a Fritzing
Per scrivere

![Top](images/stm32nucleowb55-bottom.svg)
![Bottom](images/stm32nucleowb55-bottom.svg)

## Disegno di un semplice assieme con il componente STM32 Nucleo WB55
Per scrivere

Installa Grove _parts_ da [deposito Github](https://github.com/Seeed-Studio/fritzing_parts).


## Riferimenti
* [https://gitlab.com/stm32python/frizting](https://gitlab.com/stm32python/frizting)
