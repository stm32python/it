---
title: Altri kit STM32Python
description: Descrizione di altri kit educativi STM32Python
categories: stm32 kit nucleo python
---

# Il kit LoRaWAN

Le schede nel kit educativo 2 sono:

* [NUCLEO-F446RE - Scheda di sviluppo, MCU STM32F446RE Debugger integrato, compatibile con Arduino, ST Zio e Morpho](https://www.st.com/en/evaluation-tools/nucleo-f446re.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/nucleo-f446re/carte-de-dev-arduino-mbed-nucleo/dp/2491978)).

* [I-NUCLEO-LRWAN1 -  Scheda di espansione, modulo USI LoRa LPWAN, sensori multipli per Nucleo STM32, compatibile con Arduino](https://www.st.com/en/evaluation-tools/i-nucleo-lrwan1.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/i-nucleo-lrwan1/carte-d-extension-mcu-arm-cortex/dp/2809319)).

* [scheda figlia](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) de connection [Grove](http://wiki.seeedstudio.com/Grove_System/) e i sensori forniti (potenziometro, ultrasuoni, termometro, display a LED, ricevitori a infrarossi, display LCD …) (in vendita presso [Farnell](https://fr.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove) e a[Seeedstudio](https://www.seeedstudio.com/Base-Shield-V2.html)). _Questa scheda è consigliata ma non obbligatoria._

* [P-Nucleo-LRWAN2](https://www.st.com/en/evaluation-tools/p-nucleo-lrwan2.html). _Questo kit è consigliato ma non obbligatorio._

* [Gateway LoRaWAN TTIG](https://www.thethingsnetwork.org/docs/gateways/thethingsindoor/) (in vendita presso [RS-Online](https://fr.rs-online.com/web/p/kits-de-developpement-pour-radio-frequence/1843981/))

| | | |
|-|-|-|
![nucleo-f446re](images/nucleo-f446re.jpg) | ![i-nucleo-lrwan1](images/i-nucleo-lrwan1.jpg) | ![grove_starter_kit](images/grove_starter_kit.jpg)




# Altri kit STM32 per il futuro

* [STEVAL-STLKT01V1 - Kit di sviluppo, modulo IoT SensorTile, MCU STM32L476JGY, forma quadrata in miniatura](https://www.st.com/en/evaluation-tools/steval-stlkt01v1.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/steval-stlkt01v1/carte-de-developpement-capteur/dp/2664520))

* [STEVAL-BCN002V1B BlueTile -Kit di sviluppo del nodo sensore abilitato Bluetooth LE](https://www.st.com/en/evaluation-tools/steval-bcn002v1b.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/steval-bcn002v1b/kit-d-eval-bluetooth-low-energy/dp/3009966)).

* [B-L072Z-LRWAN1 -  Discovery Kit, LoRa® Low Power Wireless Module, SMA and U.FL RF Interface Connectors](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/b-l072z-lrwan1/kit-discovery-iot-connectivity/dp/2708776))

* [B-L475E-IOT01A Discovery kit for IoT node](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/b-l475e-iot01a2/kit-discovery-iot-node-868mhz/dp/2708778))

* [X-NUCLEO-GNSS1A1 GNSS expansion board](https://www.st.com/en/ecosystems/x-nucleo-gnss1a1.html)  (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/x-nucleo-gnss1a1/carte-extension-gnss-carte-nucleo/dp/2980979?ost=X-NUCLEO-GNSS1A1)


* [P-NUCLEO-IHM03 motor control kit](https://www.st.com/en/evaluation-tools/p-nucleo-ihm03.html)

* [EVALKIT-ROBOT-1 Valutazione kit](https://www.st.com/en/embedded-software/stsw-robot-1.html)

| | | |
|-|-|-|
![steva-stlkt01v1](images/steva-stlkt01v1.jpg) | ![steval-bcn002v1b](images/steval-bcn002v1b.jpg) | ![b-l072z-lrwan1](images/b-l072z-lrwan1.jpg)
![b-l475e-iot01a](images/b-l475e-iot01a.jpg) | ![x-nucleo-gnss1a1](images/x-nucleo-gnss1a1.jpg) | |


Crediti immagine :
* [ST Microelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System) 
