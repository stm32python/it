---
title: Kit STM32MicroPython
description: Descrizione del kit educativo STM32MicroPython
categories: stm32 kit nucleo python
---

## Le schede STM32

Le schede STMicroelectronics STM32 Nucleo e Discovery possono essere programmate in C / C ++ da
* l'ambiente [Arduino IDE](https://github.com/stm32duino/wiki/wiki/Getting-Started)
* l'ambiente [STM32Cube](https://www.st.com/stm32cube)
* l'ambiente online [MBed](https://os.mbed.com/platforms/ST-Nucleo-F446RE/)
* l'ambiente [RIOT OS](https://github.com/RIOT-OS/RIOT/tree/master/boards/nucleo-f446re)

I modelli F4xx possono essere programmati in [microPython](https://micropython.org/stm32/).

Alcuni modelli di STM32 sono programmabili tramite MicroPython. 
Il kit educativo P-NUCLEO-WB55 è uno di questi

## Il kit educativo P-NUCLEO-WB55

Il kit educativo STM32 Python include le seguenti schede:

* [P-NUCLEO-WB55 Bluetooth™ 5 and 802.15.4 Nucleo Pack including USB dongle and Nucleo-68 with STM32WB55 MCUs](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/p-nucleo-wb55/carte-iot-solut-mcu-arm-cortex/dp/2989052)).

* [X-NUCLEO-IKS01A3 - Scheda di espansione, sensore di movimento e ambiente MEMS per STM32 Nucleo, Arduino UNO Layout R3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html) (in vendita presso [Farnell](https://fr.farnell.com/stmicroelectronics/x-nucleo-iks01a3/carte-d-extension-stm32-nucleo/dp/3106035)).

* [Scheda figlia](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) di connessione[Grove](http://wiki.seeedstudio.com/Grove_System/) e sensori forniti (potenziometro, ultrasuoni, termometro, display LED, ricevitori a infrarossi, display LCD …) (in vendita presso [Farnell](https://fr.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove) e [Seeedstudio](https://www.seeedstudio.com/Base-Shield-V2.html)). _Questa scheda è consigliata ma non obbligatoria.._



| | | |
|-|-|-|
![p-nucleo-wb55](images/p-nucleo-wb55.jpg) | ![x-nucleo-iks01a3](images/x-nucleo-iks01a3.jpg) | ![grove_starter_kit](images/grove_starter_kit.jpg)

Crediti immagine:
* [STMicroelectronics](https://www.st.com)
* [Seeedstudio](http://wiki.seeedstudio.com/Grove_System) 

## Dove acquistare questo kit ?

In Francia, le schede STM32 Nucleo del kit didattico SNT possono essere acquistate online o tramite modulo d'ordine da fornitori di componenti elettronici come [Farnell](https://fr.farnell.com), [RS Online](https://fr.rs-online.com/web/), [Conrad](https://www.conrad.fr) ou bien [Mouser](https://www.mouser.fr/).

In Francia, [Lextronic](https://www.gotronic.fr/), [GoTronic](https://www.gotronic.fr/) e [Conrad](https://www.conrad.fr) distribuiscono la maggior parte delle schede di sistema [Grove de Seeedstudio](http://wiki.seeedstudio.com/Grove_System) e le schede I2C [Qwiic de Sparkfun](https://www.sparkfun.com/qwiic). _Ricordarsi di ordinare un numero sufficiente di cavi di collegamento_.

## Altri kit

Nota: sono possibili altri kit per i tutorial STM32MicroPython. [Guarda l'elenco ...](Kits)
