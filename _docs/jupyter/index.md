---
title: Jupyter
description: Come utilizzare il kit STM32 MicroPython con Jupyter
---
# Come utilizzare il kit STM32 MicroPython con Jupyter?

## Jupyter
[Jupyter](https://jupyter.org/)è un'applicazione che consente la programmazione in diversi linguaggi di programmazione come Python, Julia, Ruby, R o anche Scala. Jupyter ti permette di creare taccuini o taccuini mescolando appunti in formato Markdown, programmi e risultati delle loro esecuzioni.

## Sommario
In questa parte troverai il tutorial che ti permette di creare unNotebook [Jupyter](https://jupyter.org/)  per elaborare e visualizzare in tempo reale le misure effettuate dai sensori collegati alla scheda STM32.

* [Installazione di Jupyter](install)
* [Utilizzando un taccuino Jupyter](notebook)
