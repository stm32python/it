---
title: Interfaccia Jupyter
description: Interfaccia Jupyter
---
<div align="center"> <img  width="130" src="images/logo_jupyter.png" alt="Jupyter" /></div>

# Interfacciamento con Jupyter

Per poter visualizzare più efficacemente i risultati dei diversi programmi e per effettuare più facilmente rapporti pratici possiamo utilizzare il taccuino Jupyter.

Per poter utilizzare questo notebook è sufficiente aggiungere MicroPython a Jupyter e definire la porta corrispondente alla nostra scheda.

In Windows, questi comandi devono essere digitati nel prompt dei comandi.

![cmd windows](images/cmd.png)

## Installazione del kernel MicroPython

### Installazione di Python3

Vai al sito [https://www.python.org/downloads/](https://www.python.org/downloads/)

Scarica e installa Python3

Per verificare che l'installazione sia stata eseguita correttamente `python --version` e `pip --version`

![pip et python versions](images/python_pip_version.PNG)

### Installazione di Jupyter

```bash
pip install notebook`
```

Per verificare che tutto funzioni puoi avviare un notebook Jupyter con il comando `jupyter notebook`. Dovresti arrivare a una pagina come questa nel tuo browser:

![Accueil Jupyter](images/python_pip_version.PNG)

### Recupero del progetto MicroPython

Vai al sito [https://github.com/goatchurchprime/jupyter_micropython_kernel.git](https://github.com/goatchurchprime/jupyter_micropython_kernel.git)

![projet git](images/git_clone.PNG)

Scarica il progetto git in formato zip e decomprimilo

Metti la cartella in Utente

Installazione della libreria:
```bash
pip install -e jupyter_micropython_kernel-master
```

Installazione del kernel in Jupyter
```bash
python -m jupyter_micropython_kernel.install
```

Visualizzazione della lista dei core disponibili:
```bash
jupyter kernelspec list
```

![verification kernelgit](images/kernelspec.PNG)

È ora possibile [utilizzare un notebook Jupyter](./notebook)