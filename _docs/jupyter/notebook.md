---
title: Utilizzo di un notebook Jupyter con la scheda MicroPython
description: Utilizzo di un notebook Jupyter con la scheda MicroPython
---
<div align="center"> <img  width="130" src="images/logo_jupyter.png" alt="Jupyter" /></div>

# Utilizzo di un notebook Jupyter con la scheda MicroPython

 Avvia Jupyter inserendo il comando
```bash
jupyter notebook
```

In alto a destra fai clic su`New` e seleziona `MicroPython-USB`

![creazione di un notebook jupyter](images/jupyter_new.PNG)

 Per utilizzare la scheda dobbiamo specificare la porta da utilizzare. Questa riga viene aggiunta come prima riga del taccuino:
```bash
serialconnect to –port=COM14 –baud=115200
```

> Attenzione, la porta`COM` potrebbe cambiare a seconda del computer. È ora possibile eseguire tutti i programmi descritti negli esercizi di questo blocco note.