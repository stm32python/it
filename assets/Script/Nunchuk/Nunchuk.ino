#include <NintendoExtensionCtrl.h>

Nunchuk nchuk;

void setup() {
  Serial.begin(115200);
  nchuk.begin();

  while (!nchuk.connect()) {
    Serial.println("Manette non detectée !");
    delay(1000);
  }
}

void loop() {
  boolean success = nchuk.update();     //Récuperation des données de la manette

  if (success == true) {                //Si la connection est réussie
    nchuk.printDebug();                 //Affichage des valeurs
    delay(500);
  }
  else {                                //Si la manette est deconnectée
    Serial.println("Manette deconnectée !");
    delay(1000);
    nchuk.connect();
  }
}
